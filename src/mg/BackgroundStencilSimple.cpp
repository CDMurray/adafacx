#include "mg/BackgroundStencilSimple.h"
#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"
#include <cmath>


#include "matrixfree/stencil/ElementMatrix.h"
#include "matrixfree/stencil/StencilFactory.h"

tarch::multicore::BooleanSemaphore mg::BackgroundStencilSimple::_mysemaphore;

void mg::BackgroundStencilSimple::setCellData(double cellX, double cellY, double cellZ, double h)
{
    _cellX = cellX;
    _cellY = cellY;
    _cellZ = cellZ;
    _h = h;
}

void mg::BackgroundStencilSimple::init(mg::State &solverState, int numberOfSamplingPoints, int heapIndex)
{
    _numberofSamplingPoints = numberOfSamplingPoints;
    _finished = false;
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            _stencil(i, j) = 0.0;
        }
    }

    struct point dif = solverState.getMaxDifPoint();
    _xdif = dif.x;
    _ydif = dif.y;
    _zdif = dif.z;
    struct point min = solverState.getMin();
    _min_x = min.x;
    _min_y = min.y;
    _min_z = min.z;

    _heapIndex = heapIndex;
    _point_list = solverState.getPointList();
    _multigrid = matrixfree::solver::Multigrid();
}

mg::BackgroundStencilSimple::BackgroundStencilSimple()
{
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            _stencil(i, j) = 0.0;
        }
    }
}

mg::BackgroundStencilSimple::BackgroundStencilSimple(mg::State &solverState)
{
    //_stencil = 0;
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            _stencil(i, j) = 0.0;
        }
    }

    struct point dif = solverState.getMaxDifPoint();
    _xdif = dif.x;
    _ydif = dif.y;
    _zdif = dif.z;
    struct point min = solverState.getMin();
    _min_x = min.x;
    _min_y = min.y;
    _min_z = min.z;

    _point_list = solverState.getPointList();
    _multigrid = matrixfree::solver::Multigrid();
}


bool mg::BackgroundStencilSimple::operator()()
{
    _finished = false;

    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> stencil;

#ifdef Dim2
    stencil = matrixfree::stencil::getElementWiseAssemblyMatrix( matrixfree::stencil::get2DLaplaceStencil() );
#elif Dim3
    stencil = matrixfree::stencil::getElementWiseAssemblyMatrix( matrixfree::stencil::get3DLaplaceStencil() );
#endif
    double perm = getEpsilon(_numberofSamplingPoints);

    stencil = perm * stencil;

    _stencil = stencil;
    _numberofSamplingPoints++;

    _finished = true;

    tarch::multicore::Lock myLock(_mysemaphore);
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            DataHeap::getInstance().getData(_heapIndex)[4 * i + j]._persistentRecords._u = stencil(i, j);
        }
    }
    BoolHeap::getInstance().getData(_heapIndex)[0]._persistentRecords._u = true;
    myLock.free();
    return false;
}

bool mg::BackgroundStencilSimple::isFinished()
{
    return _finished;
}
void mg::BackgroundStencilSimple::resetFinished()
{
    _finished = false;
}

int mg::BackgroundStencilSimple::getNumPoints()
{
    return _numberofSamplingPoints;
}


tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> mg::BackgroundStencilSimple::getStencil()
{
    return _stencil;
}


double mg::BackgroundStencilSimple::getEpsilon(const int numberofSamplingPoints)
{
    double averagedEps = 0.0;

    struct point lowestCorner;
    lowestCorner.x = _cellX - _h / 2.0;
    lowestCorner.y = _cellY - _h / 2.0;
    lowestCorner.z = _cellZ - _h / 2.0;

    const double cellSize = _h;
    const double stepSize = 1.0 / (double)numberofSamplingPoints;
    const double quadratureSpacing = cellSize / (double)numberofSamplingPoints;

    double xpos, ypos;
    xpos = lowestCorner.x + quadratureSpacing / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        ypos = lowestCorner.y + quadratureSpacing / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            averagedEps += getPermeabilityD(xpos, ypos, 0) * stepSize * stepSize;
            ypos += quadratureSpacing;
        }
        xpos += quadratureSpacing;
    }
    return averagedEps;
}

double mg::BackgroundStencilSimple::getPermeabilityD(double x, double y, double z)
{
    tarch::la::Vector<DIMENSIONS, double> vector;

#ifdef Dim3
    vector = x, y, z;
#else
    vector = x, y;
#endif

    return mg::mappings::GenerateStencil::getEpsilon(vector);

}
