#include "tarch/logging/Log.h"
#include "tarch/tests/TestCaseRegistry.h"
#include "tarch/logging/CommandLineLogger.h"
#include "tarch/parallel/Node.h"

#include "peano/peano.h"

#include "mg/runners/Runner.h"

#include "mg/mappings/CreateGrid.h"
#include "mg/mappings/GenerateStencil.h"
#include "mg/mappings/ComputeRitzGalerkinOperator.h"
#include "mg/mappings/BStencil.h"
#include "mg/mappings/ProjectThreeGridDiff.h"
#include "mg/mappings/FMGRefine.h"



tarch::logging::Log _log("");


int main(int argc, char **argv)
{
    peano::fillLookupTables();

    int parallelSetup = peano::initParallelEnvironment(&argc, &argv);
    if ( parallelSetup != 0 )
    {
#ifdef Parallel
        // Please do not use the logging if MPI doesn't work properly.
        std::cerr << "mpi initialisation wasn't successful. Application shut down" << std::endl;
#else
        _log.error("main()", "mpi initialisation wasn't successful. Application shut down");
#endif
        return parallelSetup;
    }

#if defined(Parallel)
    tarch::logging::CommandLineLogger::getInstance().setLogFormat(
        " ", false, false,
        true, // this true switches the machine information on
        true, true, "log");
#endif

    int sharedMemorySetup = peano::initSharedMemoryEnvironment();
    if (sharedMemorySetup != 0)
    {
        logError("main()", "shared memory initialisation wasn't successful. Application shut down");
        return sharedMemorySetup;
    }

    int programExitCode = 0;

    // Configure the output
    tarch::logging::CommandLineLogger::getInstance().clearFilterList();
    tarch::logging::CommandLineLogger::getInstance().addFilterListEntry( ::tarch::logging::CommandLineLogger::FilterListEntry( "info", false ) );
    tarch::logging::CommandLineLogger::getInstance().addFilterListEntry( ::tarch::logging::CommandLineLogger::FilterListEntry( "debug", true ) );


#if defined(Asserts)
    tarch::logging::CommandLineLogger::getInstance().setLogFormat(" ", true, false, true, false, true, "" );
    tarch::logging::CommandLineLogger::getInstance().addFilterListEntry( ::tarch::logging::CommandLineLogger::FilterListEntry( "info", -1, "mpibalancing", false ) );
#elif defined(Parallel)
    tarch::logging::CommandLineLogger::getInstance().setLogFormat(" ", true, false, true, false, false, "" );
    tarch::logging::CommandLineLogger::getInstance().addFilterListEntry( ::tarch::logging::CommandLineLogger::FilterListEntry( "info", -1, "mpibalancing", true ) );
    //  tarch::logging::CommandLineLogger::getInstance().setLogFormat( ... please consult source code documentation );
#endif

#if defined(Asserts)
    // Runs the unit tests
    tarch::tests::TestCaseRegistry::getInstance().getTestCaseCollection().run();
    programExitCode = tarch::tests::TestCaseRegistry::getInstance().getTestCaseCollection().getNumberOfErrors();
#endif

    mg::runners::Runner::SolverType solverType = mg::runners::Runner::SolverType::Undef;
    int numberOfSmoothingSteps = 1;
    int totalCores = 1;
    int FMGCycleLength = 1;

    bool validCommandLineArguments = true;
    const int RequiredArguments = 22;
    if (argc != RequiredArguments)
    {
        validCommandLineArguments = false;
    }
    else
    {
        const std::string meshType = argv[1];
        const int         initialLevel = atoi(argv[2]);
        const int         maxLevel    = atoi(argv[3]);
        const int         minLevel    = atoi(argv[4]);
        const int         FMGcycle    = atoi(argv[5]);
        const std::string problem     = argv[6];
        const double      gradient    = atof(argv[7]);
        const double      epsilon     = atof(argv[8]);
        const std::string type        = argv[9];
        const std::string fineType    = argv[10];
        const std::string coarseType  = argv[11];
        const std::string restrictionType  = argv[12];
        const double      omega       = atof(argv[13]);
        const double      omega2      = atof(argv[14]);
        const double      omegaI      = atof(argv[15]);
        const std::string altRestrictionType  = argv[16];
        const int         smooth      = atoi(argv[17]);
        const int         cores       = atoi(argv[18]);
        const int         delayCoarse = atoi(argv[19]);
        const std::string initialRefine = argv[20];
        const std::string simpleStencil = argv[21];

        if (meshType.compare( "regular") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::Regular;
        }
        else if (meshType.compare( "static-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::StaticRefine;
        }
        else if (meshType.compare( "dynamic-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::DynamicRefine;
        }
        else if (meshType.compare( "dynamic-with-initial-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::DynamicWithInitialRefine;
        }
        else if (meshType.compare( "boundary-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::BoundaryRefine;
        }
        else if (meshType.compare( "disc-boundary-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::DiscBoundaryRefine;
        }
        else if (meshType.compare( "line-boundary-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::LineBoundaryRefine;
        }
        else if (meshType.compare( "circle-boundary-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::CircleBoundaryRefine;
        }
        else if (meshType.compare( "checkerboard-boundary-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::CheckBoundaryRefine;
        }
        else if (meshType.compare( "automatic-boundary-refine") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::AutomaticBoundaryRefine;
        }
        else if (meshType.compare( "automatic-boundary-refine-with-convergence") == 0 )
        {
            mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::AutomaticBoundaryRefineWithConvergence;
        }
        else
        {
            std::cerr << "invalid grid type " << meshType << std::endl;
            validCommandLineArguments = false;
        }

        if (maxLevel > 0)
        {
            mg::mappings::CreateGrid::maxLevel = maxLevel;
        }
        else
        {
            std::cerr << "invalid max mesh level " << maxLevel << std::endl;
            validCommandLineArguments = false;
        }
        if (initialLevel > 0)
        {
            mg::mappings::CreateGrid::initialLevel = initialLevel;
            mg::mappings::CreateGrid::currentMaxLevel = initialLevel;
        }
        else
        {
            std::cerr << "invalid initial mesh level " << initialLevel << std::endl;
            validCommandLineArguments = false;
        }


        if (minLevel >= 0)
        {
            mg::mappings::CreateGrid::minLevel = minLevel;
            mg::mappings::CreateGrid::finalMinLevel = minLevel;
        }
        else
        {
            std::cerr << "invalid minimum solve level " << minLevel << std::endl;
            validCommandLineArguments = false;
        }

        if (FMGcycle > 0)
        {
            FMGCycleLength = FMGcycle;
        }
        else
        {
            std::cerr << "invalid FMG Cycle Length " << FMGcycle << std::endl;
            validCommandLineArguments = false;
        }


        if (problem.compare( "Poisson") == 0 )
        {
            mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::Poisson;
        }
        else if (problem.compare( "jumping-coefficient") == 0 )
        {
            mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::JumpingCoefficient;
        }
        else if (problem.compare( "checkerboard") == 0 )
        {
            mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::Checkerboard;
        }
        else if (problem.compare( "circle") == 0 )
        {
            mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::Circle;
        }
        else if (problem.compare( "line") == 0 )
        {
            mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::Line;
        }
        else if (problem.compare( "anisotropic") == 0 )
        {
            mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::Anisotropic;
        }
        else if (problem.compare( "sinusoidal") == 0 )
        {
            mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::Sinusoidal;
        }
        else if (problem.compare( "perlin") == 0 )
        {
            //mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::Perlin;
            break;
        }
        else if (problem.compare( "perlin-starvation") == 0 )
        {
            //mg::mappings::GenerateStencil::problem = mg::mappings::GenerateStencil::Problem::PerlinStarvation;
            break;
        }
        else
        {
            std::cerr << "invalid problem type " << problem << std::endl;
            validCommandLineArguments = false;
        }

        if ((gradient >= -1) && (gradient <= 1))
        {
            mg::mappings::GenerateStencil::gradient = gradient;
        }
        else
        {
            std::cerr << "invalid gradient " << gradient << std::endl;
            validCommandLineArguments = false;
        }


        mg::mappings::GenerateStencil::epsilon = epsilon;



        if (type.compare( "adaFACx") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::adaFACx;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::adaFACx;
        }
        else if (type.compare( "adaFACx-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::adaFACxWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::adaFACx;
        }
        else if (type.compare( "adaFACx-simple") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::adaFACxSimple;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::adaFACxSimple;
        }
        else if (type.compare( "adaFACx-simple-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::adaFACxSimpleWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::adaFACxSimple;
        }
        else if (type.compare( "AFACx") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::AFACx;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::AFACx;
        }
        else if (type.compare( "AFACx-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::AFACxWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::AFACx;
        }
        else if (type.compare( "AFACx-simple") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::AFACxSimple;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple;
        }
        else if (type.compare( "AFACx-simple-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::AFACxSimpleWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple;
        }
        else if (type.compare( "AFACx-alt") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::AFACxAlt;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::AFACxAlt;
        }
        else if (type.compare( "additiveMG") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::additiveMG;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::Additive;
        }
        else if (type.compare( "additiveMG-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::additiveMGWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::Additive;
        }
        else if (type.compare( "additiveMG-with-exponential-damping") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::additiveMGWithExponentialDamping;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::Additive;
        }
        else if (type.compare( "additiveMG-with-exponential-damping-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::additiveMGWithExponentialDampingWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::Additive;
        }
        else if (type.compare( "bpx") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::bpx;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::BPX;
        }
        else if (type.compare( "bpx-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::bpxWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::BPX;
        }
        else if (type.compare( "alt-rp") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::altRP;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::altRP;
        }
        else if (type.compare( "alt-rp-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::altRPWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::altRP;
        }
        else if (type.compare( "two-project") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::twoProject;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::twoProject;
        }
        else if (type.compare( "two-project-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::twoProjectWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::twoProject;
        }
        else if (type.compare( "two-project-simple") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::twoProjectSimple;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple;
        }
        else if (type.compare( "two-project-simple-with-plots") == 0 )
        {
            solverType = mg::runners::Runner::SolverType::twoProjectSimpleWithPlots;
            mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple;
        }
        else
        {
            std::cerr << "unknown solver argument " << type << std::endl;
            validCommandLineArguments = false;
        }


        if (fineType.compare( "static") == 0 )
        {
            mg::mappings::BStencil::fineStencilType = mg::mappings::BStencil::FineStencilType::Static;
        }
        else if (fineType.compare( "precompute") == 0 )
        {
            mg::mappings::BStencil::fineStencilType = mg::mappings::BStencil::FineStencilType::Precompute;
        }
        else if (fineType.compare( "background") == 0 )
        {
            mg::mappings::BStencil::fineStencilType = mg::mappings::BStencil::FineStencilType::Background;
        }
        else if (fineType.compare( "precompute-without-coarse") == 0 )
        {
            mg::mappings::BStencil::fineStencilType = mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse;
        }
        else if (fineType.compare( "background-with-coarse") == 0 )
        {
            mg::mappings::BStencil::fineStencilType = mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse;
        }
        else if (fineType.compare( "static-with-coarse") == 0 )
        {
            mg::mappings::BStencil::fineStencilType = mg::mappings::BStencil::FineStencilType::StaticWithCoarse;
        }
        else
        {
            std::cerr << "unknown fine type argument " << fineType << std::endl;
            validCommandLineArguments = false;
        }

        if (coarseType.compare( "geometric") == 0 )
        {
            mg::mappings::ComputeRitzGalerkinOperator::equationType = mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Geometric;
        }
        else if (coarseType.compare( "algebraic") == 0 )
        {
            mg::mappings::ComputeRitzGalerkinOperator::equationType = mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Algebraic;
        }
        else
        {
            std::cerr << "unknown coarse type argument " << coarseType << std::endl;
            validCommandLineArguments = false;
        }

        if (restrictionType.compare( "dlinear") == 0 )
        {
            mg::mappings::ProjectThreeGridDiff::restriction = mg::mappings::ProjectThreeGridDiff::Restriction::dlinear;
        }
        else if (restrictionType.compare( "boxMG") == 0 )
        {
            mg::mappings::ProjectThreeGridDiff::restriction = mg::mappings::ProjectThreeGridDiff::Restriction::boxMG;
        }
        else
        {
            std::cerr << "unknown restriction type argument " << restrictionType << std::endl;
            validCommandLineArguments = false;
        }

        /*if (omega > 0.0 and omega < 1.5 )
        {
            mg::Vertex::FineGridOmegaAlt = omega;
        }
        else
        {
            std::cerr << "invalid relaxation factor " << omega << std::endl;
            validCommandLineArguments = false;
        }*/


        if (omega2 > 0.0 and omega2 < 1.5 )
        {
            mg::Vertex::FineGridOmega = omega;
            mg::Vertex::FineGridOmega = omega2;
        }
        else
        {
            std::cerr << "invalid relaxation factor " << omega2 << std::endl;
            validCommandLineArguments = false;
        }
        if (omegaI > -1.5 and omegaI < 2.5 )
        {
            mg::Vertex::AdditionalOmegaWeighting = omegaI;
        }
        else
        {
            std::cerr << "invalid relaxation factor " << omegaI << std::endl;
            validCommandLineArguments = false;
        }

        if (altRestrictionType.compare( "injection") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::Injection;
        }
        else if (altRestrictionType.compare( "half-weighting") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::HalfWeighting;
        }
        else if (altRestrictionType.compare( "reduced-kernel-full-weighting") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::ReducedKernelFullWeighting;
        }
        else if (altRestrictionType.compare( "full-weighting") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::FullWeighting;
        }
        else if (altRestrictionType.compare( "piecewise-constant") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::PiecewiseConstant;
        }
        else if (altRestrictionType.compare( "smoothed") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::Smoothed;
        }
        else if (altRestrictionType.compare( "smoothed-partial") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::SmoothedPartial;
        }
        else if (altRestrictionType.compare( "damped-half") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::DampedHalf;
        }
        else if (altRestrictionType.compare( "alt-half") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::AltHalf;
        }
        else if (altRestrictionType.compare( "alt-smoothed") == 0 )
        {
            mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::AltSmoothed;
        }
        else
        {
            std::cerr << "unknown alternative restriction type argument " << restrictionType << std::endl;
            validCommandLineArguments = false;
        }

        if (smooth > 0)
        {
            numberOfSmoothingSteps = smooth;
        }
        else
        {
            std::cerr << "invalid number of smoothing steps " << smooth << std::endl;
            validCommandLineArguments = false;
        }
        if (cores > 0)
        {
            totalCores = cores;
        }
        else
        {
            std::cerr << "invalid number of cores " << cores << std::endl;
            validCommandLineArguments = false;
        }

        if(delayCoarse == 0)
        {
            mg::mappings::ProjectThreeGridDiff::delayCoarse = false;
        }
        else if(delayCoarse > 0)
        {
            mg::mappings::ProjectThreeGridDiff::delayCoarse = true;
            mg::mappings::CreateGrid::minLevel = mg::mappings::CreateGrid::initialLevel;

        }
        else
        {
            std::cerr << "invalid delay coarse" << delayCoarse << std::endl;
            validCommandLineArguments = false;

        }

        if(initialRefine.compare("prerefine") == 0)
        {
            mg::mappings::FMGRefine::initial = true;
            mg::mappings::CreateGrid::currentMaxLevel = mg::mappings::CreateGrid::maxLevel;

        }
        else if(initialRefine.compare("no-prerefine") == 0)
        {
            mg::mappings::FMGRefine::initial = false;

        }
        else
        {
            std::cerr << "invalid prerefinement setting " << initialRefine << std::endl;
            validCommandLineArguments = false;

        }

        if(simpleStencil.compare("integrate-stencil") == 0)
        {
            mg::mappings::BStencil::fineStencilSimple = mg::mappings::BStencil::FineStencilSimple::Integrated;

        }
        else if(simpleStencil.compare("average-stencil") == 0)
        {
            mg::mappings::BStencil::fineStencilSimple = mg::mappings::BStencil::FineStencilSimple::Averaged;

        }
        else
        {
            std::cerr << "invalid stencil simplicity setting " << simpleStencil << std::endl;
            validCommandLineArguments = false;

        }
    }


    if (!validCommandLineArguments)
    {
        std::cerr << "Usage: ./executable meshtype initial-levels max-levels problem type fine-stencil coarse-stencil restriction omega omega2 smooth cores" << std::endl
                  << "meshtype             regular, dynamic-refine, dynamic-with-initial-refine or static-refine" << std::endl
                  << "initial-levels       integer >0" << std::endl
                  << "max-levels           integer >0" << std::endl
                  << "FMG-cycle            integer >0" << std::endl
                  << "problem              Poisson or jumping-coefficient-1exxx with xxxx being -3,-2,-1,1,2" << std::endl
                  << "type                 additiveMG, additiveMG-with-plots, additiveMG-with-exponential-damping, additiveMG-with-exponential-damping-with-plots," << std::endl
                  << "                     bpx, bpx-with-plots, adaFACx or adaFACx-with-plots" << std::endl
                  << "fine-stencil         background, precompute or static" << std::endl
                  << "coarse-stencil       algebraic or geometric" << std::endl
                  << "restriction          dlinear or boxMG" << std::endl
                  << "omega                Fine grid relaxation parameter (0,1)" << std::endl
                  << "omega2               Alternative fine grid relaxation parameter (0,1) (only used with multiple smoothing steps)" << std::endl
                  << "omega adaFACx        Additional multiplicative weighting applied to existing omega exlusively on adaFACx grid" << std::endl
                  << "restriction adaFACx  Additional restriction type exlusively on adaFACx grid - injection, half-weighting, reduced-kernel-full-weighting, full-weighting" << std::endl
                  << "smooth               Number of smoothing steps" << std::endl
                  << "cores                Number of cores to utilise" << std::endl
                  << std::endl;
        return -1;
    }

    if (programExitCode == 0)
    {
        tarch::logging::CommandLineLogger::getInstance().addFilterListEntry( ::tarch::logging::CommandLineLogger::FilterListEntry( "debug", -1, "mg", false ) );
        mg::runners::Runner runner;
        programExitCode = runner.run(solverType, numberOfSmoothingSteps, totalCores, FMGCycleLength);
    }

    // ============================================================

    if (programExitCode == 0)
    {
#ifdef Parallel
        if (tarch::parallel::Node::getInstance().isGlobalMaster())
        {
            logInfo( "main()", "Peano terminates successfully" );
        }
#else
        logInfo( "main()", "Peano terminates successfully" );
#endif
    }
    else
    {
        logInfo( "main()", "quit with error code " << programExitCode );
    }

    peano::shutdownParallelEnvironment();
    peano::shutdownSharedMemoryEnvironment();
    peano::releaseCachedData();

    return programExitCode;
}
