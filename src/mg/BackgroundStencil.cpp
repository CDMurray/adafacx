#include "mg/BackgroundStencil.h"
#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"
#include <cmath>

#include "matrixfree/stencil/ElementMatrix.h"
#include "matrixfree/stencil/StencilFactory.h"


tarch::multicore::BooleanSemaphore mg::BackgroundStencil::_mysemaphore;



void mg::BackgroundStencil::setCellData(double cellX, double cellY, double cellZ, double h)
{
    _cellX = cellX;
    _cellY = cellY;
    _cellZ = cellZ;
    _h = h;
}


void mg::BackgroundStencil::init(mg::State &solverState, int numberOfSamplingPoints, int heapIndex)
{
    _numberofSamplingPoints = numberOfSamplingPoints;
    _finished = false;
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            _stencil(i, j) = 0.0;
        }
    }

    struct point dif = solverState.getMaxDifPoint();
    _xdif = dif.x;
    _ydif = dif.y;
    _zdif = dif.z;
    struct point min = solverState.getMin();
    _min_x = min.x;
    _min_y = min.y;
    _min_z = min.z;

    _heapIndex = heapIndex;
    _point_list = solverState.getPointList();
    _multigrid = matrixfree::solver::Multigrid();
}


mg::BackgroundStencil::BackgroundStencil(const mg::State &solverState)
{
    //_stencil = 0;
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            _stencil(i, j) = 0.0;
        }
    }

    struct point dif = solverState.getMaxDifPoint();
    _xdif = dif.x;
    _ydif = dif.y;
    _zdif = dif.z;
    struct point min = solverState.getMin();
    _min_x = min.x;
    _min_y = min.y;
    _min_z = min.z;

    _point_list = solverState.getPointList();
    _multigrid = matrixfree::solver::Multigrid();
}


bool mg::BackgroundStencil::operator()()
{
    _finished = false;
    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> stencil;
    bool outer_x = false, outer_y = false, outer_z = false;
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        bool inner_x = false, inner_y = false, inner_z = false;
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            int count = 0;
            if(inner_x == outer_x)
            {
                count++;
            }
            if(inner_y == outer_y)
            {
                count++;
            }
            if(inner_z == outer_z)
            {
                count++;
            }
            double result = 0.0;
            switch(count)
            {
            case 0:
                result = cornerNumericalIntegration(outer_x, outer_y, outer_z, _numberofSamplingPoints);
                break;
            case 1:
                result = edgeNumericalIntegration(outer_x, outer_y, outer_z, inner_x, inner_y, inner_z, _numberofSamplingPoints);
                break;
            case 2:
                result = faceNumericalIntegration(outer_x, outer_y, outer_z, inner_x, inner_y, inner_z, _numberofSamplingPoints);
                break;
            case 3:
                result = centreNumericalIntegration(outer_x, outer_y, outer_z, _numberofSamplingPoints);
                break;
            }
            stencil(i, j) = result;

            if (inner_y && inner_x)
            {
                inner_z = !inner_z;
            }
            if (inner_x)
            {
                inner_y = !inner_y;
            }
            inner_x = !inner_x;

        }
        if (outer_y && outer_x)
        {
            outer_z = !outer_z;
        }
        if (outer_x)
        {
            outer_y = !outer_y;
        }
        outer_x = !outer_x;
    }
    _stencil = stencil;
    _numberofSamplingPoints++;
    _finished = true;

    tarch::multicore::Lock myLock(_mysemaphore);
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            DataHeap::getInstance().getData(_heapIndex)[8 * i + j]._persistentRecords._u = stencil(i, j);
        }
    }
    BoolHeap::getInstance().getData(_heapIndex)[0]._persistentRecords._u = true;
    myLock.free();

    return false;
}


bool mg::BackgroundStencil::isFinished()
{
    return _finished;
}
void mg::BackgroundStencil::resetFinished()
{
    _finished = false;
}

int mg::BackgroundStencil::getNumPoints()
{
    return _numberofSamplingPoints;
}


tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> mg::BackgroundStencil::getStencil()
{
    return _stencil;
}



double mg::BackgroundStencil::centreNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_zpos, const int numberofSamplingPoints)
{
#ifdef Dim3
    struct point lowestCorner;
    if(vert_xpos)
    {
        lowestCorner.x = _cellX - _h / 2.0;
    }
    else
    {
        lowestCorner.x = _cellX + _h / 2.0;
    }
    if(vert_ypos)
    {
        lowestCorner.y = _cellY - _h / 2.0;
    }
    else
    {
        lowestCorner.y = _cellY + _h / 2.0;
    }
    if(vert_zpos)
    {
        lowestCorner.z = _cellZ - _h / 2.0;
    }
    else
    {
        lowestCorner.z = _cellZ + _h / 2.0;
    }

    const double cellSize = _h;
    const double quadratureSpacing = cellSize / (double)numberofSamplingPoints;
    const double stepSize = 1.0 / (double)numberofSamplingPoints;
    double xpos, ypos, zpos;
    double cellXPos, cellYPos, cellZPos;

    int evalCount = 0;

    double xmod, ymod, zmod;
    if(vert_xpos)
    {
        xmod = 1.0;
    }
    else
    {
        xmod = -1.0;
    }
    if(vert_ypos)
    {
        ymod = 1.0;
    }
    else
    {
        ymod = -1.0;
    }
    if(vert_zpos)
    {
        zmod = 1.0;
    }
    else
    {
        zmod = -1.0;
    }



    std::vector<double> partialDerivativeEvalsx(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    std::vector<double> partialDerivativeEvalsy(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    std::vector<double> partialDerivativeEvalsz(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);

    xpos = stepSize / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        ypos = stepSize / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            zpos = stepSize / 2.0;
            for(int k = 0; k < numberofSamplingPoints; k++)
            {
                struct point p;
                p.x = xpos;
                p.y = ypos;
                p.z = zpos;

                partialDerivativeEvalsx[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 0, true, true, true, stepSize);
                partialDerivativeEvalsy[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 1, true, true, true, stepSize);
                partialDerivativeEvalsz[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 2, true, true, true, stepSize);
                evalCount++;
                zpos += stepSize;
            }
            ypos += stepSize;
        }
        xpos += stepSize;
    }
    std::vector<double> permeabilityValues(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    int permCount = 0;
    //need this as all epsilon probably different (or no way of knowing if different or not w/o sampling)
    cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
            for(int k = 0; k < numberofSamplingPoints; k++)
            {
                permeabilityValues[permCount] = getPermeabilityD(cellXPos, cellYPos, cellZPos);
                cellZPos += zmod * quadratureSpacing;
                permCount++;
            }
            cellYPos += ymod * quadratureSpacing;
        }
        cellXPos += xmod * quadratureSpacing;
    }

    double totalVal = 0.0;
    double dx, dy, dz;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            for(int k = 0; k < numberofSamplingPoints; k++)
            {

                dx = partialDerivativeEvalsx[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                dy = partialDerivativeEvalsy[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                dz = partialDerivativeEvalsz[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                totalVal += permeabilityValues[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] * (dx + dy + dz);
            }
        }
    }

    return totalVal;
#else
    assertionMsg(false, "not implemented yet");
    return 0.0;
#endif
}

double mg::BackgroundStencil::faceNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_zpos, bool vert_xposf, bool vert_yposf, bool vert_zposf, const int numberofSamplingPoints)
{
#ifdef Dim3
    struct point lowestCorner;
    if(vert_xpos)
    {
        lowestCorner.x = _cellX - _h / 2.0;
    }
    else
    {
        lowestCorner.x = _cellX + _h / 2.0;
    }
    if(vert_ypos)
    {
        lowestCorner.y = _cellY - _h / 2.0;
    }
    else
    {
        lowestCorner.y = _cellY + _h / 2.0;
    }
    if(vert_zpos)
    {
        lowestCorner.z = _cellZ - _h / 2.0;
    }
    else
    {
        lowestCorner.z = _cellZ + _h / 2.0;
    }

    const double cellSize = _h;
    const double quadratureSpacing = cellSize / (double)numberofSamplingPoints;
    const double stepSize = 1.0 / (double)numberofSamplingPoints;
    double xpos, ypos, zpos;
    double cellXPos, cellYPos, cellZPos;


    std::vector<double> partialDerivativeEvalsx(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    std::vector<double> partialDerivativeEvalsy(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    std::vector<double> partialDerivativeEvalsz(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    int evalCount = 0;
    xpos = stepSize / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        ypos = stepSize / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            zpos = stepSize / 2.0;
            for(int k = 0; k < numberofSamplingPoints; k++)
            {
                struct point p;
                p.x = xpos;
                p.y = ypos;
                p.z = zpos;

                partialDerivativeEvalsx[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 0, false, true, true, stepSize);
                partialDerivativeEvalsy[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 1, false, true, true, stepSize);
                partialDerivativeEvalsz[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 2, false, true, true, stepSize);

                evalCount++;
                zpos += stepSize;
            }
            ypos += stepSize;
        }
        xpos += stepSize;
    }

    double xmod, ymod, zmod;
    if(vert_xpos)
    {
        xmod = 1.0;
    }
    else
    {
        xmod = -1.0;
    }
    if(vert_ypos)
    {
        ymod = 1.0;
    }
    else
    {
        ymod = -1.0;
    }
    if(vert_zpos)
    {
        zmod = 1.0;
    }
    else
    {
        zmod = -1.0;
    }
    std::vector<double> permeabilityValues(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    int permCount = 0;

    cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
    cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
    if((vert_zposf ^ vert_zpos))
    {
        cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
    }
    else if ((vert_xposf ^ vert_xpos))
    {
        cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    }
    else
    {
        cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
    }
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        if((vert_zposf ^ vert_zpos))
        {
            cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
        }
        else if((vert_xposf ^ vert_xpos))
        {
            cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
        }
        else
        {
            cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
        }
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            if((vert_zposf ^ vert_zpos))
            {
                cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
            }
            else if((vert_xposf ^ vert_xpos))
            {
                cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
            }
            else
            {
                cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
            }
            for(int k = 0; k < numberofSamplingPoints; k++)
            {
                permeabilityValues[permCount] = getPermeabilityD(cellXPos, cellYPos, cellZPos);

                if((vert_zposf ^ vert_zpos))
                {
                    cellYPos += ymod * quadratureSpacing;
                }
                else if((vert_xposf ^ vert_xpos))
                {
                    cellZPos += zmod * quadratureSpacing;
                }
                else
                {
                    cellXPos += xmod * quadratureSpacing;
                }
                permCount++;
            }
            if((vert_zposf ^ vert_zpos))
            {
                cellXPos += xmod * quadratureSpacing;
            }
            else if((vert_xposf ^ vert_xpos))
            {
                cellYPos += ymod * quadratureSpacing;
            }
            else
            {
                cellZPos += zmod * quadratureSpacing;
            }
        }
        if((vert_zposf ^ vert_zpos))
        {
            cellZPos += zmod * quadratureSpacing;
        }
        else if((vert_xposf ^ vert_xpos))
        {
            cellXPos += xmod * quadratureSpacing;
        }
        else
        {
            cellYPos += ymod * quadratureSpacing;
        }
    }
    double faceVal = 0.0;
    double dx, dy, dz;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            for(int k = 0; k < numberofSamplingPoints; k++)
            {

                dx = partialDerivativeEvalsx[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                dy = partialDerivativeEvalsy[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                dz = partialDerivativeEvalsz[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                faceVal += permeabilityValues[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] * (dx + dy + dz);

            }
        }
    }
    return faceVal;
#else
    assertionMsg(false, "not implemented yet" );
    return 0.0;
#endif
}


double mg::BackgroundStencil::edgeNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_zpos, bool vert_xpose, bool vert_ypose, bool vert_zpose, const int numberofSamplingPoints)
{
#ifdef Dim3
    struct point lowestCorner;
    if(vert_xpos)
    {
        lowestCorner.x = _cellX - _h / 2.0;
    }
    else
    {
        lowestCorner.x = _cellX + _h / 2.0;
    }
    if(vert_ypos)
    {
        lowestCorner.y = _cellY - _h / 2.0;
    }
    else
    {
        lowestCorner.y = _cellY + _h / 2.0;
    }
    if(vert_zpos)
    {
        lowestCorner.z = _cellZ - _h / 2.0;
    }
    else
    {
        lowestCorner.z = _cellZ + _h / 2.0;
    }
    const double cellSize = _h;
    const double quadratureSpacing = cellSize / (double)numberofSamplingPoints;
    const double stepSize = 1.0 / (double)numberofSamplingPoints;
    double xpos, ypos, zpos;
    double cellXPos, cellYPos, cellZPos;

    std::vector<double> partialDerivativeEvalsx(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    std::vector<double> partialDerivativeEvalsy(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    std::vector<double> partialDerivativeEvalsz(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    int evalCount = 0;
    xpos = stepSize / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        ypos = stepSize / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            zpos = stepSize / 2.0;
            for(int k = 0; k < numberofSamplingPoints; k++)
            {
                struct point p;
                p.x = xpos;
                p.y = ypos;
                p.z = zpos;

                partialDerivativeEvalsx[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 0, false, false, true, stepSize);
                partialDerivativeEvalsy[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 1, false, false, true, stepSize);
                partialDerivativeEvalsz[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 2, false, false, true, stepSize);

                evalCount++;
                zpos += stepSize;
            }
            ypos += stepSize;
        }
        xpos += stepSize;
    }


    double xmod, ymod, zmod;
    if(vert_xpos)
    {
        xmod = 1.0;
    }
    else
    {
        xmod = -1.0;
    }
    if(vert_ypos)
    {
        ymod = 1.0;
    }
    else
    {
        ymod = -1.0;
    }
    if(vert_zpos)
    {
        zmod = 1.0;
    }
    else
    {
        zmod = -1.0;
    }
    cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
    cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
    std::vector<double> permeabilityValues(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    int permCount = 0;
    //need this as all epsilon probably different (or no way of knowing if different or not w/o sampling)
    if(!(vert_zpose ^ vert_zpos))
    {
        //if((vert_zpose^vert_zpos)){
        cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    }
    else if (!(vert_xpose ^ vert_xpos))
    {
        cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
    }
    else
    {
        cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
    }
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        if(!(vert_zpose ^ vert_zpos))
        {
            cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
        }
        else if (!(vert_xpose ^ vert_xpos))
        {
            cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
        }
        else
        {
            cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
        }
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            if(!(vert_zpose ^ vert_zpos))
            {
                cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
            }
            else if (!(vert_xpose ^ vert_xpos))
            {
                cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
            }
            else
            {
                cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
            }
            for(int k = 0; k < numberofSamplingPoints; k++)
            {
                permeabilityValues[permCount] = getPermeabilityD(cellXPos, cellYPos, cellZPos);

                if(!(vert_zpose ^ vert_zpos))
                {
                    cellZPos += zmod * quadratureSpacing;
                }
                else if (!(vert_xpose ^ vert_xpos))
                {
                    cellXPos += xmod * quadratureSpacing;
                }
                else
                {
                    cellYPos += ymod * quadratureSpacing;
                }
                permCount++;
            }
            if(!(vert_zpose ^ vert_zpos))
            {
                cellYPos += ymod * quadratureSpacing;
            }
            else if (!(vert_xpose ^ vert_xpos))
            {
                cellZPos += zmod * quadratureSpacing;
            }
            else
            {
                cellXPos += xmod * quadratureSpacing;
            }
        }
        if(!(vert_zpose ^ vert_zpos))
        {
            cellXPos += xmod * quadratureSpacing;
        }
        else if (!(vert_xpose ^ vert_xpos))
        {
            cellYPos += ymod * quadratureSpacing;
        }
        else
        {
            cellZPos += zmod * quadratureSpacing;
        }
    }

    double rowVal = 0.0;
    double dx, dy, dz;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            for(int k = 0; k < numberofSamplingPoints; k++)
            {

                dx = partialDerivativeEvalsx[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                dy = partialDerivativeEvalsy[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                dz = partialDerivativeEvalsz[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];

                rowVal += permeabilityValues[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] * (dx + dy + dz);

            }
        }
    }
    return rowVal;
#else
    assertionMsg(false, "not implemented yet" );
    return 0.0;
#endif
}

double mg::BackgroundStencil::cornerNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_zpos, const int numberofSamplingPoints)
{
#ifdef Dim3
    struct point lowestCorner;
    if(vert_xpos)
    {
        lowestCorner.x = _cellX - _h / 2.0;
    }
    else
    {
        lowestCorner.x = _cellX + _h / 2.0;
    }
    if(vert_ypos)
    {
        lowestCorner.y = _cellY - _h / 2.0;
    }
    else
    {
        lowestCorner.y = _cellY + _h / 2.0;
    }
    if(vert_zpos)
    {
        lowestCorner.z = _cellZ - _h / 2.0;
    }
    else
    {
        lowestCorner.z = _cellZ + _h / 2.0;
    }
    const double cellSize = _h;
    const double quadratureSpacing = cellSize / (double)numberofSamplingPoints;
    const double stepSize = 1.0 / (double)numberofSamplingPoints;
    double xpos, ypos, zpos;
    double cellXPos, cellYPos, cellZPos;

    std::vector<double> partialDerivativeEvals000(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    int evalCount = 0;

    double xmod, ymod, zmod;
    if(vert_xpos)
    {
        xmod = 1.0;
    }
    else
    {
        xmod = -1.0;
    }
    if(vert_ypos)
    {
        ymod = 1.0;
    }
    else
    {
        ymod = -1.0;
    }
    if(vert_zpos)
    {
        zmod = 1.0;
    }
    else
    {
        zmod = -1.0;
    }

    xpos = stepSize / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        ypos = stepSize / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            zpos = stepSize / 2.0;
            for(int k = 0; k < numberofSamplingPoints; k++)
            {
                struct point p;
                p.x = xpos;
                p.y = ypos;
                p.z = zpos;
                partialDerivativeEvals000[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] = getPartialDerivativeEvalAnalytic(p, 0, false, false, false, stepSize);
                evalCount++;
                zpos += stepSize;
            }
            ypos += stepSize;
        }
        xpos += stepSize;
    }

    std::vector<double> permeabilityValues(numberofSamplingPoints * numberofSamplingPoints * numberofSamplingPoints);
    int permCount = 0;
    //need this as all epsilon probably different (or no way of knowing if different or not w/o sampling)
    cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            cellZPos = lowestCorner.z + zmod * quadratureSpacing / 2.0;
            for(int k = 0; k < numberofSamplingPoints; k++)
            {
                permeabilityValues[permCount] = getPermeabilityD(cellXPos, cellYPos, cellZPos);
                cellZPos += zmod * quadratureSpacing;
                permCount++;
            }
            cellYPos += ymod * quadratureSpacing;
        }
        cellXPos += xmod * quadratureSpacing;
    }//calculate Corner

    double cornerVal = 0.0;
    double dx, dy, dz;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            for(int k = 0; k < numberofSamplingPoints; k++)
            {

                dx = partialDerivativeEvals000[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints];
                dy = partialDerivativeEvals000[j + i * numberofSamplingPoints + k * numberofSamplingPoints * numberofSamplingPoints];
                dz = partialDerivativeEvals000[i + k * numberofSamplingPoints + j * numberofSamplingPoints * numberofSamplingPoints];

                cornerVal += permeabilityValues[k + j * numberofSamplingPoints + i * numberofSamplingPoints * numberofSamplingPoints] * (dx + dy + dz);

            }
        }
    }

    return cornerVal;
#else
    assertionMsg(false, "not implemented yet");
    return 0.0;
#endif
}


double mg::BackgroundStencil::getPartialDerivativeEval(point p, int dim, bool xpos, bool ypos, bool zpos, double h)
{
    //point is the location in [0,cell_h]x[0,cell_h]x[0,cell_h] if cell point originally from was translated to origin
    //xpos,ypos,zpos are a boolean flag to check positivity of x,y,z term in basis function
    //	-term either of form x/h or (1-x/h) as trilinear
    //h is the spacing of the quadrature NOT the cell size!
    //dim is the dimension the partial derivative is being differentiated with respect to
    //	-0 is x, 1 is y, 2 is z

    double values[] = {p.x, p.y, p.z};
    bool varstate[] = {xpos, ypos, zpos};
    double total = 1.0;
    for(int i = 0; i < 3; i++)
    {
        //for dimension differentiating w.r.t don't want to multiply by that in trilinear product
        //check that it's not that dimension
        if(i != dim)
        {
            if(varstate[i])
            {
                total *= values[i] / h;
            }
            else
            {
                total *= (1.0 - values[i] / h);
            }
            //if it is that dimension then want to multiply by derivative of that
        }
        else
        {
            if(varstate[i])
            {
                total *= 1.0 / h;
            }
            else
            {
                total *= -1.0 / h;
            }
        }
    }
    return total;
}


double mg::BackgroundStencil::getPermeabilityD(double x, double y, double z)
{
    tarch::la::Vector<DIMENSIONS, double> vector;

#ifdef Dim3
    vector = x, y, z;
#else
    vector = x, y;
#endif

    return mg::mappings::GenerateStencil::getEpsilon(vector);

}

double mg::BackgroundStencil::getPartialDerivativeEvalAnalytic(point p, int dim, bool xpos, bool ypos, bool zpos, double h)
{
    //point is the location in [0,cell_h]x[0,cell_h]x[0,cell_h] if cell point originally from was translated to origin
    //xpos,ypos,zpos are a boolean flag to check positivity of x,y,z term in basis function
    //	-term either of form x/h or (1-x/h) as trilinear
    //h is the spacing of the quadrature NOT the cell size!
    //dim is the dimension the partial derivative is being differentiated with respect to
    //	-0 is x, 1 is y, 2 is z

    //double values[] = {p.x,p.y,p.z};
    double lowerBound[] = {p.x - h / 2.0, p.y - h / 2.0, p.z - h / 2.0};
    double upperBound[] = {p.x + h / 2.0, p.y + h / 2.0, p.z + h / 2.0};
    bool varstate[] = {xpos, ypos, zpos};
    double total = 1.0;
    for(int i = 0; i < 3; i++)
    {
        //for dimension differentiating w.r.t don't want to multiply by that in trilinear product
        //check that it's not that dimension
        if(i != dim)
        {
            //total *= values[i]/h;
            if(varstate[i])
            {
                //total *= values[i]/h;
                total *= (upperBound[i] * upperBound[i] * upperBound[i] - lowerBound[i] * lowerBound[i] * lowerBound[i]) / 3.0;
            }
            else
            {
                //total *= (1.0-values[i]/h);
                total *= (upperBound[i] * upperBound[i] - lowerBound[i] * lowerBound[i]) / 2.0 - (upperBound[i] * upperBound[i] * upperBound[i] - lowerBound[i] * lowerBound[i] * lowerBound[i]) / 3.0;
            }
            //if it is that dimension then want to multiply by derivative of that
        }
        else
        {
            //total *= 1.0/h;
            if(varstate[i])
            {
                //total *= 1.0/h;
                total *= h;
            }
            else
            {
                //total *= -1.0/h;
                total *= -1.0 * h;
            }
        }
    }
    return total;
}
