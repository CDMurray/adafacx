#include "mg/State.h"
#include "mg/Cell.h"
#include "mg/Vertex.h"

#include "peano/grid/Checkpoint.h"



mg::State::State():
    Base()
{
    // @todo Insert your code here
}


mg::State::State(const Base::PersistentState &argument):
    Base(argument)
{
    // @todo Insert your code here
}


void mg::State::writeToCheckpoint( peano::grid::Checkpoint<mg::Vertex, mg::Cell> &checkpoint ) const
{
    // @todo Insert your code here
}


void mg::State::readFromCheckpoint( const peano::grid::Checkpoint<mg::Vertex, mg::Cell> &checkpoint )
{
    // @todo Insert your code here
}


void mg::State::setPointList(std::list<point> point_list)
{
    //_stateData.setPoint_list(point_list);
    this->point_list = point_list;
}


std::list<point> mg::State::getPointList() const
{
    return point_list;
}


void mg::State::setMaxDif(double maxDif)
{
    this->maxDif = maxDif;

}

void mg::State::setMaxDif(double maxX, double maxY, double maxZ)
{
    this->xdif = maxX;
    this->ydif = maxY;
    this->zdif = maxZ;

}

void mg::State::setMin(double minX, double minY, double minZ)
{
    this->min_x = minX;
    this->min_y = minY;
    this->min_z = minZ;

}

double mg::State::getMaxDif()
{
    return maxDif;
}

point mg::State::getMaxDifPoint() const
{
    struct point max;
    max.x = xdif;
    max.y = ydif;
    max.z = zdif;
    max.val = 0;
    return max;
}

point mg::State::getMin() const
{
    struct point min;
    min.x = min_x;
    min.y = min_y;
    min.z = min_z;
    min.val = 0;
    return min;
}



double mg::State::getL2Norm() const
{
    return _stateData.getL2norm();
}

double mg::State::getLinfNorm() const
{
    return _stateData.getLinfNorm();
}

double mg::State::getGlobalL2Norm() const
{
    return _stateData.getGlobalL2norm();
}

double mg::State::getGlobalLinfNorm() const
{
    return _stateData.getGlobalLinfNorm();
}


double mg::State::getPointNorm() const
{
    return _stateData.getPointNorm();
}
double mg::State::getPointNorm2() const
{
    return _stateData.getPointNorm2();
}

bool mg::State::getStencilConverged() const
{
    return _stateData.getStencilConverged();
}
void mg::State::setL2Norm(double updatedNorm)
{
    return _stateData.setL2norm(updatedNorm);
}
void mg::State::setLinfNorm(double updatedNorm)
{
    return _stateData.setLinfNorm(updatedNorm);
}
void mg::State::setGlobalL2Norm(double updatedNorm)
{
    return _stateData.setGlobalL2norm(updatedNorm);
}
void mg::State::setGlobalLinfNorm(double updatedNorm)
{
    return _stateData.setGlobalLinfNorm(updatedNorm);
}
void mg::State::setPointNorm(double updatedNorm)
{
    return _stateData.setPointNorm(updatedNorm);
}
void mg::State::setPointNorm2(double updatedNorm)
{
    return _stateData.setPointNorm2(updatedNorm);
}
void mg::State::setStencilConverged(bool hasConverged)
{
    return _stateData.setStencilConverged(hasConverged);
}

bool mg::State::getFirstIter() const
{
    return _stateData.getFirstIter();
}

bool mg::State::getActivelyProjecting() const
{
    return _stateData.getActivelyProjecting();
}
void mg::State::setActivelyProjecting(bool active)
{
    _stateData.setActivelyProjecting(active);
}
void mg::State::setFirstIter(bool firstIter)
{
    _stateData.setFirstIter(firstIter);
}

bool mg::State::getAllDiagonallyDominant() const
{
    return _stateData.getDiagonallyDominant();
}
void mg::State::setDiagonallyDominant(bool diagonallyDominant)
{
    _stateData.setDiagonallyDominant(diagonallyDominant);
}

bool mg::State::useAlternativeDamping()
{
    return _stateData.getUseAlternateSmoothing();
}
void mg::State::setAlternativeDamping(bool useAlternativeDamping)
{
    _stateData.setUseAlternateSmoothing(useAlternativeDamping);
}
bool mg::State::getPerformRefine()
{
    return _stateData.getPerformRefine();
}
void mg::State::setPerformRefine(bool performRefine)
{
    _stateData.setPerformRefine(performRefine);
}
void mg::State::switchPerformRefine()
{
    _stateData.setPerformRefine(!_stateData.getPerformRefine());
}


bool mg::State::justRefined()
{
    return _stateData.getMadeRefinement();
}
void mg::State::setRecentRefine(bool recentRefine)
{
    _stateData.setMadeRefinement(recentRefine);
}


double mg::State::getSpectralNorm()
{
    return _stateData.getSpectralNorm();
}

double mg::State::getRowNorm()
{
    return _stateData.getRowNorm();
}

double mg::State::getFrobeniusNorm()
{
    return _stateData.getFrobeniusNorm();
}

void mg::State::setSpectralNorm(double updatedNorm)
{
    return _stateData.setSpectralNorm(updatedNorm);
}
void mg::State::setRowNorm(double updatedNorm)
{
    return _stateData.setRowNorm(updatedNorm);
}
void mg::State::setFrobeniusNorm(double updatedNorm)
{
    return _stateData.setFrobeniusNorm(updatedNorm);
}

bool mg::State::getUpdateCoarse() const
{
    return _stateData.getUpdateCoarse();
}
void mg::State::setUpdateCoarse(bool updateCoarse)
{
    _stateData.setUpdateCoarse(updateCoarse);
}

