// This file originally was created by pdt (Peano Development Toolkit) as part
// of a code based upon the Peano project by Tobias Weinzierl. For conditions
// of distribution and use of this project, please see the copyright notice at
// www.peano-framework.org. Feel free to adopt the license and authorship of
// this file and your project to your needs as long as the license is in
// agreement with the original Peano user constraints. A reference to/citation
// of  Peano and its author is highly appreciated.
#ifndef _MG_STATE_H_
#define _MG_STATE_H_

#include "mg/records/State.h"
#include "peano/grid/State.h"

#include "peano/grid/Checkpoint.h"

#include <list>
#include "mg/point.h"


namespace mg
{
    class State;
    /**
     * Forward declaration
     */
    class Vertex;
    /**
     * Forward declaration
     */
    class Cell;

    namespace repositories
    {
        /**
         * Forward declaration
         */
        class RepositoryArrayStack;
        class RepositorySTDStack;
    }
}


/**
 * Blueprint for solver state.
 *
 * This file has originally been created by the PDT and may be manually extended to
 * the needs of your application. We do not recommend to remove anything!
 */
class mg::State: public peano::grid::State< mg::records::State >
{
private:
    typedef class peano::grid::State< mg::records::State >  Base;

    /**
     * Needed for checkpointing.
     */
    friend class mg::repositories::RepositoryArrayStack;
    friend class mg::repositories::RepositorySTDStack;

    void writeToCheckpoint( peano::grid::Checkpoint<Vertex, Cell>  &checkpoint ) const;
    void readFromCheckpoint( const peano::grid::Checkpoint<Vertex, Cell>  &checkpoint );



    std::list<point> point_list;


    double xdif;
    double ydif;
    double zdif;
    double maxDif;
    double min_x;
    double min_y;
    double min_z;

public:
    /**
     * Default Constructor
     *
     * This constructor is required by the framework's data container. Do not
     * remove it.
     */
    State();

    /**
     * Constructor
     *
     * This constructor is required by the framework's data container. Do not
     * remove it. It is kind of a copy constructor that converts an object which
     * comprises solely persistent attributes into a full attribute. This very
     * functionality is implemented within the super type, i.e. this constructor
     * has to invoke the correponsing super type's constructor and not the super
     * type standard constructor.
     */
    State(const Base::PersistentState &argument);

    void setPointList(std::list<point> point_list);

    std::list<point> getPointList() const;

    void setMaxDif(double max);

    void setMaxDif(double minX, double minY, double minZ);

    void setMin(double minX, double minY, double minZ);

    double getMaxDif();

    point getMin() const;

    point getMaxDifPoint() const;

    double getL2Norm() const;

    double getLinfNorm() const;

    double getGlobalL2Norm() const;

    double getGlobalLinfNorm() const;

    double getPointNorm() const;

    double getPointNorm2() const;

    bool getStencilConverged() const;

    void setL2Norm(double updatedNorm);

    void setLinfNorm(double updatedNorm);

    void setGlobalL2Norm(double updatedNorm);

    void setGlobalLinfNorm(double updatedNorm);

    void setPointNorm(double updatedNorm);

    void setPointNorm2(double updatedNorm);


    bool useAlternativeDamping();
    void setAlternativeDamping(bool hasConverged);

    void setActivelyProjecting(bool active);
    bool getActivelyProjecting() const;

    bool getPerformRefine();
    void setPerformRefine(bool performRefine);
    void switchPerformRefine();

    bool justRefined();
    void setRecentRefine(bool recentRefine);


    void setSpectralNorm(double updatedNorm);

    void setRowNorm(double updatedNorm);

    void setFrobeniusNorm(double updatedNorm);


    double getSpectralNorm();

    double getRowNorm();

    double getFrobeniusNorm();

    void setFirstIter(bool firstIter);

    bool getFirstIter() const;

    bool getUpdateCoarse() const;
    void setUpdateCoarse(bool updateCoarse);


    bool getAllDiagonallyDominant() const;
    void setDiagonallyDominant(bool diagonallyDominant);



    void setStencilConverged(bool hasConverged);
};


#endif
