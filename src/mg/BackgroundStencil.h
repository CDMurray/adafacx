
#ifndef _MG_BACKGROUNDSTENCIL_H_
#define _MG_BACKGROUNDSTENCIL_H_


#include "mg/point.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"
#include "mg/State.h"
#include <list>
#include <vector>

#include "tarch/multicore/BooleanSemaphore.h"
#include "tarch/multicore/Lock.h"

#include "peano/heap/Heap.h"

#include "matrixfree/solver/Multigrid.h"


#include "mg/mappings/GenerateStencil.h"

namespace mg
{
    class BackgroundStencil;
    typedef peano::heap::PlainDoubleHeapWithDaStGenRecords DataHeap;
    typedef peano::heap::PlainBooleanHeap BoolHeap;
    typedef peano::heap::RLEIntegerHeap IntHeap;

}

class mg::BackgroundStencil
{
private:
    int _heapIndex;

    std::list<point> _point_list;
    matrixfree::solver::Multigrid _multigrid;
    double _min_x;
    double _min_y;
    double _min_z;
    double _maxDif;
    double _xdif;
    double _ydif;
    double _zdif;

    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> _stencil;
    double _cellX;
    double _cellY;
    double _cellZ;
    double _h;
    int _numberofSamplingPoints;
    bool _finished;

    double centreNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_zpos, const int numberofSamplingPoints);
    double faceNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_zpos, bool vert_xposf, bool vert_yposf, bool vert_zposf, const int numberofSamplingPoints);
    double edgeNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_zpos, bool vert_xpose, bool vert_ypose, bool vert_zpose, const int numberofSamplingPoints);
    double cornerNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_zpos, const int numberofSamplingPoints);
    double getPartialDerivativeEval(point p, int dim, bool xpos, bool ypos, bool zpos, double h);
    double getPartialDerivativeEvalAnalytic(point p, int dim, bool xpos, bool ypos, bool zpos, double h);
    double getPermeabilityD(double xpos, double ypos, double zpos);

public:
    /**
     * @param solverState The solver is only read out and not temporarily stored.
     */
    BackgroundStencil(const mg::State &solverState);
    //virtual ~BackgroundStencil();


    /*enum class Problem {
      Undef, Poisson, JumpingCoefficient
    };

    static Problem  problem;*/

    //double getEpsilon( const tarch::la::Vector<DIMENSIONS,double>&  fineGridX );

    void init(mg::State &solverState, int numberOfSamplingPoints, int heapIndex);
    bool operator()();
    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> getStencil();
    int getNumPoints();
    void setCellData(double cellX, double cellY, double cellZ, double h);
    bool isFinished();
    void resetFinished();

    static tarch::multicore::BooleanSemaphore _mysemaphore;
};

#endif