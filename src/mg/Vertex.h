// This file originally was created by pdt (Peano Development Toolkit) as part
// of a code based upon the Peano project by Tobias Weinzierl. For conditions 
// of distribution and use of this project, please see the copyright notice at
// www.peano-framework.org. Feel free to adopt the license and authorship of 
// this file and your project to your needs as long as the license is in 
// agreement with the original Peano user constraints. A reference to/citation  
// of  Peano and its author is highly appreciated.
#ifndef _MG_VERTEX_H_ 
#define _MG_VERTEX_H_


#include "mg/records/Vertex.h"
#include "peano/grid/Vertex.h"
#include "peano/grid/VertexEnumerator.h"
#include "peano/utils/Globals.h"


namespace mg { 
      class Vertex; 
      
      /**
       * Forward declaration
       */ 
      class VertexOperations;
}


/**
 * Blueprint for grid vertex.
 * 
 * This file has originally been created by the PDT and may be manually extended to 
 * the needs of your application. We do not recommend to remove anything!
 */
class mg::Vertex: public peano::grid::Vertex< mg::records::Vertex > { 
  private: 
    typedef class peano::grid::Vertex< mg::records::Vertex >  Base;

    friend class VertexOperations;
  public:
    static double FineGridOmega;
    //static double FineGridOmegaAlt;
    static double AdditionalOmegaWeighting;

    enum class AlternativeRestriction {
      Undef, Injection, HalfWeighting, ReducedKernelFullWeighting, FullWeighting, PiecewiseConstant, Smoothed,SmoothedPartial, DampedHalf, AltHalf, AltSmoothed
    };

    static AlternativeRestriction altRestriction;

    /**
     * Default Constructor
     *
     * This constructor is required by the framework's data container. Do not 
     * remove it.
     */
    Vertex();
    
    /**
     * This constructor should not set any attributes. It is used by the 
     * traversal algorithm whenever it allocates an array whose elements 
     * will be overwritten later anyway.  
     */
    Vertex(const Base::DoNotCallStandardConstructor&);
    
    /**
     * Constructor
     *
     * This constructor is required by the framework's data container. Do not 
     * remove it. It is kind of a copy constructor that converts an object which 
     * comprises solely persistent attributes into a full attribute. This very 
     * functionality is implemented within the super type, i.e. this constructor 
     * has to invoke the correponsing super type's constructor and not the super 
     * type standard constructor.
     */
    Vertex(const Base::PersistentVertex& argument);


	
	void init(const tarch::la::Vector<DIMENSIONS,double>& x,double phi);
	void reset();
	void setF(double f);
	void setFI(double f);

	void backupCurrentSolution();

	/**
	 * Update solution does reset
	 */
	void updateSolution();
	void updateRes(double h);
	void updateRes();

	double getRes() const;
	double getUpdatedRes() const;
	double getTempRes() const;
	//double getTempF() const;
	//double getTempD() const;
	double getResI() const;
	double getUpdatedResI() const;
	//double getResDiff() const;
	//double getResTwo() const;
	double getU() const;
	//double getUOld() const;
	double getUH() const;
	//double getPrevPhi() const;
	double getUI() const;
	//double getUHI() const;
	//double getUTwo() const;
	double getResH() const;
	double getUpdatedResH() const;
	//double getResHI() const;
	//double getPhiH() const;
	//double getDiff() const;
	//double getDiffI() const;
	//double getK() const;
	double getF() const;
	double getFI() const;
	//double getD() const;
	//double getDI() const;
	//double getCorrection() const;
	//double getCoarseCorrection() const;
	//double getFineCorrection() const;
	double getLocalCorrection() const;
	//double getCorrectionI() const;
	//double getCoarseCorrectionI() const;
	//double getFineCorrectionI() const;
	double getLocalCorrectionI() const;
	double getBPXCorrection() const;
	//double getCorrectionTwo() const;
	//double getLocalCorrectionTwo() const;
	//double getTempCorrections() const;
	void setU(double newU);
	//void setUTwo(double newU);
	//void updateUOld();
	//void setPrevPhi(double newPhi);
	void setUI(double newU);
	void setUH(double newU);
	//void setUHI(double newU);
	//void setPrevPhi(double newPhi);
	//void setPrevPhiH(double newPhi);
	//void setDiff(double newDiff);
	//void setDiffI(double newDiff);
	void setResI(double newRes);
	//void setD(double newD);
	//void setDI(double newD);
	//void getDiff() const;
	//void getDiffI() const;
	//void setK(double newK);
	//void setCorrection(double newCorrection);
	//void setCoarseCorrection(double newCorrection);
	//void setFineCorrection(double newCorrection);
	void setLocalCorrection(double newCorrection);
	//void setCorrectionI(double newCorrection);
	//void setCoarseCorrectionI(double newCorrection);
	//void setFineCorrectionI(double newCorrection);
	void setLocalCorrectionI(double newCorrection);
	void setBPXCorrection(double newCorrection);
	//void setCorrectionTwo(double newCorrection);
	//void setLocalCorrectionTwo(double newCorrection);
	//void setTempCorrection(double newCorrection);

	double getPotentialUpdate();
	double getPotentialUpdateI();
	//double getPotentialUpdateTwo();


	//double getPotentialUpdateAlt();
	//double getPotentialUpdateIAlt();

	double getPotentialUpdateIDistinctOmega();
	//double getPotentialUpdateIAltDistinctOmega();

	double getRefinementCondition() const;
	void resetRefinementCondition();


	bool isStencilDiagonallyDominant();

	bool isDifferentToUpdate();

	void incrementSamplingPoints();
	bool canAnalyseForRefine();
	double getSamplingPoints();
	void setAnalyseForRefine(bool newValue);
	//double getChange() const;


	//void backupD();
	//void backupDI();


	void setParent(bool parent);
	bool getParent() const;
	
	/*void setInterpolatedValue(tarch::la::Vector<8, double> intVals);
	void setEffectedValue(tarch::la::Vector<8, double> effVals);
	void incEffectedValues(tarch::la::Vector<8, double> effVals);

	void resetEffectedValue();*/
	void initOmega(double newOmega);	

	void setOmega(double newOmega);	
	void resetOmega();	
	double getOmega() const;

	//void setOmegaAlt(double newOmega);	
	//double getOmegaAlt() const;

	void setOmegaI(double newOmega);	
	double getOmegaI() const;

	//void setOmegaIAlt(double newOmega);	
	//double getOmegaIAlt() const;

	void resetAccumulatedValues();
	void resetC();
	void resetLocalCorrections();
	//void resetCoarseCorrections();
	//void resetFineCorrections();
	void resetLocalCorrectionsI();
	//void resetCoarseCorrectionsI();
	//void resetFineCorrectionsI();
	void resetBPXCorrections();
	//void resetLocalCorrectionsTwo();
	//void resetTempCorrections();
	void setRes(double res);
	void setTempRes(double res);
	//void setTempD(double d);
	//void setTempF(double f);
	void incAccumulatedValue(double accVal, int x, int y, int z);
	//void incAccumulatedValueI(double accVal, int x, int y, int z);
	//void incAccumulatedValueTranspose(double accVal, int x, int y, int z);
	//void setAccumulatedValueTranspose(double accVal, int x, int y, int z);
	//void setAccumulatedValueTranspose(double accVal, int index);

	tarch::la::Vector<THREE_POWER_D, double> getAccumulatedValue() const;
	//tarch::la::Vector<THREE_POWER_D, double> getAccumulatedValueI() const;
	//tarch::la::Vector<THREE_POWER_D, double> getAccumulatedValueTranspose() const;

	void updateStencil();
	//void updateStencilI();
	//void updateStencilFine();
	//void updateStencilFine(double h);
	//void updateStencilIFine();
	//void updateStencilIFine(double h);
	//void updateStencilTranspose();

	tarch::la::Vector<THREE_POWER_D, double> getStencil() const;
	//tarch::la::Vector<THREE_POWER_D, double> getStencilI() const;
	//tarch::la::Vector<THREE_POWER_D, double> getStencilTranspose() const;

	tarch::la::Vector<FIVE_POWER_D, double> getRestriction() const;
	//tarch::la::Vector<FIVE_POWER_D, double> getProlongation() const;
	tarch::la::Vector<FIVE_POWER_D, double> getRestrictionI() const;
	//tarch::la::Vector<FIVE_POWER_D, double> getProlongationI() const;

	//tarch::la::Vector<FIVE_POWER_D, double> getDelta() const;

	double getRestrictionElement(int x, int y, int z);
	//double getProlongationElement(int x, int y, int z);
	double getRestrictionElementI(int x, int y, int z);
	//double getProlongationElementI(int x, int y, int z);
	double getDummyProlongationElement(int x, int y, int z);
	double getRestrictionElementIBoundary(int x, int y, int z);
	void initialiseDLinearRestriction();
	//void initialiseDLinearProlongation();
	void initialiseRestrictionI();
	//void initialiseProlongationI();
	void initialiseRestrictionIAlt();
	void initialiseRestrictionIAlt2();
	//tarch::la::Vector<27, double> getSpaceStencil();
	void setStencil(tarch::la::Vector<THREE_POWER_D, double> stencil);
	//void setStencilI(tarch::la::Vector<THREE_POWER_D, double> stencil);
	void setRestriction(tarch::la::Vector<FIVE_POWER_D, double> r);
	void setRestrictionIMG(tarch::la::Vector<FIVE_POWER_D, double> r);
	//void setProlongation(tarch::la::Vector<FIVE_POWER_D, double> p);
	void setRestrictionI(tarch::la::Vector<FIVE_POWER_D, double> r);
	//void setProlongationI(tarch::la::Vector<FIVE_POWER_D, double> p);


	//void incDelta(tarch::la::Vector<FIVE_POWER_D, double> delta);
	//void incDelta(tarch::la::Vector<FIVE_POWER_D, double> delta);

	void resetDeltas();


	void updateBoxMGRestriction();

	bool isFirstTouch();
	void resetFirstTouch();
	void madeFirstTouch();

	void setAccVal(tarch::la::Vector<THREE_POWER_D, double> stencil);
	//void setAccValI(tarch::la::Vector<THREE_POWER_D, double> stencil);

	void incRestrictionAccVal(double accVal, int x, int y, int z);
	void resetRestrictionAccVal();
	void updateRestrictionI();

	bool isConverged();
	void setConverged(bool _isConverged);
	//bool getTempConverged();

	//void setTempConverged(bool tempConverged);
	//void updateConvergence();

	double getError(const tarch::la::Vector<DIMENSIONS,double>& fineGridX,const tarch::la::Vector<DIMENSIONS,double>& fineGridH) const;
	double getRelativeError(const tarch::la::Vector<DIMENSIONS,double>& fineGridX,const tarch::la::Vector<DIMENSIONS,double>& fineGridH) const;


	//double getStencilIndexOffset(double x, double y, double z);
	double getStencilIndexOffset(int x, int y, int z);
	//double getStencilIndexOffsetI(int x, int y, int z);


	//tarch::la::Vector<THREE_POWER_D, double> getStencilBuffer() const;
	//void prepBuffer();

	void zeroInterpolants();

	tarch::la::Vector<SEVEN_POWER_D, double> getRestrictionElementIStencil();


};


#endif
