#include "mg/mappings/GenerateStencil.h"
#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"

#include "matrixfree/stencil/StencilFactory.h"


#include "mg/mappings/CreateGrid.h"
#include "mg/mappings/BStencil.h"


#include <cmath>

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::GenerateStencil::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::GenerateStencil::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::GenerateStencil::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::GenerateStencil::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::GenerateStencil::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::GenerateStencil::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::GenerateStencil::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


tarch::logging::Log                     mg::mappings::GenerateStencil::_log( "mg::mappings::GenerateStencil" );
mg::mappings::GenerateStencil::Problem  mg::mappings::GenerateStencil::problem  = mg::mappings::GenerateStencil::Problem::Undef;
double                                  mg::mappings::GenerateStencil::epsilon = 1.0;
double                                  mg::mappings::GenerateStencil::gradient = 0.0;


mg::mappings::GenerateStencil::GenerateStencil()
{
    logTraceIn( "GenerateStencil()" );
    // @todo Insert your code here
    logTraceOut( "GenerateStencil()" );
}


mg::mappings::GenerateStencil::~GenerateStencil()
{
    logTraceIn( "~GenerateStencil()" );
    // @todo Insert your code here
    logTraceOut( "~GenerateStencil()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::GenerateStencil::GenerateStencil(const GenerateStencil  &masterThread):
    _localState(masterThread._localState)
{
    logTraceIn( "GenerateStencil(GenerateStencil)" );
    // @todo Insert your code here
    logTraceOut( "GenerateStencil(GenerateStencil)" );
}


void mg::mappings::GenerateStencil::mergeWithWorkerThread(const GenerateStencil &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(GenerateStencil)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithWorkerThread(GenerateStencil)" );
}
#endif


void mg::mappings::GenerateStencil::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::GenerateStencil::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::GenerateStencil::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::GenerateStencil::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::GenerateStencil::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::GenerateStencil::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::GenerateStencil::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::GenerateStencil::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::GenerateStencil::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::GenerateStencil::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::GenerateStencil::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::GenerateStencil::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::GenerateStencil::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::GenerateStencil::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::GenerateStencil::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::GenerateStencil::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::GenerateStencil::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::GenerateStencil::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::GenerateStencil::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif


double mg::mappings::GenerateStencil::getEpsilon( const tarch::la::Vector<DIMENSIONS, double>  &fineGridX )
{
    switch (problem)
    {
    case Problem::Undef:
        return 0.0;
        break;
    case Problem::Poisson:
        return 1.0;
        break;
    case Problem::JumpingCoefficient:
    {
        if (fineGridX(0) - gradient * fineGridX(1) < 0.5)
        {
            return epsilon;
        }
        else return 1.0;
        break;
    }



    case Problem::Checkerboard:
    {
        if ((fineGridX(0) - gradient * fineGridX(1) < 0.5) && (fineGridX(1) - gradient * fineGridX(0) < 0.5))
        {
            return epsilon;
        }
        else if ((fineGridX(0) - gradient * fineGridX(1) >= 0.5) && (fineGridX(1) - gradient * fineGridX(0) >= 0.5))
        {
            return epsilon;
        }
        else return 1.0;
    }
    break;
    case Problem::Circle:
    {
        if((fineGridX(0) - 0.5) * (fineGridX(0) - 0.5) + (fineGridX(1) - 0.5) * (fineGridX(1) - 0.5) < 0.0625)
        {
            return epsilon;
        }
        else
        {
            return 1.0;
        }
    }
    break;
    case Problem::Line:
    {
        double slotSize = 0.0051;
        if ( (fineGridX(0) - gradient * fineGridX(1) < 0.5 + slotSize) && (fineGridX(0) - gradient * fineGridX(1) > 0.5 - slotSize) && (fineGridX(1) < 0.5) )
        {
            return epsilon;
        }
        else
        {
            return 1.0;
        }
    }
    break;
    case Problem::Anisotropic:
        return 1.0;
        break;
    case Problem::Sinusoidal:
    {
        return 0.3 * sin(1.0 / fineGridX(1)) + 1.0;
        break;
    }
    case Problem::Perlin:
    {
        /*unsigned int seed = 237;
        mg::PerlinNoise pn(seed);
        return 1e4 * pn.noise(1e4 * fineGridX(0), 1e4 * fineGridX(1), 0.8);*/
        return 1.0;
        break;
    }
    case Problem::PerlinStarvation:
    {
        /*unsigned int seed = 237;
        mg::PerlinNoise pn(seed);
        return 1e4 * pn.noise(1e8 * fineGridX(0), 1e8 * fineGridX(1), 0.8);*/
        return 1.0;
        break;
    }
    }
    return 1.0;
}


tarch::la::Vector<DIMENSIONS, double> mg::mappings::GenerateStencil::getAnisotropicEpsilon( const tarch::la::Vector<DIMENSIONS, double>  &fineGridX )
{
    tarch::la::Vector<DIMENSIONS, double> epsilon;
#ifdef Dim2
    epsilon = 1.0, 1.0;
#elif Dim3
    epsilon = 1.0, 1.0, 1.0;
#endif

    for(int i = 0; i < DIMENSIONS; i++)
    {
        if(i == 0)
        {

            epsilon(i) = mg::mappings::GenerateStencil::epsilon;
        }
        else
        {
            epsilon(i) = 1.0;
        }
    }
    return epsilon;
}

tarch::la::Vector<THREE_POWER_D, double> mg::mappings::GenerateStencil::getAnisotropicStencil(const tarch::la::Vector<DIMENSIONS, double>  &fineGridX)
{
    tarch::la::Vector<DIMENSIONS, double> epsilon;
    epsilon = getAnisotropicEpsilon(fineGridX);

    tarch::la::Vector<THREE_POWER_D, double> stencil;


#ifdef Dim2
    tarch::la::Vector<THREE_POWER_D, double> xstencil;
    tarch::la::Vector<THREE_POWER_D, double> ystencil;
    xstencil = matrixfree::stencil::stencilProduct(matrixfree::stencil::get1DLaplaceStencil(), matrixfree::stencil::get1DMassStencil());
    ystencil = matrixfree::stencil::stencilProduct(matrixfree::stencil::get1DMassStencil(), matrixfree::stencil::get1DLaplaceStencil());
    stencil = -1.0 * (epsilon(0) * xstencil + epsilon(1) * ystencil);
#elif Dim3
    tarch::la::Vector<THREE_POWER_D, double> xstencil;
    tarch::la::Vector<THREE_POWER_D, double> ystencil;
    tarch::la::Vector<THREE_POWER_D, double> zstencil;
    xstencil = matrixfree::stencil::stencilProduct(matrixfree::stencil::get1DLaplaceStencil(), matrixfree::stencil::get1DMassStencil(), matrixfree::stencil::get1DMassStencil());
    ystencil = matrixfree::stencil::stencilProduct(matrixfree::stencil::get1DMassStencil(), matrixfree::stencil::get1DLaplaceStencil(), matrixfree::stencil::get1DMassStencil());
    zstencil = matrixfree::stencil::stencilProduct(matrixfree::stencil::get1DMassStencil(), matrixfree::stencil::get1DMassStencil(), matrixfree::stencil::get1DLaplaceStencil());
    stencil = -1.0 * (epsilon(0) * xstencil + epsilon(1) * ystencil + epsilon(2) * zstencil);
#endif

    return stencil;
}


void mg::mappings::GenerateStencil::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );

    fineGridVertex.resetAccumulatedValues();


    tarch::la::Vector<THREE_POWER_D, double> stencil;

    switch(problem)
    {
    case Problem::Poisson:
    case Problem::JumpingCoefficient:
    case Problem::Checkerboard:
    case Problem::Circle:
    case Problem::Line:
    case Problem::Sinusoidal:
    case Problem::Perlin:
    case Problem::PerlinStarvation:
    {
#ifdef Dim2
        double h = fineGridH(0);
        tarch::la::Vector<DIMENSIONS, double> xOffset1;
        xOffset1(0) = h / 2.0;
        xOffset1(1) = h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> yOffset1;
        yOffset1(0) = -h / 2.0;
        yOffset1(1) = h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> xOffset2;
        xOffset2(0) = h / 2.0;
        xOffset2(1) = -h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> yOffset2;
        yOffset2(0) = -h / 2.0;
        yOffset2(1) = -h / 2.0;



        tarch::la::Vector<DIMENSIONS, double> pos1 = fineGridX + xOffset1;
        double eps1 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos2 = fineGridX + xOffset2;
        double eps2 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos3 = fineGridX + yOffset1;
        double eps3 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos4 = fineGridX + yOffset2;
        double eps4 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * yOffset2));

        //elementwise contributions
        tarch::la::Vector<THREE_POWER_D, double> stencil0;
        stencil0 = -1.0, -0.5, 0.0, -0.5, 2.0, 0.0, 0.0, 0.0, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil1;
        stencil1 = 0.0, -0.5, -1.0, 0.0, 2.0, -0.5, 0.0, 0.0, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil2;
        stencil2 = 0.0, 0.0, 0.0, -0.5, 2.0, 0.0, -1.0, -0.5, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil3;
        stencil3 = 0.0, 0.0, 0.0, 0.0, 2.0, -0.5, 0.0, -0.5, -1.0;



        stencil0 *= eps4 * (1.0 / 3.0);
        stencil1 *= eps2 * (1.0 / 3.0);
        stencil2 *= eps3 * (1.0 / 3.0);
        stencil3 *= eps1 * (1.0 / 3.0);


        stencil = stencil0 + stencil1 + stencil2 + stencil3;

#elif Dim3
        stencil = 1.0 * mg::mappings::GenerateStencil::getEpsilon(fineGridX) * fineGridH(1) * matrixfree::stencil::get3DLaplaceStencil();
#endif
    }
    break;
    case Problem::Anisotropic:
#ifdef Dim2
        stencil = (-1.0) * getAnisotropicStencil(fineGridX);
#elif Dim3
        stencil = fineGridH(1) * getAnisotropicStencil(fineGridX);
#endif
        break;
    case Problem::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }
    fineGridVertex.setStencil(stencil);
    //fineGridVertex.setD(stencil(THREE_POWER_D / 2));
    //fineGridVertex.setTempD(stencil(THREE_POWER_D / 2));

    /*if(fineGridVertex.isInside() && (fineGridVertex.getRefinementControl() == Vertex::Records::Refined))
    {
        fineGridVertex.setStencilI(stencil);
        fineGridVertex.setDI(stencil(THREE_POWER_D / 2));
        fineGridVertex.backupDI();

    }*/


    tarch::la::Vector<THREE_POWER_D, double> stencilI;


    fineGridVertex.resetAccumulatedValues();
    //for time being this is outside here as need it for AMG stuff

    if(fineGridVertex.isInside())
    {
        fineGridVertex.initialiseDLinearRestriction();
        fineGridVertex.initialiseRestrictionI();

    }
    else
    {
        fineGridVertex.zeroInterpolants();
    }
    fineGridVertex.initialiseDLinearRestriction();
    //fineGridVertex.initialiseDLinearProlongation();
    //fineGridVertex.initialiseProlongationI();

    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::GenerateStencil::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) )
    {
        const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setUI(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setResI(  0.0 );
    }
    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::GenerateStencil::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );

    //needed so that the background stencil integration has a baseline to compare results to after first iteration (otherwise will perform three stencil integrations when stencil converges to correct value immediately)
    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> stencil;

    matrixfree::stencil::VectorOfStencils stencils;
    dfor2(k)
    tarch::la::slice(
        stencils, // into stencils
        tarch::la::Vector<THREE_POWER_D, double>(fineGridVertices[ fineGridVerticesEnumerator(k) ].getStencil()),
        kScalar * THREE_POWER_D
    );
    enddforx

    stencil = matrixfree::stencil::getElementWiseAssemblyMatrix(stencils);

    tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, double> out;
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            out(TWO_POWER_D * i + j) = stencil(i, j);
        }
    }
    fineGridCell.setStencil(out);



    switch(mg::mappings::BStencil::fineStencilType)
    {
    case mg::mappings::BStencil::FineStencilType::Static:
    case mg::mappings::BStencil::FineStencilType::StaticWithCoarse:
        break;
    case mg::mappings::BStencil::FineStencilType::Precompute:
    case mg::mappings::BStencil::FineStencilType::Background:
    case mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse:
    case mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse:
    {

#ifdef Dim2
        bool allVerticesRefined = true;
        dfor2(i)
        if(fineGridVertices[fineGridVerticesEnumerator(i)].getRefinementControl() == Vertex::Records::Unrefined)
        {
            allVerticesRefined = false;
        }
        enddforx
        if(!allVerticesRefined)
        {
            int heapIndex;

            if(mg::mappings::BStencil::fineStencilSimple == mg::mappings::BStencil::FineStencilSimple::Averaged)
            {

                tarch::multicore::Lock myLock(mg::BackgroundStencilSimple::_mysemaphore);
                heapIndex = DataHeap::getInstance().createData(16, 16);
                fineGridCell.setHeap(heapIndex);
                IntHeap::getInstance().createDataForIndex(heapIndex, 1, 1);
                IntHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = 2;

                BoolHeap::getInstance().createDataForIndex(heapIndex, 2, 2);
                BoolHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = false;
                BoolHeap::getInstance().getData(heapIndex)[1]._persistentRecords._u = false;
                myLock.free();

                BackgroundStencilSimple *task = new BackgroundStencilSimple(_localState);
                int samplingPoints = 2;
                task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
                task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), 0.0, fineGridVerticesEnumerator.getCellSize()(0));
                peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::IsTaskAndRunImmediately);

            }
            else
            {


                tarch::multicore::Lock myLock(mg::BackgroundStencil2D::_mysemaphore);
                heapIndex = DataHeap::getInstance().createData(16, 16);
                fineGridCell.setHeap(heapIndex);
                IntHeap::getInstance().createDataForIndex(heapIndex, 1, 1);
                IntHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = 2;

                BoolHeap::getInstance().createDataForIndex(heapIndex, 2, 2);
                BoolHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = false;
                BoolHeap::getInstance().getData(heapIndex)[1]._persistentRecords._u = false;
                myLock.free();

                BackgroundStencil2D *task = new BackgroundStencil2D(_localState);
                int samplingPoints = 2;
                //int samplingPoints = 3;
                task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
                task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), 0.0, fineGridVerticesEnumerator.getCellSize()(0));
                peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::IsTaskAndRunImmediately);

            }

#elif Dim3
        if(!fineGridCell.isRefined())
        {
            int heapIndex;
            tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
            heapIndex = DataHeap::getInstance().createData(64, 64);
            fineGridCell.setHeap(heapIndex);
            IntHeap::getInstance().createDataForIndex(heapIndex, 1, 1);
            IntHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = 2;

            BoolHeap::getInstance().createDataForIndex(heapIndex, 2, 2);
            BoolHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = false;
            BoolHeap::getInstance().getData(heapIndex)[1]._persistentRecords._u = false;
            myLock.free();
            BackgroundStencil *task = new BackgroundStencil(_localState);
            int samplingPoints = 2;
            task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
            task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), fineGridVerticesEnumerator.getCellCenter()(2), fineGridVerticesEnumerator.getCellSize()(0));
            peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::IsTaskAndRunAsSoonAsPossible);
        }
#endif
        }
        break;

    }
    case mg::mappings::BStencil::FineStencilType::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }


    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::GenerateStencil::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here



    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::GenerateStencil::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );
    // @todo Insert your code here
    struct point dif = solverState.getMaxDifPoint();
    _xdif = dif.x;
    _ydif = dif.y;
    _zdif = dif.z;
    struct point min = solverState.getMin();
    _min_x = min.x;
    _min_y = min.y;
    _min_z = min.z;

    _point_list = solverState.getPointList();

    _localState = solverState;

    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::GenerateStencil::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::GenerateStencil::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "descend(...)" );
}


void mg::mappings::GenerateStencil::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}

/*tarch::la::Matrix<4,4,double> mg::mappings::GenerateStencil::generateStencil(double xpos, double ypos, double h, int numberOfSamplingPoints){

  tarch::la::Matrix<4,4,double> stencil;
  bool outer_x = false, outer_y = false;
  //i is the vertex that storing stencil in (so always view as x/h and y/h as basis functions)
  for(int i =0; i < 4; i++){
    bool inner_x = false, inner_y = false;
    for(int j =0; j < 4; j++){
      int count = 0;
      if(outer_x==inner_x){
        count++;
      }
      if(outer_y==inner_y){
        count++;
      }
      double result = 0.0;
      switch(count){
        case 0:
          result = cornerNumericalIntegration(xpos,ypos,outer_x,outer_y,numberOfSamplingPoints,h);
          break;
        case 1:
          result = edgeNumericalIntegration(xpos,ypos,outer_x,outer_y,inner_x,inner_y,numberOfSamplingPoints,h);
          break;
        case 2:
          result = centreNumericalIntegration(xpos,ypos,outer_x,outer_y,numberOfSamplingPoints,h);
          break;
      }
      stencil(i,j) = result;
      if (inner_x){
        inner_y = !inner_y;
      }
      inner_x = !inner_x;
    }

    if (outer_x){
      outer_y = !outer_y;
    }
    outer_x = !outer_x;
  }
  return stencil;
}


//calculate the exact partial derivative integral centred around the value at point p
//in d-dim cube with side length h
double mg::mappings::GenerateStencil::getSquaredSum(double xpos, double ypos,double h){

  double lowerBound[] = {xpos-h/2.0,ypos-h/2.0};
  double upperBound[] = {xpos+h/2.0,ypos+h/2.0};
  double total = 0.0;
  total +=  (upperBound[0]*upperBound[0]*upperBound[0]-lowerBound[0]*lowerBound[0]*lowerBound[0])/3.0;
  total +=  (upperBound[1]*upperBound[1]*upperBound[1]-lowerBound[1]*lowerBound[1]*lowerBound[1])/3.0;
  total *= h;
  return total;
}


double mg::mappings::GenerateStencil::getVarInt(double upos, double h){
  double lowerBound = upos - h/2.0;
  double total = 2*lowerBound + h;
  total *= (h*h)/2.0;
  return total;
}

double mg::mappings::GenerateStencil::getCentreQuadrature(double xpos, double ypos,double h){
  return getSquaredSum(xpos,ypos,h);
}

double mg::mappings::GenerateStencil::getEdgeQuadrature(double xpos, double ypos,double h,bool xdirection){
  if(xdirection){
    return getVarInt(xpos,h) - getSquaredSum(xpos,ypos,h);
  } else{
    return getVarInt(ypos,h) - getSquaredSum(xpos,ypos,h);
  }
}

double mg::mappings::GenerateStencil::getCornerQuadrature(double xpos, double ypos,double h){
  return getSquaredSum(xpos,ypos,h) - getVarInt(xpos,h) - getVarInt(ypos,h);
}

double mg::mappings::GenerateStencil::getPermeabilityD(double x, double y){
  return 1.0;
}

double mg::mappings::GenerateStencil::edgeNumericalIntegration(double xpos, double ypos, bool vert_xpos, bool vert_ypos,bool vert_xposf, bool vert_yposf, const int numberofSamplingPoints,double h){
  struct point lowestCorner;
  if(vert_xpos){
    lowestCorner.x = xpos-h/2.0;
  } else {
    lowestCorner.x = xpos+h/2.0;
  }
  if(vert_ypos){
    lowestCorner.y = ypos-h/2.0;
  } else {
    lowestCorner.y = ypos+h/2.0;
  }
  //traversing x direction if difference in x values (hence xor the bools)
  bool xdirection = vert_xpos^vert_xposf;
  const double cellSize = h;
  const double quadratureSpacing = cellSize/(double)numberofSamplingPoints;
  const double stepSize = 1.0/(double)numberofSamplingPoints;
  std::vector<double> partialDerivativeEvals000(numberofSamplingPoints*numberofSamplingPoints);
  double cellXPos,cellYPos;
  double xmod, ymod;
  if(vert_xpos){
    xmod = 1.0;
  } else {
    xmod = -1.0;
  }
  if(vert_ypos){
    ymod = 1.0;
  } else {
    ymod = -1.0;
  }
  xpos = stepSize/2.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    ypos = stepSize/2.0;
    for(int j = 0; j < numberofSamplingPoints; j++){
      partialDerivativeEvals000[j+i*numberofSamplingPoints] = getEdgeQuadrature(xpos,ypos,stepSize,xdirection);
      ypos += stepSize;
    }
    xpos += stepSize;
  }
  std::vector<double> permeabilityValues(numberofSamplingPoints*numberofSamplingPoints);
  int permCount = 0;
  cellXPos = lowestCorner.x + xmod*quadratureSpacing/2.0;
  //cellXPos = lowestCorner.x + quadratureSpacing/2.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    cellYPos = lowestCorner.y + ymod*quadratureSpacing/2.0;
    //cellYPos = lowestCorner.y + quadratureSpacing/2.0;
    for(int j = 0; j < numberofSamplingPoints; j++){
      permeabilityValues[permCount] = getPermeabilityD(cellXPos,cellYPos);
      cellYPos += ymod*quadratureSpacing;
      permCount++;
    }
    cellXPos += xmod*quadratureSpacing;
  }
  double totalVal = 0.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    for(int j = 0; j < numberofSamplingPoints; j++){
      totalVal += permeabilityValues[j+i*numberofSamplingPoints]*partialDerivativeEvals000[j+i*numberofSamplingPoints];
    }
  }
  return totalVal;
}

double mg::mappings::GenerateStencil::centreNumericalIntegration(double xpos, double ypos, bool vert_xpos, bool vert_ypos, const int numberofSamplingPoints, double h){
  struct point lowestCorner;
  if(vert_xpos){
    lowestCorner.x = xpos-h/2.0;
  } else {
    lowestCorner.x = xpos+h/2.0;
  }
  if(vert_ypos){
    lowestCorner.y = ypos-h/2.0;
  } else {
    lowestCorner.y = ypos+h/2.0;
  }
  const double cellSize = h;
  const double quadratureSpacing = cellSize/(double)numberofSamplingPoints;
  const double stepSize = 1.0/(double)numberofSamplingPoints;
  std::vector<double> partialDerivativeEvals000(numberofSamplingPoints*numberofSamplingPoints);
  double cellXPos,cellYPos;
  double xmod, ymod;
  if(vert_xpos){
    xmod = 1.0;
  } else {
    xmod = -1.0;
  }
  if(vert_ypos){
    ymod = 1.0;
  } else {
    ymod = -1.0;
  }
  xpos = stepSize/2.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    ypos = stepSize/2.0;
    for(int j = 0; j < numberofSamplingPoints; j++){
      partialDerivativeEvals000[j+i*numberofSamplingPoints] = getCentreQuadrature(xpos,ypos,stepSize);
      ypos += stepSize;
    }
    xpos += stepSize;
  }
  std::vector<double> permeabilityValues(numberofSamplingPoints*numberofSamplingPoints);
  int permCount = 0;
  cellXPos = lowestCorner.x + xmod*quadratureSpacing/2.0;
  //cellXPos = lowestCorner.x + quadratureSpacing/2.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    cellYPos = lowestCorner.y + ymod*quadratureSpacing/2.0;
    //cellYPos = lowestCorner.y + quadratureSpacing/2.0;
    for(int j = 0; j < numberofSamplingPoints; j++){
      permeabilityValues[permCount] = getPermeabilityD(cellXPos,cellYPos);
      cellYPos += ymod*quadratureSpacing;
      permCount++;
    }
    cellXPos += xmod*quadratureSpacing;
  }
  double totalVal = 0.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    for(int j = 0; j < numberofSamplingPoints; j++){
      totalVal += permeabilityValues[j+i*numberofSamplingPoints]*partialDerivativeEvals000[j+i*numberofSamplingPoints];
    }
  }
  return totalVal;
}

double mg::mappings::GenerateStencil::cornerNumericalIntegration(double xpos, double ypos, bool vert_xpos, bool vert_ypos, const int numberofSamplingPoints,double h){
  struct point lowestCorner;
  if(vert_xpos){
    lowestCorner.x = xpos-h/2.0;
  } else {
    lowestCorner.x = xpos+h/2.0;
  }
  if(vert_ypos){
    lowestCorner.y = ypos-h/2.0;
  } else {
    lowestCorner.y = ypos+h/2.0;
  }
  const double cellSize = h;
  const double quadratureSpacing = cellSize/(double)numberofSamplingPoints;
  const double stepSize = 1.0/(double)numberofSamplingPoints;
  std::vector<double> partialDerivativeEvals000(numberofSamplingPoints*numberofSamplingPoints);
  double cellXPos,cellYPos;
  double xmod, ymod;
  if(vert_xpos){
    xmod = 1.0;
  } else {
    xmod = -1.0;
  }
  if(vert_ypos){
    ymod = 1.0;
  } else {
    ymod = -1.0;
  }
  xpos = stepSize/2.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    ypos = stepSize/2.0;
    for(int j = 0; j < numberofSamplingPoints; j++){
      partialDerivativeEvals000[j+i*numberofSamplingPoints] = getCornerQuadrature(xpos,ypos,stepSize);
      ypos += stepSize;
    }
    xpos += stepSize;
  }

  std::vector<double> permeabilityValues(numberofSamplingPoints*numberofSamplingPoints);
  int permCount = 0;
  cellXPos = lowestCorner.x + xmod*quadratureSpacing/2.0;
  //cellXPos = lowestCorner.x + quadratureSpacing/2.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    cellYPos = lowestCorner.y + ymod*quadratureSpacing/2.0;
    //cellYPos = lowestCorner.y + quadratureSpacing/2.0;
    for(int j = 0; j < numberofSamplingPoints; j++){
      permeabilityValues[permCount] = getPermeabilityD(cellXPos,cellYPos);
      cellYPos += ymod*quadratureSpacing;
      permCount++;
    }
    cellXPos += xmod*quadratureSpacing;
  }
  double cornerVal = 0.0;
  for(int i = 0; i < numberofSamplingPoints; i++){
    for(int j = 0; j < numberofSamplingPoints; j++){
      cornerVal += permeabilityValues[j+i*numberofSamplingPoints]*partialDerivativeEvals000[j+i*numberofSamplingPoints];
    }
  }
  return cornerVal;
}*/
