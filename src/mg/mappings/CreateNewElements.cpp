#include "mg/mappings/CreateNewElements.h"
#include "mg/mappings/ComputeRitzGalerkinOperator.h"
#include "mg/mappings/BStencil.h"
#include "mg/mappings/GenerateStencil.h"
#include "mg/mappings/CreateGrid.h"
#include "mg/mappings/ProjectThreeGridDiff.h"

#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"

#include <cmath>

#include "matrixfree/stencil/StencilFactory.h"



/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::CreateNewElements::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateNewElements::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateNewElements::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateNewElements::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateNewElements::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateNewElements::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateNewElements::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


tarch::logging::Log                mg::mappings::CreateNewElements::_log( "mg::mappings::CreateNewElements" );


mg::mappings::CreateNewElements::CreateNewElements()
{
    logTraceIn( "CreateNewElements()" );
    // @todo Insert your code here
    logTraceOut( "CreateNewElements()" );
}


mg::mappings::CreateNewElements::~CreateNewElements()
{
    logTraceIn( "~CreateNewElements()" );
    // @todo Insert your code here
    logTraceOut( "~CreateNewElements()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::CreateNewElements::CreateNewElements(const CreateNewElements  &masterThread)
{
    logTraceIn( "CreateNewElements(CreateNewElements)" );
    // @todo Insert your code here
    logTraceOut( "CreateNewElements(CreateNewElements)" );
}


void mg::mappings::CreateNewElements::mergeWithWorkerThread(const CreateNewElements &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(CreateNewElements)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithWorkerThread(CreateNewElements)" );
}
#endif


void mg::mappings::CreateNewElements::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    fineGridVertex.resetC();
    fineGridVertex.initialiseDLinearRestriction();
    //fineGridVertex.initialiseDLinearProlongation();
    //fineGridVertex.initialiseProlongationI();
    double u = _multigrid.getDLinearInterpolatedValue(VertexOperations::readU(coarseGridVerticesEnumerator, coarseGridVertices ), fineGridPositionOfVertex);

    fineGridVertex.init(fineGridX, u);

    if((mg::mappings::ProjectThreeGridDiff::delayCoarse) && _hasRefined)
    {
        if((coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::minLevel - 1) && (fineGridVertex.getRefinementControl() == Vertex::Records::Refined))
        {
            fineGridVertex.setOmega(0.0);
            fineGridVertex.setOmegaI(0.0);
        }
    }

    fineGridVertex.reset();
    double h = fineGridH(0);
    fineGridVertex.setF( std::pow( h, DIMENSIONS) * fineGridVertex.getF());
    fineGridVertex.setFI(0.0);


    tarch::la::Vector<TWO_POWER_D, double > weightVector;
    dfor2(l)
    tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
    tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
#ifdef Dim3
    coarseGridIndex = 2, 2, 2;
#else
    coarseGridIndex = 2, 2;
#endif
    for(int a = 0; a < DIMENSIONS; a++)
    {
        if(l(a) == 1)
        {
            coarseGridMods(a) = -3;
        }
        else
        {
            coarseGridMods(a) = 0;
        }
    }
    tarch::la::Vector<DIMENSIONS, int> index;
    for(int a = 0; a < DIMENSIONS; a++)
    {
        index(a) = coarseGridIndex(a) + coarseGridMods(a) + (fineGridPositionOfVertex(a));
    }
#ifdef Dim3
    const int z = index(2);
#else
    const int z = 0;
#endif
    //double projectionWeight = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getProlongationElement(index(0), index(1), z);
    double projectionWeight = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElement(index(0), index(1), z);
    weightVector(lScalar) = projectionWeight;
    enddforx

    //double coarseMod = weightVector * VertexOperations::readCCoarse(coarseGridVerticesEnumerator, coarseGridVertices);
    //double coarseModI = weightVector * VertexOperations::readCCoarseI(coarseGridVerticesEnumerator, coarseGridVertices);
    double coarseMod = weightVector * VertexOperations::readCLocal(coarseGridVerticesEnumerator, coarseGridVertices);
    //double coarseModI = weightVector * VertexOperations::readCLocalI(coarseGridVerticesEnumerator, coarseGridVertices);
    double bpxMod = weightVector * VertexOperations::readCBPX(coarseGridVerticesEnumerator, coarseGridVertices);



    if(!peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex))
    {
        coarseMod -= bpxMod;
        //BPX corrections from coarse grids must be propogated to fine grid layers
        //diffMod -= bpxMod;
    }
    if(fineGridVertex.isInside())
    {
        //fineGridVertex.setCoarseCorrection(coarseMod);
        //fineGridVertex.setCoarseCorrectionI(coarseModI);
        fineGridVertex.setLocalCorrection(coarseMod);
    }
    fineGridVertex.setUH(fineGridVertex.getU() - u);
    fineGridVertex.setUI(0.0);
    fineGridVertex.setUI(0.0);
    tarch::la::Vector<THREE_POWER_D, double> Astencil;



    switch(mg::mappings::GenerateStencil::problem)
    {
    case mg::mappings::GenerateStencil::Problem::Poisson:
    case mg::mappings::GenerateStencil::Problem::JumpingCoefficient:
    case mg::mappings::GenerateStencil::Problem::Checkerboard:
    case mg::mappings::GenerateStencil::Problem::Circle:
    case mg::mappings::GenerateStencil::Problem::Line:
    case mg::mappings::GenerateStencil::Problem::Sinusoidal:
    case mg::mappings::GenerateStencil::Problem::Perlin:
    case mg::mappings::GenerateStencil::Problem::PerlinStarvation:
    {
#ifdef Dim2
        Astencil = 1.0 * mg::mappings::GenerateStencil::getEpsilon(fineGridX) * matrixfree::stencil::get2DLaplaceStencil();

        double h = fineGridH(0);
        tarch::la::Vector<DIMENSIONS, double> xOffset1;
        xOffset1(0) = h / 2.0;
        xOffset1(1) = h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> yOffset1;
        yOffset1(0) = -h / 2.0;
        yOffset1(1) = h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> xOffset2;
        xOffset2(0) = h / 2.0;
        xOffset2(1) = -h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> yOffset2;
        yOffset2(0) = -h / 2.0;
        yOffset2(1) = -h / 2.0;


        tarch::la::Vector<DIMENSIONS, double> pos1 = fineGridX + xOffset1;
        double eps1 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos2 = fineGridX + xOffset2;
        double eps2 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos3 = fineGridX + yOffset1;
        double eps3 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos4 = fineGridX + yOffset2;
        double eps4 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * yOffset2));

        //elementwise contributions
        tarch::la::Vector<THREE_POWER_D, double> stencil0;
        stencil0 = -1.0, -0.5, 0.0, -0.5, 2.0, 0.0, 0.0, 0.0, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil1;
        stencil1 = 0.0, -0.5, -1.0, 0.0, 2.0, -0.5, 0.0, 0.0, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil2;
        stencil2 = 0.0, 0.0, 0.0, -0.5, 2.0, 0.0, -1.0, -0.5, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil3;
        stencil3 = 0.0, 0.0, 0.0, 0.0, 2.0, -0.5, 0.0, -0.5, -1.0;



        stencil0 *= eps4 * (1.0 / 3.0);
        stencil1 *= eps2 * (1.0 / 3.0);
        stencil2 *= eps3 * (1.0 / 3.0);
        stencil3 *= eps1 * (1.0 / 3.0);

        Astencil = stencil0 + stencil1 + stencil2 + stencil3;

#elif Dim3
        Astencil = 1.0 * mg::mappings::GenerateStencil::getEpsilon(fineGridX) * fineGridH(1) * matrixfree::stencil::get3DLaplaceStencil();
#endif
    }
    break;
    case mg::mappings::GenerateStencil::Problem::Anisotropic:
#ifdef Dim2
        Astencil = (-1.0) * mg::mappings::GenerateStencil::getAnisotropicStencil(fineGridX);
#elif Dim3
        Astencil = fineGridH(1) * mg::mappings::GenerateStencil::getAnisotropicStencil(fineGridX);
#endif
        break;
    case mg::mappings::GenerateStencil::Problem::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }

    fineGridVertex.setStencil(Astencil);
    //fineGridVertex.setD(Astencil(THREE_POWER_D / 2));
    //fineGridVertex.setTempD(Astencil(THREE_POWER_D / 2));

    fineGridVertex.resetAccumulatedValues();
    fineGridVertex.initialiseDLinearRestriction();
    //fineGridVertex.initialiseDLinearProlongation();
    //fineGridVertex.initialiseProlongationI();

    if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) )
    {
        const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setUI(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setFI(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setResI(  0.0 );

    }

    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::CreateNewElements::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::CreateNewElements::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here


    double u = _multigrid.getDLinearInterpolatedValue(VertexOperations::readU(coarseGridVerticesEnumerator, coarseGridVertices ), fineGridPositionOfVertex);
    fineGridVertex.init(fineGridX, u);
    fineGridVertex.resetC();
    fineGridVertex.reset();
    fineGridVertex.resetAccumulatedValues();
    double h = fineGridH(0);
    fineGridVertex.setF( std::pow( h, DIMENSIONS) * fineGridVertex.getF());
    fineGridVertex.setFI(0.0);

    //double coarseMod = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCCoarse(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
    //double coarseModI = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCCoarseI(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
    double coarseMod = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCLocal(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
    double coarseModI = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCLocalI(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
    double bpxMod = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCBPX(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
    if(!peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex))
    {
        coarseMod -= bpxMod;

    }
    if(_activelyProjecting)
    {
        fineGridVertex.setU(u - (coarseMod - coarseModI));
        if(mg::mappings::ProjectThreeGridDiff::delayCoarse)
        {
            fineGridVertex.setU(u);

        }

    }
    else
    {
        fineGridVertex.setU(u);

    }
    tarch::la::Vector<THREE_POWER_D, double> Astencil;

    switch(mg::mappings::GenerateStencil::problem)
    {
    case mg::mappings::GenerateStencil::Problem::Poisson:
    case mg::mappings::GenerateStencil::Problem::JumpingCoefficient:
    case mg::mappings::GenerateStencil::Problem::Checkerboard:
    case mg::mappings::GenerateStencil::Problem::Circle:
    case mg::mappings::GenerateStencil::Problem::Line:
    case mg::mappings::GenerateStencil::Problem::Sinusoidal:
    case mg::mappings::GenerateStencil::Problem::Perlin:
    case mg::mappings::GenerateStencil::Problem::PerlinStarvation:
    {
#ifdef Dim2
        double h = fineGridH(0);
        tarch::la::Vector<DIMENSIONS, double> xOffset1;
        xOffset1(0) = h / 2.0;
        xOffset1(1) = h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> yOffset1;
        yOffset1(0) = -h / 2.0;
        yOffset1(1) = h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> xOffset2;
        xOffset2(0) = h / 2.0;
        xOffset2(1) = -h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> yOffset2;
        yOffset2(0) = -h / 2.0;
        yOffset2(1) = -h / 2.0;


        tarch::la::Vector<DIMENSIONS, double> pos1 = fineGridX + xOffset1;
        double eps1 = mg::mappings::GenerateStencil::getEpsilon(pos1);
        tarch::la::Vector<DIMENSIONS, double> pos2 = fineGridX + xOffset2;
        double eps2 = mg::mappings::GenerateStencil::getEpsilon(pos2);
        tarch::la::Vector<DIMENSIONS, double> pos3 = fineGridX + yOffset1;
        double eps3 = mg::mappings::GenerateStencil::getEpsilon(pos3);
        tarch::la::Vector<DIMENSIONS, double> pos4 = fineGridX + yOffset2;
        double eps4 = mg::mappings::GenerateStencil::getEpsilon(pos4);

        //elementwise contributions
        tarch::la::Vector<THREE_POWER_D, double> stencil0;
        stencil0 = -1.0, -0.5, 0.0, -0.5, 2.0, 0.0, 0.0, 0.0, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil1;
        stencil1 = 0.0, -0.5, -1.0, 0.0, 2.0, -0.5, 0.0, 0.0, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil2;
        stencil2 = 0.0, 0.0, 0.0, -0.5, 2.0, 0.0, -1.0, -0.5, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil3;
        stencil3 = 0.0, 0.0, 0.0, 0.0, 2.0, -0.5, 0.0, -0.5, -1.0;


        stencil0 *= eps4 * (1.0 / 3.0);
        stencil1 *= eps2 * (1.0 / 3.0);
        stencil2 *= eps3 * (1.0 / 3.0);
        stencil3 *= eps1 * (1.0 / 3.0);


        Astencil = stencil0 + stencil1 + stencil2 + stencil3;

#elif Dim3
        Astencil = 1.0 * mg::mappings::GenerateStencil::getEpsilon(fineGridX) * fineGridH(1) * matrixfree::stencil::get3DLaplaceStencil();
#endif
    }
    break;
    case mg::mappings::GenerateStencil::Problem::Anisotropic:
#ifdef Dim2
        Astencil = (-1.0) * mg::mappings::GenerateStencil::getAnisotropicStencil(fineGridX);
#elif Dim3
        Astencil = fineGridH(1) * mg::mappings::GenerateStencil::getAnisotropicStencil(fineGridX);
#endif
        break;
    case mg::mappings::GenerateStencil::Problem::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }

    fineGridVertex.setStencil(Astencil);
    //fineGridVertex.setD(Astencil(THREE_POWER_D / 2));
    //fineGridVertex.setTempD(Astencil(THREE_POWER_D / 2));
    fineGridVertex.initialiseDLinearRestriction();
    fineGridVertex.initialiseRestrictionI();
    //fineGridVertex.initialiseDLinearProlongation();
    //fineGridVertex.initialiseProlongationI();
    if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) )
    {
        const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setUI(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setFI(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setResI(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setLocalCorrectionI(0.0);

    }


    if(!peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex))
    {
    }
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::CreateNewElements::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    fineGridVertex.init(fineGridX, 0.0);
    fineGridVertex.reset();
    fineGridVertex.resetC();
    double h = fineGridH(0);
    fineGridVertex.setF(h * h * h * fineGridVertex.getF());
    fineGridVertex.setFI(0.0);


    tarch::la::Vector<THREE_POWER_D, double> Astencil;


    switch(mg::mappings::GenerateStencil::problem)
    {
    case mg::mappings::GenerateStencil::Problem::Poisson:
    case mg::mappings::GenerateStencil::Problem::JumpingCoefficient:
    case mg::mappings::GenerateStencil::Problem::Checkerboard:
    case mg::mappings::GenerateStencil::Problem::Circle:
    case mg::mappings::GenerateStencil::Problem::Line:
    case mg::mappings::GenerateStencil::Problem::Sinusoidal:
    case mg::mappings::GenerateStencil::Problem::Perlin:
    case mg::mappings::GenerateStencil::Problem::PerlinStarvation:
    {
#ifdef Dim2
        double h = fineGridH(0);
        tarch::la::Vector<DIMENSIONS, double> xOffset1;
        xOffset1(0) = h / 2.0;
        xOffset1(1) = h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> yOffset1;
        yOffset1(0) = -h / 2.0;
        yOffset1(1) = h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> xOffset2;
        xOffset2(0) = h / 2.0;
        xOffset2(1) = -h / 2.0;
        tarch::la::Vector<DIMENSIONS, double> yOffset2;
        yOffset2(0) = -h / 2.0;
        yOffset2(1) = -h / 2.0;

        tarch::la::Vector<DIMENSIONS, double> pos1 = fineGridX + xOffset1;
        double eps1 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos1 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos2 = fineGridX + xOffset2;
        double eps2 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos2 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos3 = fineGridX + yOffset1;
        double eps3 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos3 + 0.5 * yOffset2));
        tarch::la::Vector<DIMENSIONS, double> pos4 = fineGridX + yOffset2;
        double eps4 = 0.25 * (mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * xOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * xOffset2) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * yOffset1) + mg::mappings::GenerateStencil::getEpsilon(pos4 + 0.5 * yOffset2));

        //elementwise contributions
        tarch::la::Vector<THREE_POWER_D, double> stencil0;
        stencil0 = -1.0, -0.5, 0.0, -0.5, 2.0, 0.0, 0.0, 0.0, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil1;
        stencil1 = 0.0, -0.5, -1.0, 0.0, 2.0, -0.5, 0.0, 0.0, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil2;
        stencil2 = 0.0, 0.0, 0.0, -0.5, 2.0, 0.0, -1.0, -0.5, 0.0;

        tarch::la::Vector<THREE_POWER_D, double> stencil3;
        stencil3 = 0.0, 0.0, 0.0, 0.0, 2.0, -0.5, 0.0, -0.5, -1.0;


        stencil0 *= eps4 * (1.0 / 3.0);
        stencil1 *= eps2 * (1.0 / 3.0);
        stencil2 *= eps3 * (1.0 / 3.0);
        stencil3 *= eps1 * (1.0 / 3.0);

        Astencil = stencil0 + stencil1 + stencil2 + stencil3;

#elif Dim3
        stencil = 1.0 * mg::mappings::GenerateStencil::getEpsilon(fineGridX) * fineGridH(1) * matrixfree::stencil::get3DLaplaceStencil();
#endif
    }
    break;
    case mg::mappings::GenerateStencil::Problem::Anisotropic:
#ifdef Dim2
        Astencil = (-1.0) * mg::mappings::GenerateStencil::getAnisotropicStencil(fineGridX);
#elif Dim3
        Astencil = fineGridH(1) * mg::mappings::GenerateStencil::getAnisotropicStencil(fineGridX);
#endif
        break;
    case mg::mappings::GenerateStencil::Problem::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }

    fineGridVertex.setStencil(Astencil);
    //fineGridVertex.setD(Astencil(THREE_POWER_D / 2));
    //fineGridVertex.setTempD(Astencil(THREE_POWER_D / 2));
    //fineGridVertex.initialiseDLinearProlongation();
    fineGridVertex.initialiseDLinearRestriction();
    //fineGridVertex.initialiseProlongationI();
    if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) )
    {
        const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setUI(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setFI(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setF(  0.0 );
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setResI(  0.0 );


    }

    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::CreateNewElements::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::CreateNewElements::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here

    fineGridCell.setConverged(false);


    matrixfree::stencil::VectorOfStencils stencils;
    dfor2(k)
    tarch::la::slice(
        stencils, // into stencils
        tarch::la::Vector<THREE_POWER_D, double>(fineGridVertices[ fineGridVerticesEnumerator(k) ].getStencil()),
        kScalar * THREE_POWER_D
    );
    enddforx


    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> stencil = matrixfree::stencil::getElementWiseAssemblyMatrix( matrixfree::stencil::get2DLaplaceStencil() );


    tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, double> out;
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            out(TWO_POWER_D * i + j) = stencil(i, j);
        }
    }
    fineGridCell.setStencil(out);



    switch(mg::mappings::BStencil::fineStencilType)
    {
    case mg::mappings::BStencil::FineStencilType::Static:
    case mg::mappings::BStencil::FineStencilType::StaticWithCoarse:
        break;
    case mg::mappings::BStencil::FineStencilType::Precompute:
    case mg::mappings::BStencil::FineStencilType::Background:
    case mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse:
    case mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse:
    {
#ifdef Dim2
        if(true)
        {
            int heapIndex;

            if(mg::mappings::BStencil::fineStencilSimple == mg::mappings::BStencil::FineStencilSimple::Averaged )
            {

                tarch::multicore::Lock myLock(mg::BackgroundStencilSimple::_mysemaphore);
                heapIndex = DataHeap::getInstance().createData(16, 16);
                fineGridCell.setHeap(heapIndex);
                IntHeap::getInstance().createDataForIndex(heapIndex, 1, 1);
                IntHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = 199;

                BoolHeap::getInstance().createDataForIndex(heapIndex, 2, 2);
                BoolHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = false;
                BoolHeap::getInstance().getData(heapIndex)[1]._persistentRecords._u = false;
                myLock.free();
                BackgroundStencilSimple *task = new BackgroundStencilSimple(_localState);
                int samplingPoints = 200;
                task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
                task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), 0.0, fineGridVerticesEnumerator.getCellSize()(0));
                peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::IsTaskAndRunImmediately);

            }
            else if (mg::mappings::BStencil::fineStencilSimple == mg::mappings::BStencil::FineStencilSimple::Integrated )
            {


                tarch::multicore::Lock myLock(mg::BackgroundStencil2D::_mysemaphore);
                heapIndex = DataHeap::getInstance().createData(16, 16);
                fineGridCell.setHeap(heapIndex);
                IntHeap::getInstance().createDataForIndex(heapIndex, 1, 1);
                IntHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = 199;

                BoolHeap::getInstance().createDataForIndex(heapIndex, 2, 2);
                BoolHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = false;
                BoolHeap::getInstance().getData(heapIndex)[1]._persistentRecords._u = false;
                myLock.free();
                BackgroundStencil2D *task = new BackgroundStencil2D(_localState);
                int samplingPoints = 200;
                task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
                task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), 0.0, fineGridVerticesEnumerator.getCellSize()(0));
                peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::IsTaskAndRunImmediately);
            }
        }

#elif Dim3
        int heapIndex;
        tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
        heapIndex = DataHeap::getInstance().createData(64, 64);
        fineGridCell.setHeap(heapIndex);
        IntHeap::getInstance().createDataForIndex(heapIndex, 1, 1);
        IntHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = 2;

        BoolHeap::getInstance().createDataForIndex(heapIndex, 2, 2);
        BoolHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = false;
        BoolHeap::getInstance().getData(heapIndex)[1]._persistentRecords._u = false;
        myLock.free();

        BackgroundStencil *task = new BackgroundStencil(_localState);
        int samplingPoints = 2;
        task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
        task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), fineGridVerticesEnumerator.getCellCenter()(2), fineGridVerticesEnumerator.getCellSize()(0));
        peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::IsTaskAndRunAsSoonAsPossible);

#endif
        break;
    }
    case mg::mappings::BStencil::FineStencilType::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }
    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::CreateNewElements::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::CreateNewElements::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::CreateNewElements::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::CreateNewElements::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::CreateNewElements::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::CreateNewElements::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::CreateNewElements::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::CreateNewElements::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::CreateNewElements::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::CreateNewElements::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::CreateNewElements::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here


    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::CreateNewElements::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::CreateNewElements::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::CreateNewElements::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::CreateNewElements::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::CreateNewElements::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::CreateNewElements::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::CreateNewElements::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );
    // @todo Insert your code here

    _localState = solverState;
    _firstIter = solverState.getFirstIter();
    _hasRefined = solverState.justRefined();
    _activelyProjecting = solverState.getActivelyProjecting();
    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::CreateNewElements::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here

    solverState.setActivelyProjecting(false);

    //logInfo( "endIteration()", "inner verts=" << solverState.getNumberOfInnerVertices() );
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::CreateNewElements::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "descend(...)" );
}


void mg::mappings::CreateNewElements::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}
