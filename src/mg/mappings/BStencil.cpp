#include "mg/mappings/BStencil.h"

#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"
#include <cmath>

#include "matrixfree/stencil/StencilFactory.h"


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::BStencil::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::BStencil::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::BStencil::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::BStencil::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::BStencil::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::BStencil::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::BStencil::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}



tarch::logging::Log                mg::mappings::BStencil::_log( "mg::mappings::BStencil" );
mg::mappings::BStencil::FineStencilType mg::mappings::BStencil::fineStencilType = mg::mappings::BStencil::FineStencilType::Undef;
mg::mappings::BStencil::FineStencilSimple mg::mappings::BStencil::fineStencilSimple = mg::mappings::BStencil::FineStencilSimple::Undef;


mg::mappings::BStencil::BStencil()
{
    logTraceIn( "BStencil()" );
    // @todo Insert your code here
    logTraceOut( "BStencil()" );
}


mg::mappings::BStencil::~BStencil()
{
    logTraceIn( "~BStencil()" );
    // @todo Insert your code here
    logTraceOut( "~BStencil()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::BStencil::BStencil(const BStencil  &masterThread)
{
    logTraceIn( "BStencil(BStencil)" );
    // @todo Insert your code here
    _allConverged = true;
    _count = 0;
    _count2 = 0;

    _maxDiff = -1.0;
    logTraceOut( "BStencil(BStencil)" );
}


void mg::mappings::BStencil::mergeWithWorkerThread(const BStencil &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(BStencil)" );
    // @todo Insert your code here
    _count += workerThread._count;
    _count2 += workerThread._count2;
    if(!workerThread._allConverged)
    {
        _allConverged = false;
    }
    if(workerThread._maxDiff > _maxDiff)
    {
        _maxDiff = workerThread._maxDiff;
    }
    logTraceOut( "mergeWithWorkerThread(BStencil)" );
}
#endif


void mg::mappings::BStencil::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::BStencil::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::BStencil::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::BStencil::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::BStencil::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::BStencil::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here

    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::BStencil::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::BStencil::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::BStencil::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::BStencil::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::BStencil::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::BStencil::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::BStencil::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::BStencil::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::BStencil::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::BStencil::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::BStencil::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::BStencil::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::BStencil::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::BStencil::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    switch(fineStencilType)
    {
    case mg::mappings::BStencil::FineStencilType::Static:
    case mg::mappings::BStencil::FineStencilType::StaticWithCoarse:
        break;
    case mg::mappings::BStencil::FineStencilType::Precompute:
    case mg::mappings::BStencil::FineStencilType::Background:
    case mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse:
    case mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse:
    {
        fineGridVertex.resetAccumulatedValues();
    }
    break;
    case mg::mappings::BStencil::FineStencilType::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }
    //fineGridVertex.setAnalyseForRefine(true);
    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::BStencil::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    switch(fineStencilType)
    {
    case mg::mappings::BStencil::FineStencilType::Static:
    case mg::mappings::BStencil::FineStencilType::StaticWithCoarse:
        break;
    case mg::mappings::BStencil::FineStencilType::Precompute:
    case mg::mappings::BStencil::FineStencilType::Background:
    case mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse:
    case mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse:
    {
        if(fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined)
        {
            if(fineGridVertex.isInside())
            {

                if(fineGridVertex.isDifferentToUpdate())
                {
                    fineGridVertex.setConverged(false);
                }
                else
                {
                    fineGridVertex.setConverged(true);
                }
                fineGridVertex.updateStencil();
                if(!fineGridVertex.isStencilDiagonallyDominant())
                {
                    _allDiagonallyDominant = false;
                }
            }
        }
    }
    break;
    case mg::mappings::BStencil::FineStencilType::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }
    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::BStencil::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::BStencil::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    switch(fineStencilType)
    {
    case mg::mappings::BStencil::FineStencilType::Static:
    case mg::mappings::BStencil::FineStencilType::StaticWithCoarse:
        break;
    case mg::mappings::BStencil::FineStencilType::Precompute:
    case mg::mappings::BStencil::FineStencilType::Background:
    case mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse:
    case mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse:
    {
        bool finest = !fineGridCell.isRefined();
        bool fin;
        bool flag;

        bool effectivelyFinest = false;
        dfor2(i)
        if(fineGridVertices[fineGridVerticesEnumerator(iScalar)].getRefinementControl() != Vertex::Records::Refined)
        {
            effectivelyFinest = true;
        }
        enddforx

        if(mg::mappings::BStencil::fineStencilSimple == mg::mappings::BStencil::FineStencilSimple::Averaged)
        {
#ifdef Dim2
            tarch::multicore::Lock myLock(mg::BackgroundStencilSimple::_mysemaphore);
#elif Dim3
            tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
#endif
            fin = BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u;
            flag = BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[1]._persistentRecords._u;
            myLock.free();
        }
        else
        {
#ifdef Dim2
            tarch::multicore::Lock myLock(mg::BackgroundStencil2D::_mysemaphore);
#elif Dim3
            tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
#endif
            fin = BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u;
            flag = BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[1]._persistentRecords._u;
            myLock.free();
        }

        tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> A;
        bool tempFlag = finest || effectivelyFinest;
        bool allFineConverged = true;
        dfor2(k)
        if(!fineGridVertices[ fineGridVerticesEnumerator(k) ].isConverged())
        {
            allFineConverged = false;
        }
        enddforx
        allFineConverged = false;

        if((tempFlag && (!flag)))
        {
            _allConverged = false;
            _count++;
        }

        if((tempFlag && (fin)))
        {
            _allConverged = false;
            _count2++;
        }

        if(fin && tempFlag && !allFineConverged )
        {
            if(mg::mappings::BStencil::fineStencilSimple == mg::mappings::BStencil::FineStencilSimple::Averaged)
            {
#ifdef Dim2
                tarch::multicore::Lock myLock(mg::BackgroundStencilSimple::_mysemaphore);
#elif Dim3
                tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
#endif

                for(int i = 0; i < TWO_POWER_D; i++)
                {
                    for(int j = 0; j < TWO_POWER_D; j++)
                    {
                        A(i, j) = DataHeap::getInstance().getData(fineGridCell.getHeapIndex())[TWO_POWER_D * i + j]._persistentRecords._u;
                    }
                }
                myLock.free();


            }
            else
            {
#ifdef Dim2
                tarch::multicore::Lock myLock(mg::BackgroundStencil2D::_mysemaphore);
#elif Dim3
                tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
#endif

                for(int i = 0; i < TWO_POWER_D; i++)
                {
                    for(int j = 0; j < TWO_POWER_D; j++)
                    {
                        A(i, j) = DataHeap::getInstance().getData(fineGridCell.getHeapIndex())[TWO_POWER_D * i + j]._persistentRecords._u;
                    }
                }
                myLock.free();

            }

#ifdef Dim3
            const double h = fineGridVerticesEnumerator.getCellSize()(0);
            A = h * A;
#elif Dim2
            A = A;
#endif

            tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> oldA;
            tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, double> in = fineGridCell.getStencil();
            for(int i = 0; i < TWO_POWER_D; i++)
            {
                for(int j = 0; j < TWO_POWER_D; j++)
                {
                    oldA(i, j) = in(TWO_POWER_D * i + j);
                }
            }

            double maxDiff = 0.0;
            double total = 0.0;
            double diagonalSum = 0.0;
            for(int i = 0; i < TWO_POWER_D; i++)
            {
                for(int j = 0; j < TWO_POWER_D; j++)
                {
                    total = std::fabs(A(i, j));
                    diagonalSum = std::fabs(A(i, j) - oldA(i, j));
                    double diff = diagonalSum / total;
                    if(diff > maxDiff)
                    {
                        maxDiff = diff;
                    }
                }
            }

            tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, double> out;

            for(int i = 0; i < TWO_POWER_D; i++)
            {
                for(int j = 0; j < TWO_POWER_D; j++)
                {
                    out(TWO_POWER_D * i + j) = A(i, j);
                }
            }
            fineGridCell.setStencil(out);
            bool terminate = false;

            if(mg::mappings::BStencil::fineStencilSimple == mg::mappings::BStencil::FineStencilSimple::Averaged)
            {

#ifdef Dim2
                tarch::multicore::Lock myLock2(mg::BackgroundStencilSimple::_mysemaphore);
#elif Dim3
                tarch::multicore::Lock myLock2(mg::BackgroundStencil::_mysemaphore);
#endif

                if(_maxDiff < maxDiff)
                {
                    _maxDiff = maxDiff;
                }
                if(maxDiff < 0.03)
                {
                    terminate = true;
                    fineGridCell.setConverged(true);
                }
                else
                {
                    fineGridCell.setConverged(false);
                }
                myLock2.free();

            }
            else
            {

#ifdef Dim2
                tarch::multicore::Lock myLock2(mg::BackgroundStencil2D::_mysemaphore);
#elif Dim3
                tarch::multicore::Lock myLock2(mg::BackgroundStencil::_mysemaphore);
#endif

                if(_maxDiff < maxDiff)
                {
                    _maxDiff = maxDiff;
                }
                if(maxDiff < 0.01)
                {
                    terminate = true;
                    fineGridCell.setConverged(true);
                }
                else
                {
                    fineGridCell.setConverged(false);
                }
                myLock2.free();

            }
            tarch::la::Vector<TWO_POWER_D_TIMES_THREE_POWER_D, double> frag;
            frag = matrixfree::stencil::reconstructStencilFragments(A);
            for(int i = 0; i < TWO_POWER_D; i++)
            {
                dfor3(foo)
#ifdef Dim2
                double val = frag(i * THREE_POWER_D + + 3 * foo(1) + foo(0));
                if((fineGridVertices[fineGridVerticesEnumerator(i)].getRefinementControl() == Vertex::Records::Unrefined) && fineGridVertices[fineGridVerticesEnumerator(i)].isInside())
                {
                    fineGridVertices[fineGridVerticesEnumerator(i)].incAccumulatedValue( val, foo(0), foo(1), 0);
                }
#elif Dim3
                double val = frag(i * THREE_POWER_D + 9 * foo(2) + 3 * foo(1)  + foo(0));
                if((fineGridVertices[fineGridVerticesEnumerator(i)].getRefinementControl() == Vertex::Records::Unrefined) && fineGridVertices[fineGridVerticesEnumerator(i)].isInside())
                {
                    fineGridVertices[fineGridVerticesEnumerator(i)].incAccumulatedValue( val, foo(0), foo(1), foo(2));
                }
#endif
                enddforx
            }
            if(!terminate)
            {
                int samplingPoints;

                if(mg::mappings::BStencil::fineStencilSimple == mg::mappings::BStencil::FineStencilSimple::Averaged)
                {

#ifdef Dim2
                    tarch::multicore::Lock myLock(mg::BackgroundStencilSimple::_mysemaphore);
#elif Dim3
                    tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
#endif

                    BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u = false;
                    BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[1]._persistentRecords._u = false;
                    IntHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u += 10;
                    samplingPoints = IntHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u;
                    myLock.free();


#ifdef Dim3
                    BackgroundStencil *task = new BackgroundStencil(_localState);
                    task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
                    task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), fineGridVerticesEnumerator.getCellCenter()(2), fineGridVerticesEnumerator.getCellSize()(0));
                    peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::Background);
#elif Dim2
                    BackgroundStencilSimple *task = new BackgroundStencilSimple(_localState);
                    task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
                    task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), 0.0, fineGridVerticesEnumerator.getCellSize()(0));
                    peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::Background);
#endif

                }
                else
                {

#ifdef Dim2
                    tarch::multicore::Lock myLock(mg::BackgroundStencil2D::_mysemaphore);
#elif Dim3
                    tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
#endif

                    BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u = false;
                    BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[1]._persistentRecords._u = false;
                    IntHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u += 10;
                    samplingPoints = IntHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u;
                    myLock.free();


#ifdef Dim3
                    BackgroundStencil *task = new BackgroundStencil(_localState);
                    task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
                    task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), fineGridVerticesEnumerator.getCellCenter()(2), fineGridVerticesEnumerator.getCellSize()(0));
                    peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::Background);
#elif Dim2
                    BackgroundStencil2D *task = new BackgroundStencil2D(_localState);
                    task->init(_localState, samplingPoints, fineGridCell.getHeapIndex());
                    task->setCellData(fineGridVerticesEnumerator.getCellCenter()(0), fineGridVerticesEnumerator.getCellCenter()(1), 0.0, fineGridVerticesEnumerator.getCellSize()(0));
                    peano::datatraversal::TaskSet foo(task, peano::datatraversal::TaskSet::TaskType::Background);
#endif
                }

            }
            else
            {
                fineGridCell.setConverged(true);

                if(mg::mappings::BStencil::fineStencilSimple == mg::mappings::BStencil::FineStencilSimple::Averaged)
                {
#ifdef Dim2
                    tarch::multicore::Lock myLock(mg::BackgroundStencilSimple::_mysemaphore);
#elif Dim3
                    tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
#endif

                    BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u = false;
                    BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[1]._persistentRecords._u = true;
                    myLock.free();

                }
                else
                {
#ifdef Dim2
                    tarch::multicore::Lock myLock(mg::BackgroundStencil2D::_mysemaphore);
#elif Dim3
                    tarch::multicore::Lock myLock(mg::BackgroundStencil::_mysemaphore);
#endif

                    BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[0]._persistentRecords._u = false;
                    BoolHeap::getInstance().getData(fineGridCell.getHeapIndex())[1]._persistentRecords._u = true;
                    myLock.free();
                }

            }

        }
        else
        {
            if(tempFlag)
            {
                if(true)
                {
                    tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, double> in = fineGridCell.getStencil();
                    for(int i = 0; i < TWO_POWER_D; i++)
                    {
                        for(int j = 0; j < TWO_POWER_D; j++)
                        {
                            A(i, j) = in(TWO_POWER_D * i + j);
                        }
                    }
                    tarch::la::Vector<TWO_POWER_D_TIMES_THREE_POWER_D, double> frag;
                    frag = matrixfree::stencil::reconstructStencilFragments(A);
                    for(int i = 0; i < TWO_POWER_D; i++)
                    {
                        dfor3(foo)
#ifdef Dim2
                        double val = frag(i * THREE_POWER_D + + 3 * foo(1) + foo(0));
                        if((fineGridVertices[fineGridVerticesEnumerator(i)].getRefinementControl() == Vertex::Records::Unrefined) && fineGridVertices[fineGridVerticesEnumerator(i)].isInside())
                        {
                            fineGridVertices[fineGridVerticesEnumerator(i)].incAccumulatedValue( val, foo(0), foo(1), 0);
                        }
#elif Dim3
                        double val = frag(i * THREE_POWER_D + 9 * foo(2) + 3 * foo(1)  + foo(0));
                        if((fineGridVertices[fineGridVerticesEnumerator(i)].getRefinementControl() == Vertex::Records::Unrefined) && fineGridVertices[fineGridVerticesEnumerator(i)].isInside())
                        {
                            fineGridVertices[fineGridVerticesEnumerator(i)].incAccumulatedValue( val, foo(0), foo(1), foo(2));
                        }
#endif

                        enddforx
                    }
                }
            }
        }

    }
    break;
    case mg::mappings::BStencil::FineStencilType::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }

    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::BStencil::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );

    _localState = solverState;
    _allConverged = true;
    _count = 0;
    _count2 = 0;

    _maxDiff = -1.0;


    _allDiagonallyDominant = true;

    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::BStencil::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here
    logInfo( "bstencil()", "unterminated stencils=" << _count );
    logInfo( "bstencil()", "updated stencils=" << _count2 );
    logInfo( "bstencil()", "max updated difference=" << _maxDiff );
    solverState.setStencilConverged(_allConverged);

    if(!_allDiagonallyDominant)
    {
        solverState.setDiagonallyDominant(false);
    }

    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::BStencil::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "descend(...)" );
}


void mg::mappings::BStencil::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}
