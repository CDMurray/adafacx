#include "mg/mappings/RestrictThree.h"
#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"

#include <cmath>



/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::RestrictThree::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::RestrictThree::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::AvoidCoarseGridRaces,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::RestrictThree::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::RestrictThree::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::RestrictThree::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::RestrictThree::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::RestrictThree::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


tarch::logging::Log                mg::mappings::RestrictThree::_log( "mg::mappings::RestrictThree" );


mg::mappings::RestrictThree::RestrictThree()
{
    logTraceIn( "RestrictThree()" );
    // @todo Insert your code here
    logTraceOut( "RestrictThree()" );
}


mg::mappings::RestrictThree::~RestrictThree()
{
    logTraceIn( "~RestrictThree()" );
    // @todo Insert your code here
    logTraceOut( "~RestrictThree()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::RestrictThree::RestrictThree(const RestrictThree  &masterThread)
{
    logTraceIn( "RestrictThree(RestrictThree)" );
    // @todo Insert your code here
    logTraceOut( "RestrictThree(RestrictThree)" );
}


void mg::mappings::RestrictThree::mergeWithWorkerThread(const RestrictThree &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(RestrictThree)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithWorkerThread(RestrictThree)" );
}
#endif


void mg::mappings::RestrictThree::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::RestrictThree::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::RestrictThree::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::RestrictThree::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::RestrictThree::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::RestrictThree::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::RestrictThree::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::RestrictThree::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::RestrictThree::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::RestrictThree::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::RestrictThree::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::RestrictThree::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::RestrictThree::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::RestrictThree::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::RestrictThree::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::RestrictThree::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::RestrictThree::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::RestrictThree::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::RestrictThree::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::RestrictThree::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::RestrictThree::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    fineGridVertex.resetC();
    fineGridVertex.setUI(0.0);

    if(fineGridVertex.isInside())
    {
        fineGridVertex.updateRes();
        double res = fineGridVertex.getResH();
        double resI = fineGridVertex.getRes();

        // injection
        if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) )
        {
            const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
            //coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setFineCorrection(fineGridVertex.getFineCorrection() + fineGridVertex.getDiff());
            coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setLocalCorrection(fineGridVertex.getLocalCorrection() + fineGridVertex.getPotentialUpdate());
        }
        //fineGridVertex.setFineCorrection(fineGridVertex.getFineCorrection() + fineGridVertex.getPotentialUpdate());
        fineGridVertex.setLocalCorrection(fineGridVertex.getLocalCorrection() + fineGridVertex.getPotentialUpdate());
        tarch::la::Vector<TWO_POWER_D, double > weights;

        switch(mg::mappings::ProjectThreeGridDiff::restriction)
        {
        case mg::mappings::ProjectThreeGridDiff::Restriction::dlinear:
            weights = _multigrid.getDLinearInterpolationWeights(
                          fineGridPositionOfVertex
                      );
            break;
        case mg::mappings::ProjectThreeGridDiff::Restriction::boxMG:
            //temp thing to test boxmg
            dfor2(l)
            tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
            tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
#ifdef Dim3
            coarseGridIndex = 2, 2, 2;
#else
            coarseGridIndex = 2, 2;
#endif
            for(int a = 0; a < DIMENSIONS; a++)
            {
                if(l(a) == 1)
                {
                    coarseGridMods(a) = -3;
                }
                else
                {
                    coarseGridMods(a) = 0;
                }
            }
            tarch::la::Vector<DIMENSIONS, int> index;
            for(int a = 0; a < DIMENSIONS; a++)
            {
                index(a) = coarseGridIndex(a) + coarseGridMods(a) + (fineGridPositionOfVertex(a));
            }
#ifdef Dim3
            const int z = index(2);
#else
            const int z = 0;
#endif
            double projectionWeight = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElement(index(0), index(1), z);
            weights(lScalar) = projectionWeight;
            enddforx

            break;
        case mg::mappings::ProjectThreeGridDiff::Restriction::Undef:
            assertionMsg(false, "not properly initialised" );
            break;

        }

        dfor2(k)
        if (
            coarseGridVertices[ coarseGridVerticesEnumerator(k) ].getRefinementControl() == Vertex::Records::Refined
            &&
            coarseGridVertices[ coarseGridVerticesEnumerator(k) ].isInside()
        )
        {
            double curF  = coarseGridVertices[ coarseGridVerticesEnumerator(k) ].getF();

            VertexOperations::writeF(coarseGridVertices[ coarseGridVerticesEnumerator(k) ], curF + weights(kScalar) * res);


        }
        enddforx
        if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) )
        {
            const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
            //double curFI = coarseGridVertices[ coarseGridVerticesEnumerator(k) ].getFI();
            coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setFI(  resI );
        }
    }

    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::RestrictThree::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::RestrictThree::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::RestrictThree::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );
    // @todo Insert your code here
    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::RestrictThree::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::RestrictThree::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "descend(...)" );
}


void mg::mappings::RestrictThree::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}
