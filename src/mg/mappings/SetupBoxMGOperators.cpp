#include "mg/mappings/SetupBoxMGOperators.h"

#include "mg/mappings/ProjectThreeGridDiff.h"
#include "mg/mappings/BStencil.h"

#include "matrixfree/stencil/StencilFactory.h"


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::SetupBoxMGOperators::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SetupBoxMGOperators::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SetupBoxMGOperators::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SetupBoxMGOperators::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SetupBoxMGOperators::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SetupBoxMGOperators::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SetupBoxMGOperators::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


tarch::logging::Log                mg::mappings::SetupBoxMGOperators::_log( "mg::mappings::SetupBoxMGOperators" );


mg::mappings::SetupBoxMGOperators::SetupBoxMGOperators()
{
    logTraceIn( "SetupBoxMGOperators()" );
    // @todo Insert your code here
    logTraceOut( "SetupBoxMGOperators()" );
}


mg::mappings::SetupBoxMGOperators::~SetupBoxMGOperators()
{
    logTraceIn( "~SetupBoxMGOperators()" );
    // @todo Insert your code here
    logTraceOut( "~SetupBoxMGOperators()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::SetupBoxMGOperators::SetupBoxMGOperators(const SetupBoxMGOperators  &masterThread)
{
    logTraceIn( "SetupBoxMGOperators(SetupBoxMGOperators)" );
    // @todo Insert your code here
    logTraceOut( "SetupBoxMGOperators(SetupBoxMGOperators)" );
}


void mg::mappings::SetupBoxMGOperators::mergeWithWorkerThread(const SetupBoxMGOperators &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(SetupBoxMGOperators)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithWorkerThread(SetupBoxMGOperators)" );
}
#endif


void mg::mappings::SetupBoxMGOperators::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::SetupBoxMGOperators::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::SetupBoxMGOperators::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::SetupBoxMGOperators::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::SetupBoxMGOperators::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::SetupBoxMGOperators::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::SetupBoxMGOperators::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::SetupBoxMGOperators::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::SetupBoxMGOperators::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::SetupBoxMGOperators::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::SetupBoxMGOperators::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::SetupBoxMGOperators::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::SetupBoxMGOperators::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::SetupBoxMGOperators::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::SetupBoxMGOperators::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::SetupBoxMGOperators::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::SetupBoxMGOperators::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::SetupBoxMGOperators::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::SetupBoxMGOperators::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::SetupBoxMGOperators::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    fineGridVertex.resetRestrictionAccVal();

    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::SetupBoxMGOperators::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    switch(mg::mappings::ProjectThreeGridDiff::restriction)
    {
    case mg::mappings::ProjectThreeGridDiff::Restriction::dlinear:
    //break;

    case mg::mappings::ProjectThreeGridDiff::Restriction::boxMG:
    {

        if((fineGridVertex.getRefinementControl() == Vertex::Records::Refined) && (fineGridVertex.isInside()))
        {
            fineGridVertex.updateRestrictionI();
        }

        dfor2(l)
        if(fineGridVertex.isInside() && coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].isInside())
        {
            tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
            tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
#ifdef Dim3
            coarseGridIndex = 2, 2, 2;
#else
            coarseGridIndex = 2, 2;
#endif
            for(int a = 0; a < DIMENSIONS; a++)
            {
                if(l(a) == 1)
                {
                    coarseGridMods(a) = -3;
                }
                else
                {
                    coarseGridMods(a) = 0;
                }
            }
            tarch::la::Vector<DIMENSIONS, int> index;
            for(int a = 0; a < DIMENSIONS; a++)
            {
                index(a) = coarseGridIndex(a) + coarseGridMods(a) + (fineGridPositionOfVertex(a));
            }
#ifdef Dim3
            const int z = index(2);
#else
            const int z = 0;
#endif
            tarch::la::Vector<THREE_POWER_D, double> projectionWeights;
            dfor3(k)
            if(std::fabs(coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElement(index(0), index(1), 0)) > 1e-8)
            {
                projectionWeights(kScalar) = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElement(index(0) + k(0) - 1, index(1) + k(1) - 1, 0);
            }
            else
            {
                projectionWeights(kScalar) = 0.0;
            }
            enddforx
            tarch::la::Vector<THREE_POWER_D, double> stencil = fineGridVertex.getStencil() * ((1.0) / (fineGridVertex.getStencil()(THREE_POWER_D / 2)));

            double total = 0.0;
            dfor3(k)
            total += projectionWeights(kScalar) * stencil(kScalar);
            enddforx
            coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].incRestrictionAccVal(total, index(0), index(1), z);
        }
        enddforx
    }
    break;
    case mg::mappings::ProjectThreeGridDiff::Restriction::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }


    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::SetupBoxMGOperators::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here


    switch(mg::mappings::ProjectThreeGridDiff::restriction)
    {
    case mg::mappings::ProjectThreeGridDiff::Restriction::dlinear:
        break;

    case mg::mappings::ProjectThreeGridDiff::Restriction::boxMG:
    {
    }
    break;
    case mg::mappings::ProjectThreeGridDiff::Restriction::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }

    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::SetupBoxMGOperators::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::SetupBoxMGOperators::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );
    // @todo Insert your code here
    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::SetupBoxMGOperators::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::SetupBoxMGOperators::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );


    switch(mg::mappings::ProjectThreeGridDiff::restriction)
    {
    case mg::mappings::ProjectThreeGridDiff::Restriction::dlinear:
        break;

    case mg::mappings::ProjectThreeGridDiff::Restriction::boxMG:
    {
        

        bool allCoarseConverged = true;
        dfor2(k)
        if(!coarseGridVertices[ coarseGridVerticesEnumerator(k) ].isConverged())
        {
            allCoarseConverged = false;
        }

        enddforx

        allCoarseConverged = false;
        if(!allCoarseConverged)
        {
            if( true )
            {
                tarch::la::Vector<THREE_POWER_D_TIMES_FOUR_POWER_D, double>  stencils;
                tarch::la::Vector<THREE_POWER_D, double> tmpstencil;
                dfor4(i)
                tmpstencil = fineGridVertices[fineGridVerticesEnumerator(i)].getStencil();
                tarch::la::slice(stencils, tmpstencil, iScalar * THREE_POWER_D);
                enddforx

                dfor2(k)
                if((coarseGridVertices[coarseGridVerticesEnumerator(k)].getRefinementControl() == Vertex::Records::Refined) && coarseGridVertices[coarseGridVerticesEnumerator(k)].isInside() )
                {
                    tarch::la::Vector<FIVE_POWER_D, double> R = coarseGridVertices[coarseGridVerticesEnumerator(k)].getRestriction();
                    matrixfree::solver::boxmg::computeBoxMGIntergridTransferOperator( stencils, R, k );

                    coarseGridVertices[coarseGridVerticesEnumerator(k)].setRestriction(R);
                    //coarseGridVertices[coarseGridVerticesEnumerator(k)].setProlongationI(R);
                    //coarseGridVertices[coarseGridVerticesEnumerator(k)].setProlongation(R);
                }
                enddforx
            }
        }
    }
    break;
    case mg::mappings::ProjectThreeGridDiff::Restriction::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }

    logTraceOut( "descend(...)" );
}


void mg::mappings::SetupBoxMGOperators::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here

    switch(mg::mappings::ProjectThreeGridDiff::restriction)
    {
    case mg::mappings::ProjectThreeGridDiff::Restriction::dlinear:

    case mg::mappings::ProjectThreeGridDiff::Restriction::boxMG:

        dfor2(k)
        if(coarseGridVertices[coarseGridVerticesEnumerator(k)].isInside())
        {
            tarch::la::Vector<DIMENSIONS, int> index;
            tarch::la::Vector<DIMENSIONS, int> indexLocal;
            if(k(0) == 0)
            {
                index(0) = 5;
                indexLocal(0) = 3;
            }
            else
            {
                index(0) = -1;
                indexLocal(0) = 0;
            }
            for(int i = k(1); i < 4 - 1; i++)
            {
                index(1) = i;
                if(k(1) == 0)
                {
                    index(1) = 2 + i;
                    indexLocal(1) = i;
                }
                else
                {
                    index(1) = 2 - i;
                    indexLocal(1) = 3 - i;
                }
                if(fineGridVertices[fineGridVerticesEnumerator(indexLocal)].isInside())
                {

                    tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
                    tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
                    tarch::la::Vector<DIMENSIONS, int> index2;
#ifdef Dim3
                    coarseGridIndex = 2, 2, 2;
#else
                    coarseGridIndex = 2, 2;
#endif
                    for(int a = 0; a < DIMENSIONS; a++)
                    {
                        if(k(a) == 1)
                        {
                            coarseGridMods(a) = -3;
                        }
                        else
                        {
                            coarseGridMods(a) = 0;
                        }
                    }
                    for(int a = 0; a < DIMENSIONS; a++)
                    {
                        index2(a) = coarseGridIndex(a) + coarseGridMods(a) + (indexLocal(a));
                    }


                    tarch::la::Vector<THREE_POWER_D, double> projectionWeights;
                    dfor3(h)
                    projectionWeights(hScalar) = coarseGridVertices[ coarseGridVerticesEnumerator(kScalar) ].getRestrictionElement(index(0) + h(0) - 1, index(1) + h(1) - 1, 0);
                    enddforx
                    tarch::la::Vector<THREE_POWER_D, double> stencil = fineGridVertices[fineGridVerticesEnumerator(indexLocal)].getStencil() * ((1.0) / (fineGridVertices[fineGridVerticesEnumerator(indexLocal)].getStencil()(THREE_POWER_D / 2)));

                    double total = 0.0;
                    dfor3(h)
                    total += projectionWeights(hScalar) * stencil(hScalar);
                    enddforx
                    coarseGridVertices[ coarseGridVerticesEnumerator(kScalar) ].incRestrictionAccVal(total, index(0), index(1), 0);
                }

            }


            if(k(1) == 0)
            {
                index(1) = 5;
                indexLocal(1) = 3;
            }
            else
            {
                index(1) = -1;
                indexLocal(1) = 0;
            }
            for(int i = k(0); i < 4; i++)
            {
                index(0) = i;
                if(k(0) == 0)
                {
                    index(0) = 2 + i;
                    indexLocal(0) = i;
                }
                else
                {
                    index(0) = 2 - i;
                    indexLocal(0) = 3 - i;
                }
                if(fineGridVertices[fineGridVerticesEnumerator(indexLocal)].isInside())
                {

                    tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
                    tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
                    tarch::la::Vector<DIMENSIONS, int> index2;
#ifdef Dim3
                    coarseGridIndex = 2, 2, 2;
#else
                    coarseGridIndex = 2, 2;
#endif
                    for(int a = 0; a < DIMENSIONS; a++)
                    {
                        if(k(a) == 1)
                        {
                            coarseGridMods(a) = -3;
                        }
                        else
                        {
                            coarseGridMods(a) = 0;
                        }
                    }
                    for(int a = 0; a < DIMENSIONS; a++)
                    {
                        index2(a) = coarseGridIndex(a) + coarseGridMods(a) + (indexLocal(a));
                    }


                    tarch::la::Vector<THREE_POWER_D, double> projectionWeights;
                    dfor3(h)
                    projectionWeights(hScalar) = coarseGridVertices[ coarseGridVerticesEnumerator(kScalar) ].getRestrictionElement(index(0) + h(0) - 1, index(1) + h(1) - 1, 0);
                    enddforx
                    tarch::la::Vector<THREE_POWER_D, double> stencil = fineGridVertices[fineGridVerticesEnumerator(indexLocal)].getStencil() * ((1.0) / (fineGridVertices[fineGridVerticesEnumerator(indexLocal)].getStencil()(THREE_POWER_D / 2)));
                    double total = 0.0;
                    dfor3(h)
                    total += projectionWeights(hScalar) * stencil(hScalar);
                    enddforx
                    coarseGridVertices[ coarseGridVerticesEnumerator(kScalar) ].incRestrictionAccVal(total, index(0), index(1), 0);
                }

            }
        }
        enddforx


        break;
    case mg::mappings::ProjectThreeGridDiff::Restriction::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }


    logTraceOut( "ascend(...)" );
}
