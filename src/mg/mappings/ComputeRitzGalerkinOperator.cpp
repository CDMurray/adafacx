#include "mg/mappings/ComputeRitzGalerkinOperator.h"
#include "mg/mappings/ProjectThreeGridDiff.h"
#include "mg/mappings/GenerateStencil.h"

#include "matrixfree/stencil/StencilFactory.h"

#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"
#include <cmath>



/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::ComputeRitzGalerkinOperator::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ComputeRitzGalerkinOperator::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::AvoidCoarseGridRaces,true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ComputeRitzGalerkinOperator::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ComputeRitzGalerkinOperator::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ComputeRitzGalerkinOperator::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ComputeRitzGalerkinOperator::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ComputeRitzGalerkinOperator::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


tarch::logging::Log                mg::mappings::ComputeRitzGalerkinOperator::_log( "mg::mappings::ComputeRitzGalerkinOperator" );
mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations mg::mappings::ComputeRitzGalerkinOperator::equationType = mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Undef;


mg::mappings::ComputeRitzGalerkinOperator::ComputeRitzGalerkinOperator()
{
    logTraceIn( "ComputeRitzGalerkinOperator()" );
    // @todo Insert your code here
    logTraceOut( "ComputeRitzGalerkinOperator()" );
}


mg::mappings::ComputeRitzGalerkinOperator::~ComputeRitzGalerkinOperator()
{
    logTraceIn( "~ComputeRitzGalerkinOperator()" );
    // @todo Insert your code here
    logTraceOut( "~ComputeRitzGalerkinOperator()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::ComputeRitzGalerkinOperator::ComputeRitzGalerkinOperator(const ComputeRitzGalerkinOperator  &masterThread):
    _localState(masterThread._localState)
{
    logTraceIn( "ComputeRitzGalerkinOperator(ComputeRitzGalerkinOperator)" );
    // @todo Insert your code here
    _allConverged = true;
    _updateCoarse = masterThread._updateCoarse;
    logTraceOut( "ComputeRitzGalerkinOperator(ComputeRitzGalerkinOperator)" );
}


void mg::mappings::ComputeRitzGalerkinOperator::mergeWithWorkerThread(const ComputeRitzGalerkinOperator &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(ComputeRitzGalerkinOperator)" );
    // @todo Insert your code here
    if(!workerThread._allConverged)
    {
        //std::cout << _allConverged << " " << workerThread._allConverged << std::endl;
        _allConverged = false;
    }
    logTraceOut( "mergeWithWorkerThread(ComputeRitzGalerkinOperator)" );
}
#endif


void mg::mappings::ComputeRitzGalerkinOperator::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::ComputeRitzGalerkinOperator::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::ComputeRitzGalerkinOperator::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::ComputeRitzGalerkinOperator::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::ComputeRitzGalerkinOperator::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::ComputeRitzGalerkinOperator::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::ComputeRitzGalerkinOperator::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::ComputeRitzGalerkinOperator::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here



    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::ComputeRitzGalerkinOperator::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::ComputeRitzGalerkinOperator::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::ComputeRitzGalerkinOperator::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::ComputeRitzGalerkinOperator::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::ComputeRitzGalerkinOperator::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::ComputeRitzGalerkinOperator::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::ComputeRitzGalerkinOperator::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::ComputeRitzGalerkinOperator::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::ComputeRitzGalerkinOperator::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::ComputeRitzGalerkinOperator::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::ComputeRitzGalerkinOperator::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::ComputeRitzGalerkinOperator::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    switch(equationType)
    {
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Algebraic:
    {
        fineGridVertex.resetAccumulatedValues();
    }
    break;
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Geometric:
        break;
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    }
    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::ComputeRitzGalerkinOperator::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    switch(equationType)
    {
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Algebraic:
    {
        if(fineGridVertex.isInside())
        {
            if((fineGridVertex.getRefinementControl() == Vertex::Records::Refined) && fineGridVertex.isInside())
            {
                if(true)
                {

                    if(fineGridVertex.isDifferentToUpdate())
                    {
                        fineGridVertex.setConverged(false);
                    }
                    else
                    {
                        fineGridVertex.setConverged(true);
                    }
                    fineGridVertex.updateStencil();
                  
                    /*switch(mg::mappings::ProjectThreeGridDiff::type)
                    {
                    case mg::mappings::ProjectThreeGridDiff::Type::Undef:
                        assertionMsg(false, "not properly initialised" );
                        break;
                    case mg::mappings::ProjectThreeGridDiff::Type::adaFACx:
                    case mg::mappings::ProjectThreeGridDiff::Type::AFACx:
                    case mg::mappings::ProjectThreeGridDiff::Type::adaFACxSimple:
                    case mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple:
                    case mg::mappings::ProjectThreeGridDiff::Type::AFACxAlt:
                    case mg::mappings::ProjectThreeGridDiff::Type::AFACxBS:
                    case mg::mappings::ProjectThreeGridDiff::Type::twoProject:
                    case mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple:
                    case mg::mappings::ProjectThreeGridDiff::Type::altRP:
                        //fineGridVertex.updateStencilI();
                        break;
                    case mg::mappings::ProjectThreeGridDiff::Type::Additive:
                    case mg::mappings::ProjectThreeGridDiff::Type::BPX:
                        break;
                    }*/
                }
            }
           
        }
    }
    break;
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Geometric:
        break;
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Undef:
        assertionMsg(false, "not properly initialised" );
        break;

    }

    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::ComputeRitzGalerkinOperator::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );


    switch(equationType)
    {
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Algebraic:
    {


        if(fineGridCell.isRefined())
        {
            bool allVertsConverged = true;
            dfor2(i)
            if((!fineGridVertices[fineGridVerticesEnumerator(i)].isConverged()) && (fineGridVertices[fineGridVerticesEnumerator(i)].isInside()))
            {
                allVertsConverged = false;
            }
            enddforx
            if (allVertsConverged)
            {
                fineGridCell.setConverged(true);
            }
            else
            {
                fineGridCell.setConverged(false);
            }

        }


        bool allCoarseConverged = true;
        dfor2(k)
        if(!coarseGridVertices[ coarseGridVerticesEnumerator(k) ].isConverged())
        {
            allCoarseConverged = false;
        }
        enddforx
        allCoarseConverged = false;

        if(!allCoarseConverged)
        {
            tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> A;
            matrixfree::stencil::VectorOfStencils stencils;
            dfor2(k)
            tarch::la::slice(
                stencils, // into stencils
                tarch::la::Vector<THREE_POWER_D, double>(fineGridVertices[ fineGridVerticesEnumerator(k) ].getStencil()),
                kScalar * THREE_POWER_D
            );
            enddforx
            A = matrixfree::stencil::getElementWiseAssemblyMatrix(stencils);

            tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> oldVals;
            dfor2(i)
            tarch::la::Vector<TWO_POWER_D, double> vector;
            //tarch::la::Vector<TWO_POWER_D, double> vectorI;
            tarch::la::Vector<DIMENSIONS, int>     coarseGridIndex;
            tarch::la::Vector<DIMENSIONS, int>     coarseGridMods;

#ifdef Dim3
            coarseGridIndex = 2, 2, 2;
#else
            coarseGridIndex = 2, 2;
#endif

            for(int a = 0; a < DIMENSIONS; a++)
            {
                if(i(a) == 1)
                {
                    coarseGridMods(a) = -3;
                }
                else
                {
                    coarseGridMods(a) = 0;
                }
            }

            dfor2(j)
            tarch::la::Vector<DIMENSIONS, int> index;
            for(int a = 0; a < DIMENSIONS; a++)
            {
                index(a) = coarseGridIndex(a) + coarseGridMods(a) + (j(a) + fineGridPositionOfCell(a));
            }
#ifdef Dim3
            const int z = index(2);
#else
            const int z = 0;
#endif
            //vector(jScalar) = coarseGridVertices[ coarseGridVerticesEnumerator(iScalar) ].getProlongationElement(index(0), index(1), z);
            vector(jScalar) = coarseGridVertices[ coarseGridVerticesEnumerator(iScalar) ].getRestrictionElement(index(0), index(1), z);
            enddforx


            tarch::la::Vector<TWO_POWER_D, double> v2 = A * vector;
            //tarch::la::Vector<TWO_POWER_D, double> v2I = A * vector;
            dfor2(k)
            double val = v2(kScalar);
            //double valI = v2I(kScalar);
            dfor2(l)
            tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
            tarch::la::Vector<DIMENSIONS, int> coarseGridMods;

#ifdef Dim3
            coarseGridIndex = 2, 2, 2;
#else
            coarseGridIndex = 2, 2;
#endif

            for(int a = 0; a < DIMENSIONS; a++)
            {
                if(l(a) == 1)
                {
                    coarseGridMods(a) = -3;
                }
                else
                {
                    coarseGridMods(a) = 0;
                }
            }

            tarch::la::Vector<DIMENSIONS, int> index;
            for(int a = 0; a < DIMENSIONS; a++)
            {
                index(a) = coarseGridIndex(a) + coarseGridMods(a) + (k(a) + fineGridPositionOfCell(a));
            }

#ifdef Dim3
            const int z = index(2);
#else
            const int z = 0;
#endif
            double projectionWeight = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElement(index(0), index(1), z);
            //double projectionWeightI = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElementIBoundary(index(0), index(1), z);

            int xpos;
            if(i(0) == l(0))
            {
                xpos = 1;
            }
            else if (l(0) == 1)
            {
                xpos = 0;
            }
            else
            {
                xpos = 2;
            }
            int ypos;
            if(i(1) == l(1))
            {
                ypos = 1;
            }
            else if (l(1) == 1)
            {
                ypos = 0;
            }
            else
            {
                ypos = 2;
            }

            int zpos = 0;
#ifdef Dim3
            if(i(2) == l(2))
            {
                zpos = 1;
            }
            else if (l(2) == 1)
            {
                zpos = 0;
            }
            else
            {
                zpos = 2;
            }
#endif

            if(coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRefinementControl() == Vertex::Records::Refined)
            {
                coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].incAccumulatedValue( projectionWeight * val, xpos, ypos, zpos);
                //coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].incAccumulatedValueI( projectionWeightI * valI, xpos, ypos, zpos);
            }
            enddforx
            enddforx
            enddforx
        }
    }
    break;
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Geometric:
        break;
    case mg::mappings::ComputeRitzGalerkinOperator::CoarseGridEquations::Undef:
        assertionMsg(false, "not properly initialised" );
        break;

    }

    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::ComputeRitzGalerkinOperator::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here

    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::ComputeRitzGalerkinOperator::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );
    // @todo Insert your code here
    _localState = solverState;
    _allConverged = true;

    _allDiagonallyDominant = true;

    _updateCoarse = solverState.getUpdateCoarse();
    _updateCoarse = true;
    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::ComputeRitzGalerkinOperator::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here

    if(!_allDiagonallyDominant)
    {
        solverState.setDiagonallyDominant(false);
    }
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::ComputeRitzGalerkinOperator::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "descend(...)" );
}


void mg::mappings::ComputeRitzGalerkinOperator::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}
