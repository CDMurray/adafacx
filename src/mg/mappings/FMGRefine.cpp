#include "mg/mappings/FMGRefine.h"
#include "mg/mappings/RefineGrid.h"
#include "mg/mappings/CreateGrid.h"
#include "mg/mappings/ProjectThreeGridDiff.h"


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::FMGRefine::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::FMGRefine::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::FMGRefine::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::FMGRefine::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::FMGRefine::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::FMGRefine::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::FMGRefine::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


tarch::logging::Log                mg::mappings::FMGRefine::_log( "mg::mappings::FMGRefine" );
bool                               mg::mappings::FMGRefine::initial = false;
bool                               mg::mappings::FMGRefine::_justRefined = false;


mg::mappings::FMGRefine::FMGRefine()
{
    logTraceIn( "FMGRefine()" );
    // @todo Insert your code here
    logTraceOut( "FMGRefine()" );
}


mg::mappings::FMGRefine::~FMGRefine()
{
    logTraceIn( "~FMGRefine()" );
    // @todo Insert your code here
    logTraceOut( "~FMGRefine()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::FMGRefine::FMGRefine(const FMGRefine  &masterThread)
{
    logTraceIn( "FMGRefine(FMGRefine)" );
    // @todo Insert your code here
    logTraceOut( "FMGRefine(FMGRefine)" );
}


void mg::mappings::FMGRefine::mergeWithWorkerThread(const FMGRefine &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(FMGRefine)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithWorkerThread(FMGRefine)" );
}
#endif


void mg::mappings::FMGRefine::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::FMGRefine::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::FMGRefine::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::FMGRefine::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::FMGRefine::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::FMGRefine::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::FMGRefine::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::FMGRefine::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::FMGRefine::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::FMGRefine::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::FMGRefine::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::FMGRefine::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::FMGRefine::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::FMGRefine::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::FMGRefine::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::FMGRefine::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::FMGRefine::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::FMGRefine::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::FMGRefine::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::FMGRefine::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here



    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::FMGRefine::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here



    switch(mg::mappings::CreateGrid::gridType)
    {
    case mg::mappings::CreateGrid::GridType::Undef:
    case mg::mappings::CreateGrid::GridType::StaticRefine:
    case mg::mappings::CreateGrid::GridType::DynamicRefine:
    case mg::mappings::CreateGrid::GridType::DynamicWithInitialRefine:
        break;
    case mg::mappings::CreateGrid::GridType::BoundaryRefine:
    {
        if (
            _performRefine
            &&
            coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::maxLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {

            if(fineGridX(1) < 2 * fineGridH(1))
            {

                fineGridVertex.refine();
                _justRefined = true;
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                //fineGridVertex.setStencilI(fineGridVertex.getStencil());
            }
        }
    }
    break;
    case mg::mappings::CreateGrid::GridType::DiscBoundaryRefine:
    {
        if (
            _performRefine
            &&
            coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::maxLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {

            if((fineGridX(1) < 2 * fineGridH(1)) || std::fabs(fineGridX(0) - 0.5) < fineGridH(0))
            {

                fineGridVertex.refine();
                _justRefined = true;
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                //fineGridVertex.setStencilI(fineGridVertex.getStencil());
            }
        }
    }
    break;
    case mg::mappings::CreateGrid::GridType::CheckBoundaryRefine:
    {
        if (
            _performRefine
            &&
            coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::maxLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {

            double check = false;
            double gradient = mg::mappings::GenerateStencil::gradient;
            if ((fineGridX(0) - gradient * fineGridX(1) < 0.5 + 2 * fineGridH(0)) && (fineGridX(0) - gradient * fineGridX(1) > 0.5 - 2 * fineGridH(0)))
            {
                check = true;
            }
            else if ((fineGridX(1) - gradient * fineGridX(0) < 0.5 + 2 * fineGridH(0)) && (fineGridX(1) - gradient * fineGridX(0) > 0.5 - 2 * fineGridH(0)))
            {
                check = true;
            }

            if((fineGridX(1) < 2 * fineGridH(1)) || check)
            {

                fineGridVertex.refine();
                _justRefined = true;
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                //fineGridVertex.setStencilI(fineGridVertex.getStencil());
            }
        }
    }
    break;
    case mg::mappings::CreateGrid::GridType::LineBoundaryRefine:
    {
        if (
            _performRefine
            &&
            coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::maxLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {

            if(fineGridX(1) < 2 * fineGridH(1))
            {

                fineGridVertex.refine();
                _justRefined = true;
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                //fineGridVertex.setStencilI(fineGridVertex.getStencil());
            }
            else if( (fineGridX(1) < 0.5 + 2 * fineGridH(1)) && std::fabs(fineGridX(0) - 0.5) < 4 * fineGridH(0) + 0.02)
            {
                fineGridVertex.refine();
                _justRefined = true;
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                //fineGridVertex.setStencilI(fineGridVertex.getStencil());
            }
        }
    }
    break;
    case mg::mappings::CreateGrid::GridType::CircleBoundaryRefine:
    {
        if (
            _performRefine
            &&
            coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::maxLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            if(((fineGridX(1) - 0.5) * (fineGridX(1) - 0.5) + (fineGridX(0) - 0.5) * (fineGridX(0) - 0.5) < (0.25 + 2.85 * fineGridH(1)) * (0.25 + 2.85 * fineGridH(1))) && ((fineGridX(1) - 0.5) * (fineGridX(1) - 0.5) + (fineGridX(0) - 0.5) * (fineGridX(0) - 0.5) > (0.25 - 2.85 * fineGridH(1)) * (0.25 - 2.85 * fineGridH(1))))
            {

                fineGridVertex.refine();
                _justRefined = true;
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                //fineGridVertex.setStencilI(fineGridVertex.getStencil());
            }
            else if (fineGridX(1) < 2 * fineGridH(1))
            {
                fineGridVertex.refine();
                _justRefined = true;
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                //fineGridVertex.setStencilI(fineGridVertex.getStencil());

            }
        }
    }
    break;
    case mg::mappings::CreateGrid::GridType::Regular:
        if (
            _performRefine
            &&
            coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::maxLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {

            fineGridVertex.refine();
            _justRefined = true;
            fineGridVertex.setF(0.0);
            fineGridVertex.setFI(0.0);
            fineGridVertex.setUI(0.0);
            fineGridVertex.setResI(0.0);
            fineGridVertex.setLocalCorrectionI(0.0);
            //fineGridVertex.setCorrectionI(0.0);
            //fineGridVertex.setStencilI(0.0);

            fineGridVertex.initialiseDLinearRestriction();
            fineGridVertex.initialiseRestrictionI();
            //fineGridVertex.initialiseDLinearProlongation();
            //fineGridVertex.initialiseProlongationI();
        }
        break;
    case mg::mappings::CreateGrid::GridType::AutomaticBoundaryRefineWithConvergence:
    {
        /*if(!fineGridVertex.getTempConverged())
        {
            break;
        }*/
      break;
    }
    case mg::mappings::CreateGrid::GridType::AutomaticBoundaryRefine:
    {
        if (
            _performRefine
            &&
            coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::maxLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {

            if((fineGridX(1) < 2 * fineGridH(1)))
            {
                if(fineGridVertex.isInside())
                {
                    fineGridVertex.refine();
                    _justRefined = true;
                }
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                fineGridVertex.setLocalCorrectionI(0.0);
                //fineGridVertex.setCorrectionI(0.0);
                //fineGridVertex.setStencilI(0.0);


            }

            break;
        }
    }
    }
    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::FMGRefine::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here

    switch(mg::mappings::CreateGrid::gridType)
    {
    case mg::mappings::CreateGrid::GridType::Undef:
    case mg::mappings::CreateGrid::GridType::StaticRefine:
    case mg::mappings::CreateGrid::GridType::DynamicRefine:
    case mg::mappings::CreateGrid::GridType::DynamicWithInitialRefine:
        break;
    case mg::mappings::CreateGrid::GridType::BoundaryRefine:
    case mg::mappings::CreateGrid::GridType::DiscBoundaryRefine:
    case mg::mappings::CreateGrid::GridType::CheckBoundaryRefine:
    case mg::mappings::CreateGrid::GridType::LineBoundaryRefine:
    case mg::mappings::CreateGrid::GridType::CircleBoundaryRefine:
    case mg::mappings::CreateGrid::GridType::Regular:
        break;
    //check if any vertex has a stencil that hasn't converged
    //if that's the case then don't refine this cell
    case mg::mappings::CreateGrid::GridType::AutomaticBoundaryRefineWithConvergence:
    {
        /*bool converged = true;
        dfor2(x)
        converged &= fineGridVertices[fineGridVerticesEnumerator(x)].getTempConverged();
        enddforx
        if(!converged)
        {
            break;
        }*/
        break;
    }
    case mg::mappings::CreateGrid::GridType::AutomaticBoundaryRefine:
    {
        if (
            _performRefine
            &&
            coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::maxLevel - 1
        )
        {

            const double h = fineGridVerticesEnumerator.getCellSize()(0);
            const tarch::la::Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getCellCenter();
            double sampleEpsilon = mg::mappings::GenerateStencil::getEpsilon(pos);
            bool nearBoundary = false;
            dfor3(x)
            tarch::la::Vector<DIMENSIONS, double> newPos = pos;
            newPos(0) += 0.5 * h * (x(0) - 1);
            newPos(1) += 0.5 * h * (x(1) - 1);
            double epsilonTwo = mg::mappings::GenerateStencil::getEpsilon(newPos);
            if(std::fabs(sampleEpsilon - epsilonTwo) > 1e-8)
            {
                nearBoundary = true;
            }
            enddforx


            dfor2(i)
            mg::Vertex &fineGridVertex = fineGridVertices[fineGridVerticesEnumerator(i)];
            if(nearBoundary &&
                    fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined )
            {
                fineGridVertex.refine();
                _justRefined = true;
                fineGridVertex.setF(0.0);
                fineGridVertex.setFI(0.0);
                fineGridVertex.setUI(0.0);
                fineGridVertex.setResI(0.0);
                fineGridVertex.setLocalCorrectionI(0.0);
                //fineGridVertex.setCorrectionI(0.0);
                //fineGridVertex.setStencilI(0.0);
            }

            enddforx
        }
    }
    break;
    }

    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::FMGRefine::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here



    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::FMGRefine::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );
    // @todo Insert your code here
    _performRefine = solverState.getPerformRefine();
    _justRefined = false;
    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::FMGRefine::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here

    _performRefine = false;
    solverState.setPerformRefine(false);
    solverState.setRecentRefine(false);
    if(_justRefined)
    {
        if(mg::mappings::CreateGrid::currentMaxLevel < mg::mappings::CreateGrid::maxLevel && !solverState.justRefined())
        {
            mg::mappings::CreateGrid::currentMaxLevel++;
        }
        solverState.setRecentRefine(true);
        solverState.setStencilConverged(false);


        //thought this should be +1 but +2 works empirically
        if(mg::mappings::ProjectThreeGridDiff::delayCoarse)
        {
            mg::mappings::CreateGrid::minLevel = mg::mappings::CreateGrid::currentMaxLevel + 2;
        }

    }
    else if(!mg::mappings::RefineGrid::_justRefined)
    {
        solverState.setPerformRefine(false);

    }
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::FMGRefine::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "descend(...)" );
}


void mg::mappings::FMGRefine::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}
