#include "mg/mappings/ProjectThreeGridDiff.h"

#include "mg/mappings/CreateGrid.h"
#include "mg/mappings/GenerateStencil.h"

#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"

#include <cmath>

#include "matrixfree/stencil/StencilFactory.h"



/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::ProjectThreeGridDiff::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ProjectThreeGridDiff::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ProjectThreeGridDiff::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ProjectThreeGridDiff::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ProjectThreeGridDiff::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ProjectThreeGridDiff::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::ProjectThreeGridDiff::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


tarch::logging::Log                mg::mappings::ProjectThreeGridDiff::_log( "mg::mappings::ProjectThreeGridDiff" );
mg::mappings::ProjectThreeGridDiff::Type mg::mappings::ProjectThreeGridDiff::type  = mg::mappings::ProjectThreeGridDiff::Type::Undef;
mg::mappings::ProjectThreeGridDiff::Restriction mg::mappings::ProjectThreeGridDiff::restriction  = mg::mappings::ProjectThreeGridDiff::Restriction::Undef;
bool mg::mappings::ProjectThreeGridDiff::delayCoarse = false;


mg::mappings::ProjectThreeGridDiff::ProjectThreeGridDiff()
{
    logTraceIn( "ProjectThreeGridDiff()" );
    // @todo Insert your code here
    logTraceOut( "ProjectThreeGridDiff()" );
}


mg::mappings::ProjectThreeGridDiff::~ProjectThreeGridDiff()
{
    logTraceIn( "~ProjectThreeGridDiff()" );
    // @todo Insert your code here
    logTraceOut( "~ProjectThreeGridDiff()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::ProjectThreeGridDiff::ProjectThreeGridDiff(const ProjectThreeGridDiff  &masterThread):
    _localState(masterThread._localState)
{
    logTraceIn( "ProjectThreeGridDiff(ProjectThreeGridDiff)" );
    // @todo Insert your code here
    logTraceOut( "ProjectThreeGridDiff(ProjectThreeGridDiff)" );
}


void mg::mappings::ProjectThreeGridDiff::mergeWithWorkerThread(const ProjectThreeGridDiff &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(ProjectThreeGridDiff)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithWorkerThread(ProjectThreeGridDiff)" );
}
#endif


void mg::mappings::ProjectThreeGridDiff::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here


    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::ProjectThreeGridDiff::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::ProjectThreeGridDiff::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::ProjectThreeGridDiff::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::ProjectThreeGridDiff::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::ProjectThreeGridDiff::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here


    //}
    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::ProjectThreeGridDiff::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::ProjectThreeGridDiff::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::ProjectThreeGridDiff::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::ProjectThreeGridDiff::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::ProjectThreeGridDiff::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::ProjectThreeGridDiff::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::ProjectThreeGridDiff::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::ProjectThreeGridDiff::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here

    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::ProjectThreeGridDiff::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::ProjectThreeGridDiff::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::ProjectThreeGridDiff::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here

    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::ProjectThreeGridDiff::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::ProjectThreeGridDiff::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::ProjectThreeGridDiff::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );


    if((delayCoarse) && _hasRefined)
    {
        if((coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::minLevel) && (fineGridVertex.getRefinementControl() == Vertex::Records::Refined))
        {
            fineGridVertex.setOmega(0.0);
            fineGridVertex.setOmegaI(0.0);
        }
    }


    //fineGridVertex.backupD();
    //fineGridVertex.backupDI();

    bool converged = true;
    dfor2(x)
    converged &= coarseGridVertices[coarseGridVerticesEnumerator(x)].isConverged();
    enddforx

    /*if((!converged) || (!fineGridVertex.isConverged()))
    {
        fineGridVertex.setTempConverged(false);
    }
    else
    {
        fineGridVertex.setTempConverged(true);
    }*/

    if(fineGridVertex.isInside())
    {

        double uInterpolated = 0.0; //double uInterpolatedI;
        double u; //double uI;
        double diffMod = 0.0;
        double diffModI = 0.0;
        double bpxMod = 0.0;
        u = fineGridVertex.getU();

        double uH; //double uHI;
        switch(restriction)
        {
        case mg::mappings::ProjectThreeGridDiff::Restriction::dlinear:
        {
            //diffMod = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCCoarse(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
            diffMod = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCLocal(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
            if((mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACx) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::altRP) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACxAlt))
            {
                diffModI = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCLocalI(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);

                tarch::la::Vector<TWO_POWER_D, double > weightVectorI;
                dfor2(l)
                tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
                tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
#ifdef Dim3
                coarseGridIndex = 2, 2, 2;
#else
                coarseGridIndex = 2, 2;
#endif
                for(int a = 0; a < DIMENSIONS; a++)
                {
                    if(l(a) == 1)
                    {
                        coarseGridMods(a) = -3;
                    }
                    else
                    {
                        coarseGridMods(a) = 0;
                    }
                }
                tarch::la::Vector<DIMENSIONS, int> index;
                for(int a = 0; a < DIMENSIONS; a++)
                {
                    index(a) = coarseGridIndex(a) + coarseGridMods(a) + (fineGridPositionOfVertex(a));
                }
#ifdef Dim3
                const int z = index(2);
#else
                const int z = 0;
#endif
                double projectionWeightI;
                if(coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRefinementControl() == Vertex::Records::Refined)
                {
                    projectionWeightI = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElementI(index(0), index(1), z);

                }
                else
                {
                    projectionWeightI = 0.0;
                }
                weightVectorI(lScalar) = projectionWeightI;
                enddforx
                //diffModI += VertexOperations::readCLocalI(coarseGridVerticesEnumerator, coarseGridVertices) * weightVectorI * fineGridVertex.getOmega();
            }
            else if(mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::twoProject || mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple)
            {

                //diffModI = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCCoarseI(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
                diffModI = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCLocalI(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
                tarch::la::Vector<TWO_POWER_D, double > weightVectorI;
                dfor2(l)
                tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
                tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
#ifdef Dim3
                coarseGridIndex = 2, 2, 2;
#else
                coarseGridIndex = 2, 2;
#endif
                for(int a = 0; a < DIMENSIONS; a++)
                {
                    if(l(a) == 1)
                    {
                        coarseGridMods(a) = -3;
                    }
                    else
                    {
                        coarseGridMods(a) = 0;
                    }
                }
                tarch::la::Vector<DIMENSIONS, int> index;
                for(int a = 0; a < DIMENSIONS; a++)
                {
                    index(a) = coarseGridIndex(a) + coarseGridMods(a) + (fineGridPositionOfVertex(a));
                }
#ifdef Dim3
                const int z = index(2);
#else
                const int z = 0;
#endif
                double projectionWeightI;
                if(coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRefinementControl() == Vertex::Records::Refined)
                {
                    projectionWeightI = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElementI(index(0), index(1), z);

                }
                else
                {
                    projectionWeightI = 0.0;
                }
                weightVectorI(lScalar) = projectionWeightI;
                enddforx

                diffModI -= fineGridVertex.getOmega() * VertexOperations::readCLocalI(coarseGridVerticesEnumerator, coarseGridVertices) * weightVectorI;
                diffModI += fineGridVertex.getOmega() * VertexOperations::readCLocal(coarseGridVerticesEnumerator, coarseGridVertices) * weightVectorI;
                diffModI -= (1 - fineGridVertex.getOmega()) * fineGridVertex.getLocalCorrection();
            }
            else
            {
                //diffModI = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCCoarseI(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
            }
            uInterpolated = _multigrid.getDLinearInterpolatedValue(VertexOperations::readU(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);


            bpxMod = _multigrid.getDLinearInterpolatedValue(VertexOperations::readCBPX(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
        }
        break;
        case mg::mappings::ProjectThreeGridDiff::Restriction::boxMG:
        {
            tarch::la::Vector<TWO_POWER_D, double > weightVector;
            tarch::la::Vector<TWO_POWER_D, double > weightVectorI;
            dfor2(l)
            tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
            tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
#ifdef Dim3
            coarseGridIndex = 2, 2, 2;
#else
            coarseGridIndex = 2, 2;
#endif
            for(int a = 0; a < DIMENSIONS; a++)
            {
                if(l(a) == 1)
                {
                    coarseGridMods(a) = -3;
                }
                else
                {
                    coarseGridMods(a) = 0;
                }
            }
            tarch::la::Vector<DIMENSIONS, int> index;
            for(int a = 0; a < DIMENSIONS; a++)
            {
                index(a) = coarseGridIndex(a) + coarseGridMods(a) + (fineGridPositionOfVertex(a));
            }
#ifdef Dim3
            const int z = index(2);
#else
            const int z = 0;
#endif
            //double projectionWeight = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getProlongationElement(index(0), index(1), z);
            double projectionWeight = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElement(index(0), index(1), z);
            weightVector(lScalar) = projectionWeight;
            enddforx

            uInterpolated = VertexOperations::readU(coarseGridVerticesEnumerator, coarseGridVertices) * weightVector;
            //diffMod = VertexOperations::readCCoarse(coarseGridVerticesEnumerator, coarseGridVertices) * weightVector;
            //diffModI = VertexOperations::readCCoarseI(coarseGridVerticesEnumerator, coarseGridVertices) * weightVector;
            diffMod = VertexOperations::readCLocal(coarseGridVerticesEnumerator, coarseGridVertices) * weightVector;
            //diffModI = VertexOperations::readCCoarseI(coarseGridVerticesEnumerator, coarseGridVertices) * weightVector;

            if((mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACx) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACxAlt))
            {
                dfor2(l)
                tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
                tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
#ifdef Dim3
                coarseGridIndex = 2, 2, 2;
#else
                coarseGridIndex = 2, 2;
#endif
                for(int a = 0; a < DIMENSIONS; a++)
                {
                    if(l(a) == 1)
                    {
                        coarseGridMods(a) = -3;
                    }
                    else
                    {
                        coarseGridMods(a) = 0;
                    }
                }
                tarch::la::Vector<DIMENSIONS, int> index;
                for(int a = 0; a < DIMENSIONS; a++)
                {
                    index(a) = coarseGridIndex(a) + coarseGridMods(a) + (fineGridPositionOfVertex(a));
                }
#ifdef Dim3
                const int z = index(2);
#else
                const int z = 0;
#endif
                double projectionWeightI;
                if(coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRefinementControl() == Vertex::Records::Refined)
                {
                    projectionWeightI = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElementI(index(0), index(1), z);
                }
                else
                {
                    projectionWeightI = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElementI(index(0), index(1), z);
                }

                weightVectorI(lScalar) = projectionWeightI;
                enddforx

                //diffModI += VertexOperations::readCLocalI(coarseGridVerticesEnumerator, coarseGridVertices) * weightVectorI * fineGridVertex.getOmega();

            }
            else if(mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::twoProject || mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple)
            {

                tarch::la::Vector<TWO_POWER_D, double > weightVectorI;
                dfor2(l)
                tarch::la::Vector<DIMENSIONS, int> coarseGridIndex;
                tarch::la::Vector<DIMENSIONS, int> coarseGridMods;
#ifdef Dim3
                coarseGridIndex = 2, 2, 2;
#else
                coarseGridIndex = 2, 2;
#endif
                for(int a = 0; a < DIMENSIONS; a++)
                {
                    if(l(a) == 1)
                    {
                        coarseGridMods(a) = -3;
                    }
                    else
                    {
                        coarseGridMods(a) = 0;
                    }
                }
                tarch::la::Vector<DIMENSIONS, int> index;
                for(int a = 0; a < DIMENSIONS; a++)
                {
                    index(a) = coarseGridIndex(a) + coarseGridMods(a) + (fineGridPositionOfVertex(a));
                }
#ifdef Dim3
                const int z = index(2);
#else
                const int z = 0;
#endif
                double projectionWeightI;
                projectionWeightI = coarseGridVertices[ coarseGridVerticesEnumerator(lScalar) ].getRestrictionElementI(index(0), index(1), z);
                weightVectorI(lScalar) = projectionWeightI;
                enddforx
                diffModI -= fineGridVertex.getOmega() * VertexOperations::readCLocalI(coarseGridVerticesEnumerator, coarseGridVertices) * weightVectorI;
                diffModI += fineGridVertex.getOmega() * VertexOperations::readCLocal(coarseGridVerticesEnumerator, coarseGridVertices) * weightVectorI;
                //diffModI += fineGridVertex.getTempCorrections();
                //diffModI -= fineGridVertex.getLocalCorrectionTwo();
                diffModI -= (1 - fineGridVertex.getOmega()) * fineGridVertex.getLocalCorrection();



            }

            bpxMod = VertexOperations::readCBPX(coarseGridVerticesEnumerator, coarseGridVertices) * weightVector;
        }
        break;
        case mg::mappings::ProjectThreeGridDiff::Restriction::Undef:
            assertionMsg(false, "not properly initialised" );
            break;

        }


        switch (type)
        {
        case mg::mappings::ProjectThreeGridDiff::Type::Undef:
            assertionMsg(false, "not properly initialised" );
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::adaFACx:
        case mg::mappings::ProjectThreeGridDiff::Type::adaFACxSimple:
        case mg::mappings::ProjectThreeGridDiff::Type::twoProject:
            //u += fineGridVertex.getFineCorrection();

            //this is zero on the finest grid and only non-zero on coarser grids
            //might need to apply to make sure fine grids are in line with coarse grids however
            if((delayCoarse) && !_hasRefined)
            {
                //u -= (fineGridVertex.getLocalCorrectionI() + fineGridVertex.getFineCorrectionI());
                //u += diffMod - diffModI;
                u += diffMod;

            }
            else if(!delayCoarse)
            {
                //u -= (fineGridVertex.getLocalCorrectionI() + fineGridVertex.getFineCorrectionI());
                //u += diffMod - diffModI;
                u += diffMod;

            }
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::AFACx:
        case mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple:
        case mg::mappings::ProjectThreeGridDiff::Type::AFACxAlt:
            //u += fineGridVertex.getFineCorrection();

            //this is zero on the finest grid and only non-zero on coarser grids
            //might need to apply to make sure fine grids are in line with coarse grids however
            if((delayCoarse) && !_hasRefined)
            {
                //u -= fineGridVertex.getOmega() * fineGridVertex.getRestrictionElementI(2, 2, 0) * (fineGridVertex.getLocalCorrectionI()) + fineGridVertex.getFineCorrectionI();
                //u += diffMod - diffModI;
                u += diffMod - diffModI;

            }
            else if(!delayCoarse)
            {
                //u -= fineGridVertex.getOmega() * fineGridVertex.getRestrictionElementI(2, 2, 0) * (fineGridVertex.getLocalCorrectionI()) + fineGridVertex.getFineCorrectionI();
                //u += diffMod - diffModI;
                u += diffMod - diffModI;

            }
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple:{
            //u += (2 - fineGridVertex.getOmega()) * fineGridVertex.getFineCorrection();
            //u -= (fineGridVertex.getLocalCorrectionI() + fineGridVertex.getFineCorrectionI());
            u += diffMod - diffModI;}
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::altRP:{
            //u += fineGridVertex.getFineCorrection();
            //this is zero on the finest grid and only non-zero on coarser grids
            //might need to apply to make sure fine grids are in line with coarse grids however
            //u += (fineGridVertex.getLocalCorrectionI() + fineGridVertex.getFineCorrectionI());
            u += diffMod + diffModI;}
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::Additive:

            if((delayCoarse) && !_hasRefined)
            {
                //u += fineGridVertex.getFineCorrection();
                //u += diffMod;
                u += diffMod;

            }
            else if(!delayCoarse)
            {
                //u += fineGridVertex.getFineCorrection();
                //u += diffMod;
                u += diffMod;
            }
            else
            {

                //u += fineGridVertex.getFineCorrection();
            }
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::BPX:
            if((delayCoarse) && !_hasRefined)
            {
                //u += fineGridVertex.getFineCorrection();
                //u += diffMod;
                u += diffMod;
                //actual bpx line
                if(!peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex))
                {
                    const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
                    if(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getOmega() > 0.0)
                    {
                        u -= bpxMod;
                        diffMod -= bpxMod;
                    }
                }
            }
            else if (!delayCoarse)
            {
                //u += fineGridVertex.getFineCorrection();
                //u += diffMod;
                u += diffMod;
                //actual bpx line
                if(!peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex))
                {
                    const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
                    if(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getOmega() > 0.0)
                    {
                        u -= bpxMod;
                        diffMod -= bpxMod;
                    }
                }
            }
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::AFACxBS:
            break;
        }

        uH = u - uInterpolated;

        if(!fineGridVertex.isHangingNode())
        {
            fineGridVertex.setU(u);
            fineGridVertex.setUH(uH);
        }
        //fineGridVertex.setCoarseCorrection(fineGridVertex.getLocalCorrection() + diffMod);
        fineGridVertex.setLocalCorrection(fineGridVertex.getLocalCorrection() + diffMod);
        if((mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACx) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACxAlt))
        {
            //fineGridVertex.setCoarseCorrectionI( diffModI);
            fineGridVertex.setLocalCorrectionI( diffModI);
        }
        else
        {
            //fineGridVertex.setCoarseCorrectionI(fineGridVertex.getLocalCorrectionI() + diffModI);
            fineGridVertex.setLocalCorrectionI(fineGridVertex.getLocalCorrectionI() + diffModI);
        }
    }
    else
    {
        // boundary point.
        double uInterpolated = _multigrid.getDLinearInterpolatedValue(VertexOperations::readU(coarseGridVerticesEnumerator, coarseGridVertices), fineGridPositionOfVertex);
        double u = fineGridVertex.getU();
        fineGridVertex.setUH(u - uInterpolated);
    }


    fineGridVertex.reset();
    //fineGridVertex.resetFineCorrections();

    //fineGridVertex.resetFineCorrectionsI();
    fineGridVertex.resetAccumulatedValues();

    fineGridVertex.setUI(0.0);
    //fineGridVertex.setUTwo(0.0);
    if(fineGridVertex.getRefinementControl() != Vertex::Records::Unrefined)
    {
        //fineGridVertex.setUTwo(0.0);
        VertexOperations::writeF(fineGridVertex, 0.0);
    }
    VertexOperations::writeFI(fineGridVertex, 0.0);


    if((coarseGridVerticesEnumerator.getLevel() < mg::mappings::CreateGrid::minLevel - 1) && (fineGridVertex.getRefinementControl() == Vertex::Records::Refined))
    {
        fineGridVertex.setOmega(0.0);
        fineGridVertex.setOmegaI(0.0);
    }
    else if ((delayCoarse) )
    {
        //will want something that takes into account grid depth for exponential multigrid
        //also need some sort of funky check so that this only happens in setups where I desire it :P
        fineGridVertex.resetOmega();
        if ( mg::mappings::CreateGrid::exponentialDamping )
        {
            const double newOmega = std::pow(fineGridVertex.getOmega(), mg::mappings::CreateGrid::maxLevel - coarseGridVerticesEnumerator.getLevel());
            fineGridVertex.initOmega( newOmega );
        }
    }


    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::ProjectThreeGridDiff::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    //fineGridVertex.resetCoarseCorrections();
    //fineGridVertex.resetCoarseCorrectionsI();
    fineGridVertex.resetLocalCorrections();
    fineGridVertex.resetLocalCorrectionsI();
    //fineGridVertex.resetLocalCorrectionsTwo();
    //fineGridVertex.resetTempCorrections();

    if(peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex))
    {
        const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
        coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].resetBPXCorrections();
    }

    if(fineGridVertex.isInside())
    {
        _count++;
    }
    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::ProjectThreeGridDiff::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::ProjectThreeGridDiff::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::ProjectThreeGridDiff::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );

    _localState = solverState;
    _count = 0;
    _hasRefined = solverState.justRefined();

    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::ProjectThreeGridDiff::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here

    logInfo( "endIteration()", "number of vertices touched " << _count );
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::ProjectThreeGridDiff::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here


    logTraceOut( "descend(...)" );
}


void mg::mappings::ProjectThreeGridDiff::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}
