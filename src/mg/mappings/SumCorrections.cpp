#include "mg/mappings/SumCorrections.h"

#include "mg/mappings/ProjectThreeGridDiff.h"

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::SumCorrections::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SumCorrections::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SumCorrections::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SumCorrections::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SumCorrections::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SumCorrections::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::SumCorrections::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
}


tarch::logging::Log                mg::mappings::SumCorrections::_log( "mg::mappings::SumCorrections" );


mg::mappings::SumCorrections::SumCorrections()
{
    logTraceIn( "SumCorrections()" );
    // @todo Insert your code here
    logTraceOut( "SumCorrections()" );
}


mg::mappings::SumCorrections::~SumCorrections()
{
    logTraceIn( "~SumCorrections()" );
    // @todo Insert your code here
    logTraceOut( "~SumCorrections()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::SumCorrections::SumCorrections(const SumCorrections  &masterThread)
{
    logTraceIn( "SumCorrections(SumCorrections)" );
    // @todo Insert your code here
    logTraceOut( "SumCorrections(SumCorrections)" );
}


void mg::mappings::SumCorrections::mergeWithWorkerThread(const SumCorrections &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(SumCorrections)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithWorkerThread(SumCorrections)" );
}
#endif


void mg::mappings::SumCorrections::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::SumCorrections::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::SumCorrections::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::SumCorrections::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::SumCorrections::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::SumCorrections::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::SumCorrections::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::SumCorrections::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here

    if(vertex.isInside())
    {
        vertex.setLocalCorrection(vertex.getLocalCorrection() + neighbour.getLocalCorrection());
        vertex.setCorrection(vertex.getLocalCorrection());
        vertex.setFineCorrection(vertex.getFineCorrection() + neighbour.getFineCorrection());
        vertex.setBPXCorrection(vertex.getBPXCorrection() + neighbour.getBPXCorrection());
        vertex.setLocalCorrectionI(vertex.getLocalCorrectionI() + neighbour.getLocalCorrectionI());
        vertex.setCorrectionI(vertex.getLocalCorrectionI());
        vertex.setFineCorrectionI(vertex.getFineCorrectionI() + neighbour.getFineCorrectionI());
    }
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::SumCorrections::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::SumCorrections::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::SumCorrections::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::SumCorrections::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::SumCorrections::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::SumCorrections::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::SumCorrections::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::SumCorrections::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::SumCorrections::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::SumCorrections::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::SumCorrections::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::SumCorrections::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here

    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::SumCorrections::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    if(fineGridVertex.isInside())
    {

        double potentialUpdate = 0.0;
        double potentialUpdateI = 0.0;
        switch (mg::mappings::ProjectThreeGridDiff::type)
        {
        case mg::mappings::ProjectThreeGridDiff::Type::Undef:
            assertionMsg(false, "not properly initialised" );
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::adaFACx:
        case mg::mappings::ProjectThreeGridDiff::Type::adaFACxSimple:
        case mg::mappings::ProjectThreeGridDiff::Type::twoProject:
        case mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple:
        case mg::mappings::ProjectThreeGridDiff::Type::altRP:
                potentialUpdateI = fineGridVertex.getPotentialUpdateIDistinctOmega();
                potentialUpdate = fineGridVertex.getPotentialUpdate();
                fineGridVertex.setU(fineGridVertex.getU()+potentialUpdate-potentialUpdateI);
                fineGridVertex.setLocalCorrection(potentialUpdate-potentialUpdateI);
            /*if(_useAlternativeDamper)
            {
                potentialUpdateI = fineGridVertex.getPotentialUpdateIAltDistinctOmega();
                potentialUpdate = fineGridVertex.getPotentialUpdateAlt();
            }
            else
            {
                potentialUpdateI = fineGridVertex.getPotentialUpdateIDistinctOmega();
                potentialUpdate = fineGridVertex.getPotentialUpdate();
            }*/
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::AFACx:
        case mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple:
                //potentialUpdateI = fineGridVertex.getPotentialUpdateI();
                potentialUpdate = fineGridVertex.getPotentialUpdate();
                fineGridVertex.setU(fineGridVertex.getU()+potentialUpdate);
                fineGridVertex.setLocalCorrection(potentialUpdate);

            if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) ){
              const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
                if(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getOmega() > 0.0)
                {
                    potentialUpdateI = 0.0;
                }
                else
                {
                    potentialUpdateI = fineGridVertex.getPotentialUpdateI();
                }
                    coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setLocalCorrectionI(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getLocalCorrectionI() + potentialUpdateI);

            }
            /*if(_useAlternativeDamper)
            {
                potentialUpdateI = fineGridVertex.getPotentialUpdateIAlt();
                potentialUpdate = fineGridVertex.getPotentialUpdateAlt();
            }
            else
            {
                potentialUpdateI = fineGridVertex.getPotentialUpdateI();
                potentialUpdate = fineGridVertex.getPotentialUpdate();
            }*/
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::AFACxAlt:
                potentialUpdateI = fineGridVertex.getPotentialUpdateI();
                if(fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined)
                {
                    potentialUpdateI = 0.0;
                }
                else
                {
                    potentialUpdateI = fineGridVertex.getOmega() * fineGridVertex.getPotentialUpdate();
                }
                potentialUpdate = fineGridVertex.getPotentialUpdate();
                fineGridVertex.setU(fineGridVertex.getU()+potentialUpdate);
                fineGridVertex.setLocalCorrection(potentialUpdate);

            /*if(_useAlternativeDamper)
            {
                potentialUpdateI = fineGridVertex.getPotentialUpdateIAlt();
                if(fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined)
                {
                    potentialUpdateI = 0.0;
                }
                else
                {
                    potentialUpdateI = fineGridVertex.getPotentialUpdateAlt();
                }
                potentialUpdate = fineGridVertex.getPotentialUpdateAlt();
            }
            else
            {
                potentialUpdateI = fineGridVertex.getPotentialUpdateI();
                if(fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined)
                {
                    potentialUpdateI = 0.0;
                }
                else
                {
                    potentialUpdateI = fineGridVertex.getOmega() * fineGridVertex.getPotentialUpdate();
                }
                potentialUpdate = fineGridVertex.getPotentialUpdate();
            }*/
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::Additive:
        case mg::mappings::ProjectThreeGridDiff::Type::AFACxBS:
                potentialUpdate = fineGridVertex.getPotentialUpdate();
                fineGridVertex.setU(fineGridVertex.getU()+potentialUpdate);
                fineGridVertex.setLocalCorrection(potentialUpdate);
            /*if(_useAlternativeDamper)
            {
                potentialUpdate = fineGridVertex.getPotentialUpdateAlt();
            }
            else
            {
                potentialUpdate = fineGridVertex.getPotentialUpdate();
            }*/
            break;
        case mg::mappings::ProjectThreeGridDiff::Type::BPX:
            if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) )
            {
                const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
                if(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getOmega() > 0.0)
                {
                    potentialUpdate = 0.0;
                }
                else
                {
                    potentialUpdate = fineGridVertex.getPotentialUpdate();
                }
                    coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setBPXCorrection(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getBPXCorrection() + fineGridVertex.getPotentialUpdate());
                
                fineGridVertex.setLocalCorrection(0.0);

                //fineGridVertex.setDiff(0.0);
                //fineGridVertex.setDiffI(0.0);
            }
            else
            {
                    potentialUpdate = fineGridVertex.getPotentialUpdate();
            }
            /*if ( peano::grid::SingleLevelEnumerator::isVertexPositionAlsoACoarseVertexPosition(fineGridPositionOfVertex) )
            {
                const peano::grid::SingleLevelEnumerator::LocalVertexIntegerIndex coarseGridPosition = peano::grid::SingleLevelEnumerator::getVertexPositionOnCoarserLevel(fineGridPositionOfVertex);
                if(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getOmega() > 0.0)
                {
                    potentialUpdate = 0.0;
                }
                else
                {
                    potentialUpdate = fineGridVertex.getPotentialUpdate();
                }
                if(_useAlternativeDamper)
                {
                    coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setBPXCorrection(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getBPXCorrection() + fineGridVertex.getPotentialUpdateAlt());
                }
                else
                {
                    coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].setBPXCorrection(coarseGridVertices[ coarseGridVerticesEnumerator(coarseGridPosition) ].getBPXCorrection() + fineGridVertex.getPotentialUpdate());
                }
                fineGridVertex.setDiff(0.0);
                fineGridVertex.setDiffI(0.0);
            }
            else
            {
                if(_useAlternativeDamper)
                {
                    potentialUpdate = fineGridVertex.getPotentialUpdateAlt();
                }
                else
                {
                    potentialUpdate = fineGridVertex.getPotentialUpdate();
                }
            }*/
            break;


        }

        /*fineGridVertex.setCorrection(potentialUpdate);
        fineGridVertex.setLocalCorrection(potentialUpdate);
        fineGridVertex.setCoarseCorrection(fineGridVertex.getLocalCorrection());

        fineGridVertex.setUTwo(potentialUpdate);*/


        if((mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::adaFACxSimple) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple))
        {
            //fineGridVertex.setCorrectionI(potentialUpdateI);
            fineGridVertex.setLocalCorrectionI(potentialUpdateI);
            //fineGridVertex.setCoarseCorrectionI(fineGridVertex.getLocalCorrectionI());
        }


        if(fineGridVertex.getRefinementControl() != Vertex::Records::Refined)
        {
            fineGridVertex.setTempRes(fineGridVertex.getUpdatedRes());
        }
        else
        {
            fineGridVertex.setTempRes(0.0);
        }
    }

    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::SumCorrections::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::SumCorrections::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::SumCorrections::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );
    // @todo Insert your code here
    _useAlternativeDamper = solverState.useAlternativeDamping();
    //_useAlternativeDamper = false;
    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::SumCorrections::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::SumCorrections::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "descend(...)" );
}


void mg::mappings::SumCorrections::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}
