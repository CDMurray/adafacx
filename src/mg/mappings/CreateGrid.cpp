#include "mg/mappings/CreateGrid.h"
#include <math.h>

#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification   mg::mappings::CreateGrid::communicationSpecification() const
{
    return peano::CommunicationSpecification(
               peano::CommunicationSpecification::ExchangeMasterWorkerData::SendDataAndStateBeforeFirstTouchVertexFirstTime,
               peano::CommunicationSpecification::ExchangeWorkerMasterData::SendDataAndStateAfterLastTouchVertexLastTime,
               false
           );
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateGrid::touchVertexLastTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateGrid::touchVertexFirstTimeSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::RunConcurrentlyOnFineGrid, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateGrid::enterCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateGrid::leaveCellSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidFineGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::RunConcurrentlyOnFineGrid,true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateGrid::ascendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::RunConcurrentlyOnFineGrid,true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification   mg::mappings::CreateGrid::descendSpecification(int level) const
{
    return peano::MappingSpecification(peano::MappingSpecification::WholeTree, peano::MappingSpecification::AvoidCoarseGridRaces, true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::RunConcurrentlyOnFineGrid,true);
    //return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::Serial,true);
}


tarch::logging::Log                mg::mappings::CreateGrid::_log( "mg::mappings::CreateGrid" );
int                                mg::mappings::CreateGrid::maxLevel = 5;
int                                mg::mappings::CreateGrid::initialLevel = 3;
int                                mg::mappings::CreateGrid::currentMaxLevel = 3;
int                                mg::mappings::CreateGrid::minLevel = 0;
int                                mg::mappings::CreateGrid::finalMinLevel = 0;
mg::mappings::CreateGrid::GridType mg::mappings::CreateGrid::gridType = mg::mappings::CreateGrid::GridType::Undef;
bool                               mg::mappings::CreateGrid::exponentialDamping = false;
bool                               mg::mappings::CreateGrid::backgroundStencil = false;


mg::mappings::CreateGrid::CreateGrid()
{
    logTraceIn( "CreateGrid()" );
    // @todo Insert your code here
    logTraceOut( "CreateGrid()" );
}


mg::mappings::CreateGrid::~CreateGrid()
{
    logTraceIn( "~CreateGrid()" );
    // @todo Insert your code here
    logTraceOut( "~CreateGrid()" );
}


#if defined(SharedMemoryParallelisation)
mg::mappings::CreateGrid::CreateGrid(const CreateGrid  &masterThread):
    _localState(masterThread._localState)
{
    logTraceIn( "CreateGrid(CreateGrid)" );
    // @todo Insert your code here
    logTraceOut( "CreateGrid(CreateGrid)" );
}


void mg::mappings::CreateGrid::mergeWithWorkerThread(const CreateGrid &workerThread)
{
    logTraceIn( "mergeWithWorkerThread(CreateGrid)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithWorkerThread(CreateGrid)" );
}
#endif


void mg::mappings::CreateGrid::createHangingVertex(
    mg::Vertex     &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                &fineGridH,
    mg::Vertex *const   coarseGridVertices,
    const peano::grid::VertexEnumerator      &coarseGridVerticesEnumerator,
    mg::Cell       &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                   &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "createHangingVertex(...)", fineGridVertex );
}


void mg::mappings::CreateGrid::destroyHangingVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyHangingVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyHangingVertex(...)", fineGridVertex );
}


void mg::mappings::CreateGrid::createInnerVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createInnerVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );

    fineGridVertex.init(fineGridX, 0.0);

    fineGridVertex.reset();
    fineGridVertex.resetC();
    double h = fineGridH(0);
    fineGridVertex.setF( std::pow( h, DIMENSIONS) * fineGridVertex.getF());
    fineGridVertex.setFI(fineGridVertex.getF());

    if(coarseGridVerticesEnumerator.getLevel() < minLevel - 1)
    {
        fineGridVertex.setOmega(0.0);
    }
    if(coarseGridVerticesEnumerator.getLevel() < minLevel - 1)
    {
        fineGridVertex.setOmegaI(0.0);
    }

    switch (gridType)
    {
    case GridType::Undef:
        break;
    case GridType::Regular:
    case GridType::DynamicRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }
    break;
    case GridType::StaticRefine:
    case GridType::DynamicWithInitialRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }


        if(
            std::fabs(fineGridX(0) - 0.5) < fineGridH(0)
            and
            coarseGridVerticesEnumerator.getLevel() < maxLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), initialLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }
    break;
    case GridType::BoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }

    }

    break;
    case GridType::DiscBoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }

    break;
    case GridType::CheckBoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }

    break;
    case GridType::CircleBoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }

    break;
    case GridType::LineBoundaryRefine:
    {
        if(
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }

    break;
    case GridType::AutomaticBoundaryRefineWithConvergence:
    case GridType::AutomaticBoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }
    break;
    }

    logTraceOutWith1Argument( "createInnerVertex(...)", fineGridVertex );
}


void mg::mappings::CreateGrid::createBoundaryVertex(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "createBoundaryVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );

    switch (gridType)
    {
    case GridType::Undef:
        break;
    case GridType::Regular:
    case GridType::DynamicRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
        }
    }
    break;
    case GridType::StaticRefine:
    case GridType::DynamicWithInitialRefine:
    {
        if(
            std::fabs(fineGridX(0) - 0.5) < fineGridH(0)
            and
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
        }
    }
    break;
    case GridType::BoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
        }

    }
    break;
    case GridType::DiscBoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
        }
    }
    break;
    case GridType::CircleBoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
        }
    }
    break;
    case GridType::CheckBoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }

    break;
    case GridType::LineBoundaryRefine:
    {
        if (coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
                and
                fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined)
        {

            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }
    break;
    case GridType::AutomaticBoundaryRefineWithConvergence:
    case GridType::AutomaticBoundaryRefine:
    {
        if (
            coarseGridVerticesEnumerator.getLevel() < initialLevel - 1
            and
            fineGridVertex.getRefinementControl() == Vertex::Records::Unrefined
        )
        {
            fineGridVertex.refine();
            if ( exponentialDamping )
            {
                const double newOmega = std::pow(fineGridVertex.getOmega(), maxLevel - coarseGridVerticesEnumerator.getLevel());
                fineGridVertex.initOmega( newOmega );
            }
        }
    }
    break;
    }

    fineGridVertex.init(fineGridX, 0.0);

    fineGridVertex.reset();
    fineGridVertex.resetC();
    double h = fineGridH(0);
    fineGridVertex.setF( std::pow( h, DIMENSIONS) * fineGridVertex.getF());


    logTraceOutWith1Argument( "createBoundaryVertex(...)", fineGridVertex );
}


void mg::mappings::CreateGrid::destroyVertex(
    const mg::Vertex   &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "destroyVertex(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyVertex(...)", fineGridVertex );
}


void mg::mappings::CreateGrid::createCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "createCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );

    fineGridCell.setConverged(false);

    // CreateGrid does not set any stencils, as it is the grid creation. The
    // stencil initialisation is found in CreateStencil.

    logTraceOutWith1Argument( "createCell(...)", fineGridCell );
}


void mg::mappings::CreateGrid::destroyCell(
    const mg::Cell           &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "destroyCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "destroyCell(...)", fineGridCell );
}

#ifdef Parallel
void mg::mappings::CreateGrid::mergeWithNeighbour(
    mg::Vertex  &vertex,
    const mg::Vertex  &neighbour,
    int                                           fromRank,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>   &fineGridH,
    int                                           level
)
{
    logTraceInWith6Arguments( "mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithNeighbour(...)" );
}

void mg::mappings::CreateGrid::prepareSendToNeighbour(
    mg::Vertex  &vertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith3Arguments( "prepareSendToNeighbour(...)", vertex, toRank, level );
    // @todo Insert your code here
    logTraceOut( "prepareSendToNeighbour(...)" );
}

void mg::mappings::CreateGrid::prepareCopyToRemoteNode(
    mg::Vertex  &localVertex,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::CreateGrid::prepareCopyToRemoteNode(
    mg::Cell  &localCell,
    int                                           toRank,
    const tarch::la::Vector<DIMENSIONS, double>   &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>   &cellSize,
    int                                           level
)
{
    logTraceInWith5Arguments( "prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level );
    // @todo Insert your code here
    logTraceOut( "prepareCopyToRemoteNode(...)" );
}

void mg::mappings::CreateGrid::mergeWithRemoteDataDueToForkOrJoin(
    mg::Vertex  &localVertex,
    const mg::Vertex  &masterOrWorkerVertex,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &x,
    const tarch::la::Vector<DIMENSIONS, double>  &h,
    int                                       level
)
{
    logTraceInWith6Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localVertex, masterOrWorkerVertex, fromRank, x, h, level );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

void mg::mappings::CreateGrid::mergeWithRemoteDataDueToForkOrJoin(
    mg::Cell  &localCell,
    const mg::Cell  &masterOrWorkerCell,
    int                                       fromRank,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                       level
)
{
    logTraceInWith3Arguments( "mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank );
    // @todo Insert your code here
    logTraceOut( "mergeWithRemoteDataDueToForkOrJoin(...)" );
}

bool mg::mappings::CreateGrid::prepareSendToWorker(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker
)
{
    logTraceIn( "prepareSendToWorker(...)" );
    // @todo Insert your code here
    logTraceOutWith1Argument( "prepareSendToWorker(...)", true );
    return true;
}

void mg::mappings::CreateGrid::prepareSendToMaster(
    mg::Cell                       &localCell,
    mg::Vertex                     *vertices,
    const peano::grid::VertexEnumerator       &verticesEnumerator,
    const mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator       &coarseGridVerticesEnumerator,
    const mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>   &fineGridPositionOfCell
)
{
    logTraceInWith2Arguments( "prepareSendToMaster(...)", localCell, verticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "prepareSendToMaster(...)" );
}


void mg::mappings::CreateGrid::mergeWithMaster(
    const mg::Cell           &workerGridCell,
    mg::Vertex *const        workerGridVertices,
    const peano::grid::VertexEnumerator &workerEnumerator,
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell,
    int                                                                  worker,
    const mg::State          &workerState,
    mg::State                &masterState
)
{
    logTraceIn( "mergeWithMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "mergeWithMaster(...)" );
}


void mg::mappings::CreateGrid::receiveDataFromMaster(
    mg::Cell                        &receivedCell,
    mg::Vertex                      *receivedVertices,
    const peano::grid::VertexEnumerator        &receivedVerticesEnumerator,
    mg::Vertex *const               receivedCoarseGridVertices,
    const peano::grid::VertexEnumerator        &receivedCoarseGridVerticesEnumerator,
    mg::Cell                        &receivedCoarseGridCell,
    mg::Vertex *const               workersCoarseGridVertices,
    const peano::grid::VertexEnumerator        &workersCoarseGridVerticesEnumerator,
    mg::Cell                        &workersCoarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>    &fineGridPositionOfCell
)
{
    logTraceIn( "receiveDataFromMaster(...)" );
    // @todo Insert your code here
    logTraceOut( "receiveDataFromMaster(...)" );
}


void mg::mappings::CreateGrid::mergeWithWorker(
    mg::Cell           &localCell,
    const mg::Cell     &receivedMasterCell,
    const tarch::la::Vector<DIMENSIONS, double>  &cellCentre,
    const tarch::la::Vector<DIMENSIONS, double>  &cellSize,
    int                                          level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localCell.toString() );
}


void mg::mappings::CreateGrid::mergeWithWorker(
    mg::Vertex        &localVertex,
    const mg::Vertex  &receivedMasterVertex,
    const tarch::la::Vector<DIMENSIONS, double>   &x,
    const tarch::la::Vector<DIMENSIONS, double>   &h,
    int                                           level
)
{
    logTraceInWith2Arguments( "mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString() );
    // @todo Insert your code here
    logTraceOutWith1Argument( "mergeWithWorker(...)", localVertex.toString() );
}
#endif

void mg::mappings::CreateGrid::touchVertexFirstTime(
    mg::Vertex               &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                          &fineGridH,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexFirstTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    logTraceOutWith1Argument( "touchVertexFirstTime(...)", fineGridVertex );
}


void mg::mappings::CreateGrid::touchVertexLastTime(
    mg::Vertex         &fineGridVertex,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridX,
    const tarch::la::Vector<DIMENSIONS, double>                    &fineGridH,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfVertex
)
{
    logTraceInWith6Arguments( "touchVertexLastTime(...)", fineGridVertex, fineGridX, fineGridH, coarseGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfVertex );
    // @todo Insert your code here
    logTraceOutWith1Argument( "touchVertexLastTime(...)", fineGridVertex );
}


void mg::mappings::CreateGrid::enterCell(
    mg::Cell                 &fineGridCell,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                             &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "enterCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here



    logTraceOutWith1Argument( "enterCell(...)", fineGridCell );
}


void mg::mappings::CreateGrid::leaveCell(
    mg::Cell           &fineGridCell,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell,
    const tarch::la::Vector<DIMENSIONS, int>                       &fineGridPositionOfCell
)
{
    logTraceInWith4Arguments( "leaveCell(...)", fineGridCell, fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell );
    // @todo Insert your code here
    logTraceOutWith1Argument( "leaveCell(...)", fineGridCell );
}


void mg::mappings::CreateGrid::beginIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "beginIteration(State)", solverState );

    solverState.setPointList(_point_list);
    logTraceOutWith1Argument( "beginIteration(State)", solverState);
}


void mg::mappings::CreateGrid::endIteration(
    mg::State  &solverState
)
{
    logTraceInWith1Argument( "endIteration(State)", solverState );
    // @todo Insert your code here
    logTraceOutWith1Argument( "endIteration(State)", solverState);
}



void mg::mappings::CreateGrid::descend(
    mg::Cell *const          fineGridCells,
    mg::Vertex *const        fineGridVertices,
    const peano::grid::VertexEnumerator                &fineGridVerticesEnumerator,
    mg::Vertex *const        coarseGridVertices,
    const peano::grid::VertexEnumerator                &coarseGridVerticesEnumerator,
    mg::Cell                 &coarseGridCell
)
{
    logTraceInWith2Arguments( "descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "descend(...)" );
}


void mg::mappings::CreateGrid::ascend(
    mg::Cell *const    fineGridCells,
    mg::Vertex *const  fineGridVertices,
    const peano::grid::VertexEnumerator          &fineGridVerticesEnumerator,
    mg::Vertex *const  coarseGridVertices,
    const peano::grid::VertexEnumerator          &coarseGridVerticesEnumerator,
    mg::Cell           &coarseGridCell
)
{
    logTraceInWith2Arguments( "ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString() );
    // @todo Insert your code here
    logTraceOut( "ascend(...)" );
}

