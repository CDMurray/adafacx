#include "mg/Vertex.h"
#include "peano/utils/Loop.h"
#include "peano/grid/Checkpoint.h"
#include <cmath>


#include "matrixfree/stencil/StencilFactory.h"


double mg::Vertex::FineGridOmega = 0.0;
//double mg::Vertex::FineGridOmegaAlt = 0.0;
double mg::Vertex::AdditionalOmegaWeighting = 0.0;

mg::Vertex::AlternativeRestriction mg::Vertex::altRestriction = mg::Vertex::AlternativeRestriction::Undef;


mg::Vertex::Vertex():
    Base()
{
    // @todo Insert your code here
    _vertexData.setU(0.0);
    _vertexData.setAnalyseForRefine(true);
    tarch::la::Vector<THREE_POWER_D, double> Astencil;
    Astencil = matrixfree::stencil::get2DLaplaceStencil();
    _vertexData.setStencil(Astencil);
    //_vertexData.setD(Astencil(THREE_POWER_D / 2));
    //_vertexData.setTempD(Astencil(THREE_POWER_D / 2));
    //_vertexData.setStencilI(Astencil);
    //_vertexData.setDI(Astencil(THREE_POWER_D / 2));
    //_vertexData.setTempDI(Astencil(THREE_POWER_D / 2));
    _vertexData.setRefinementCondition(0.0);
}


mg::Vertex::Vertex(const Base::DoNotCallStandardConstructor &value):
    Base(value)
{
    // Please do not insert anything here
}


mg::Vertex::Vertex(const Base::PersistentVertex &argument):
    Base(argument)
{
    // @todo Insert your code here
}

void mg::Vertex::init(const tarch::la::Vector<DIMENSIONS, double> &x, double u)
{
    _vertexData.setF(0);
    _vertexData.setFI(0);
    _vertexData.setU(u);
    _vertexData.setUI(0.0);
    _vertexData.setUH(0.0);
    //_vertexData.setUHI(0.0);
    _vertexData.setIntegrationPoints(3);
    _vertexData.setOmega(FineGridOmega);
    //_vertexData.setOmegaAlt(FineGridOmegaAlt);
    _vertexData.setOmegaI(AdditionalOmegaWeighting * FineGridOmega);
    //_vertexData.setOmegaIAlt(AdditionalOmegaWeighting * FineGridOmegaAlt);
    //_vertexData.setC(0.0);
    _vertexData.setCLocal(0.0);
    _vertexData.setCLocalI(0.0);
    _vertexData.setCBPX(0.0);
    //_vertexData.setDiff(0.0);
    //_vertexData.setDiffI(0.0);
    _vertexData.setConverged(false);
#ifdef Dim2
    if(x(1) < 1e-8)
    {
        double pi = 3.1415926535897;
        //_vertexData.setU(1.0);
        _vertexData.setU(sin(pi*x(0)));
        //_vertexData.setU(1.0);
    }
    else if((x(0) < 1e-8) || (x(0) > 1 - 1e-8) || (x(1) > 1 - 1e-8))
    {
        _vertexData.setU(0.0);
    }

#elif Dim3
    if(x(1) < 1e-8)
    {
        _vertexData.setU(1.0);
    }
    else if((x(0) < 1e-8) || (x(2) < 1e-8) || (x(0) > 1 - 1e-8) || (x(1) > 1 - 1e-8) || (x(2) > 1 - 1e-8))
    {
        _vertexData.setU(0.0);
    }

#endif

    //_vertexData.setD(0);
    //_vertexData.setDI(0);

    //_vertexData.setTempD(0);
    //_vertexData.setTempDI(0);

    //_vertexData.setTempc(0);

    _vertexData.setAnalyseForRefine(true);

    //_vertexData.setStencilI(0.0);

    _vertexData.setConverged(true);

    _vertexData.setResH(0.0);
    _vertexData.setRes(0.0);
    //_vertexData.setResTwo(0.0);

    _vertexData.setParentOfOtherRank(false);

}

bool mg::Vertex::isStencilDiagonallyDominant()
{
    bool diagonallyDominant = false;
    double sum = 0.0;
    double centre = std::fabs(_vertexData.getD());
    for(int i = 0; i < THREE_POWER_D; i++)
    {
        if(i != THREE_POWER_D / 2)
        {
            sum += std::fabs(_vertexData.getStencil()(i));
        }
    }
    if(centre >= sum)
    {
        diagonallyDominant = true;
    }
    else if (std::fabs(centre - sum) / std::fabs(centre) < 1e-3)
    {
        diagonallyDominant = true;
    }
    return diagonallyDominant;
}

void mg::Vertex::setParent(bool parent)
{
    _vertexData.setParentOfOtherRank(parent);
}

bool mg::Vertex::getParent() const
{
    return _vertexData.getParentOfOtherRank();
}
void mg::Vertex::initOmega(double newOmega)
{
    _vertexData.setOmega(newOmega);
    _vertexData.setOmegaI(AdditionalOmegaWeighting * newOmega);
    //_vertexData.setOmegaAlt(newOmega);
    //_vertexData.setOmegaIAlt(AdditionalOmegaWeighting * newOmega);
}

void mg::Vertex::setOmega(double newOmega)
{
    _vertexData.setOmega(newOmega);
    //_vertexData.setOmegaAlt(newOmega);
}

void mg::Vertex::resetOmega()
{
    if(std::fabs(_vertexData.getOmega()) > 1e-9)
    {
        _vertexData.setOmegaI(AdditionalOmegaWeighting * FineGridOmega);
        //_vertexData.setOmegaIAlt(AdditionalOmegaWeighting * FineGridOmegaAlt);

    }
    _vertexData.setOmega(FineGridOmega);
    //_vertexData.setOmegaAlt(FineGridOmegaAlt);

}

double mg::Vertex::getOmega() const
{
    return _vertexData.getOmega();
}

/*void mg::Vertex::setOmegaAlt(double newOmega)
{
    _vertexData.setOmegaAlt(newOmega);
}*/

/*double mg::Vertex::getOmegaAlt() const
{
    return _vertexData.getOmegaAlt();
}*/


void mg::Vertex::setOmegaI(double newOmega)
{
    _vertexData.setOmegaI(newOmega);
    //_vertexData.setOmegaIAlt(newOmega);
}

double mg::Vertex::getOmegaI() const
{
    return _vertexData.getOmegaI();
}

/*void mg::Vertex::setOmegaIAlt(double newOmega)
{
    _vertexData.setOmegaIAlt(newOmega);
}*/

/*double mg::Vertex::getOmegaIAlt() const
{
    return _vertexData.getOmegaIAlt();
}*/

void mg::Vertex::setResI(double newRes)
{
    _vertexData.setResI(newRes);
}


void mg::Vertex::reset()
{
    /*if(std::abs(_vertexData.getResZero()) < 1e-9)
    {
        _vertexData.setResZero(_vertexData.getRes());
    }*/
    _vertexData.setRes(0.0);
    _vertexData.setResI(0.0);
    _vertexData.setResH(0.0);
    //_vertexData.setResHI(0.0);
    //_vertexData.setResTwo(0.0);
    //_vertexData.setTempRes(0.0);
}
/*void mg::Vertex::backupD()
{
    _vertexData.setTempD(_vertexData.getD());
}*/
/*void mg::Vertex::backupDI()
{

    _vertexData.setTempDI(_vertexData.getDI());

}*/



void mg::Vertex::setF(double f)
{
    _vertexData.setF(f);
}
/*void mg::Vertex::setTempF(double f)
{
    _vertexData.setTempF(f);
}*/
void mg::Vertex::setFI(double f)
{
    _vertexData.setFI(f);
}
void mg::Vertex::setRes(double res)
{
    _vertexData.setRes(res);
}
/*void mg::Vertex::setD(double d)
{
    _vertexData.setD(d);
}*/
/*void mg::Vertex::setTempD(double d)
{
    _vertexData.setTempD(d);
}*/
/*void mg::Vertex::setDI(double d)
{
    _vertexData.setDI(d);
}*/

void mg::Vertex::resetC()
{
    //_vertexData.setC(0.0);
    //_vertexData.setCFine(0.0);
    //_vertexData.setCCoarse(0.0);
    _vertexData.setCLocal(0.0);


    //_vertexData.setCI(0.0);
    //_vertexData.setCFineI(0.0);
    //_vertexData.setCCoarseI(0.0);
    _vertexData.setCLocalI(0.0);

    _vertexData.setCBPX(0.0);

    //_vertexData.setCTwo(0.0);
    //_vertexData.setCTwoLocal(0.0);
    //_vertexData.setTempc(0.0);
}

void mg::Vertex::resetLocalCorrections()
{
    _vertexData.setCLocal(0.0);
}
/*void mg::Vertex::resetCoarseCorrections()
{
    _vertexData.setCCoarse(0.0);
}*/
/*void mg::Vertex::resetFineCorrections()
{
    _vertexData.setCFine(0.0);
}*/

void mg::Vertex::resetLocalCorrectionsI()
{
    _vertexData.setCLocalI(0.0);
}
/*void mg::Vertex::resetCoarseCorrectionsI()
{
    _vertexData.setCCoarseI(0.0);
}*/
/*void mg::Vertex::resetFineCorrectionsI()
{
    _vertexData.setCFineI(0.0);
}*/
void mg::Vertex::resetBPXCorrections()
{
    _vertexData.setCBPX(0.0);
}
/*void mg::Vertex::resetLocalCorrectionsTwo()
{
    _vertexData.setCTwoLocal(0.0);
}*/
/*void mg::Vertex::resetTempCorrections()
{
    _vertexData.setTempc(0.0);
}*/

double mg::Vertex::getPotentialUpdate()
{
    double d;
    //if(std::fabs(_vertexData.getTempD()) < 1e-12)
    if(std::fabs(_vertexData.getStencil(4)) < 1e-12)
    {
        d = 0;
    }
    else
    {
        //d = (_vertexData.getOmega()) * (1.0 / _vertexData.getTempD()) * this->getUpdatedRes();
        d = (_vertexData.getOmega()) * (1.0 /_vertexData.getStencil(4)) * this->getUpdatedRes();
    }

    return d;
}
double mg::Vertex::getPotentialUpdateI()
{
    double dI;
    //if(std::fabs(_vertexData.getTempDI()) < 1e-12)
    if(std::fabs(_vertexData.getStencil(4)) < 1e-12)
    {
        dI = 0.0;
    }
    else
    {
        //dI = (_vertexData.getOmega()) * (1.0 / _vertexData.getTempDI()) * this->getUpdatedResI();
        dI = (_vertexData.getOmega()) * (1.0 / _vertexData.getStencil(4)) * this->getUpdatedResI();
    }
    return dI;
}


double mg::Vertex::getPotentialUpdateIDistinctOmega()
{
    double dI;
    //if(std::fabs(_vertexData.getTempDI()) < 1e-12)
    if(std::fabs(_vertexData.getStencil(4)) < 1e-12)
    {
        dI = 0.0;
    }
    else
    {
        //dI = (_vertexData.getOmegaI()) * (1.0 / _vertexData.getTempDI()) * this->getUpdatedResI();
        dI = (_vertexData.getOmegaI()) * (1.0 / _vertexData.getStencil(4)) * this->getUpdatedResI();
    }
    return dI;
}




/*double mg::Vertex::getPotentialUpdateAlt()
{
    double d;
    if(std::fabs(_vertexData.getTempD()) < 1e-12)
    {
        d = 0;
    }
    else
    {
        d = (_vertexData.getOmegaAlt()) * (1.0 / _vertexData.getTempD()) * this->getUpdatedRes();
    }


    return d;
}*/
/*double mg::Vertex::getPotentialUpdateIAlt()
{
    double dI;
    if(std::fabs(_vertexData.getTempDI()) < 1e-12)
    {
        dI = 0.0;
    }
    else
    {
        dI = (_vertexData.getOmegaAlt()) * (1.0 / _vertexData.getTempDI()) * this->getUpdatedResI();
    }
    return dI;
}*/
/*double mg::Vertex::getPotentialUpdateIAltDistinctOmega()
{
    double dI;
    if(std::fabs(_vertexData.getTempDI()) < 1e-12)
    {
        dI = 0.0;
    }
    else
    {
        dI = (_vertexData.getOmegaIAlt()) * (1.0 / _vertexData.getTempDI()) * this->getUpdatedResI();
    }
    return dI;
}*/

/*double mg::Vertex::getPotentialUpdateTwo()
{
    double d;
    if(std::fabs(_vertexData.getTempD()) < 1e-12)
    {
        d = 0;
    }
    else
    {
        d = (_vertexData.getOmega()) * (1.0 / _vertexData.getTempD()) * this->getResTwo();
    }
    return d;
}*/


void mg::Vertex::updateSolution()
{
    double d;
    double dI;
    if(std::fabs(_vertexData.getStencil(4)) < 1e-9)
    {
        d = 0.0;
    }
    else
    {
        d = (_vertexData.getOmega()) * (1.0 / _vertexData.getStencil(4)) * _vertexData.getRes();
    }
    if(std::fabs(_vertexData.getDI()) < 1e-9)
    {
        dI = 0.0;
    }
    else
    {
        dI = (_vertexData.getOmega()) * (1.0 / _vertexData.getStencil(4)) * _vertexData.getResI();
    }
    _vertexData.setU(_vertexData.getU() + d);
    _vertexData.setUI(_vertexData.getUI() + dI);
    _vertexData.setCLocal(d);
    _vertexData.setCLocalI(dI);
}
void mg::Vertex::updateRes(double h)
{
    _vertexData.setRes( _vertexData.getF() + _vertexData.getRes());
    _vertexData.setResI( _vertexData.getFI() + _vertexData.getResI());
    _vertexData.setResH( _vertexData.getF() + _vertexData.getResH());
    //_vertexData.setResHI( _vertexData.getFI() + _vertexData.getResHI());
}

void mg::Vertex::updateRes()
{
    _vertexData.setRes( _vertexData.getF() + _vertexData.getRes());
    _vertexData.setResI( _vertexData.getFI() + _vertexData.getResI());
    _vertexData.setResH( _vertexData.getF() + _vertexData.getResH());
    //_vertexData.setResHI( _vertexData.getFI() + _vertexData.getResHI());
}

double mg::Vertex::getRes() const
{
    return _vertexData.getRes();
}
double mg::Vertex::getUpdatedRes() const
{
    return _vertexData.getF() + _vertexData.getRes();
}
double mg::Vertex::getTempRes() const
{
    return _vertexData.getTempRes();
}
/*double mg::Vertex::getTempF() const
{
    return _vertexData.getTempF();
}*/
/*double mg::Vertex::getTempD() const
{
    return _vertexData.getTempD();
}*/
double mg::Vertex::getResI() const
{
    return _vertexData.getResI();
}
double mg::Vertex::getUpdatedResI() const
{
    return _vertexData.getFI() + _vertexData.getResI();
}
/*double mg::Vertex::getResTwo() const
{
    return _vertexData.getResTwo();
}*/
void mg::Vertex::setTempRes(double res)
{
    _vertexData.setTempRes(res);
}

/*double mg::Vertex::getResDiff() const
{
    if(std::abs(_vertexData.getResZero()) > 1e-9)
    {
        return _vertexData.getRes() / _vertexData.getResZero();
    }
    else
    {
        return 0.0;
    }
}*/

double mg::Vertex::getResH() const
{
    return _vertexData.getResH();
}
double mg::Vertex::getUpdatedResH() const
{
    return _vertexData.getF() +  _vertexData.getResH();
}
/*double mg::Vertex::getResHI() const
{
    return _vertexData.getResHI();
}*/

double mg::Vertex::getF() const
{
    return _vertexData.getF();
}
double mg::Vertex::getFI() const
{
    return _vertexData.getFI();
}
/*double mg::Vertex::getD() const
{
    return _vertexData.getD();
}*/
/*double mg::Vertex::getDI() const
{
    return _vertexData.getDI();
}*/

double mg::Vertex::getU() const
{
    return _vertexData.getU();
}
void mg::Vertex::setU(double newU)
{
    _vertexData.setU(newU);
}
/*double mg::Vertex::getUTwo() const
{
    return _vertexData.getUTwo();
}*/
/*void mg::Vertex::setUTwo(double newU)
{
    _vertexData.setUTwo(newU);
}*/


/*double mg::Vertex::getUOld() const
{
    return _vertexData.getUOld();
}*/
/*void mg::Vertex::updateUOld()
{
    _vertexData.setUOld(_vertexData.getU());
}*/

void mg::Vertex::setUI(double newU)
{
    _vertexData.setUI(newU);
}
double mg::Vertex::getUI() const
{
    return _vertexData.getUI();
}
/*void mg::Vertex::setUHI(double newU)
{
    _vertexData.setUHI(newU);
}*/
/*double mg::Vertex::getUHI() const
{
    return _vertexData.getUHI();
}*/
/*void mg::Vertex::setCorrection(double newCorrection)
{
    _vertexData.setC(newCorrection);
}*/
/*void mg::Vertex::setCoarseCorrection(double newCorrection)
{
    _vertexData.setCCoarse(newCorrection);
}*/
/*void mg::Vertex::setFineCorrection(double newCorrection)
{
    _vertexData.setCFine(newCorrection);
}*/
void mg::Vertex::setLocalCorrection(double newCorrection)
{
    _vertexData.setCLocal(newCorrection);
}
void mg::Vertex::setBPXCorrection(double newCorrection)
{
    _vertexData.setCBPX(newCorrection);
}
/*void mg::Vertex::setCorrectionTwo(double newCorrection)
{
    _vertexData.setCTwo(newCorrection);
}*/
/*void mg::Vertex::setLocalCorrectionTwo(double newCorrection)
{
    _vertexData.setCTwoLocal(newCorrection);
}*/
/*void mg::Vertex::setTempCorrection(double newCorrection)
{
    _vertexData.setTempc(newCorrection);
}*/
/*double mg::Vertex::getCorrection() const
{
    return _vertexData.getC();
}*/
/*double mg::Vertex::getCoarseCorrection() const
{
    return _vertexData.getCCoarse();
}*/
/*double mg::Vertex::getFineCorrection() const
{
    return _vertexData.getCFine();
}*/
double mg::Vertex::getLocalCorrection() const
{
    return _vertexData.getCLocal();
}
/*double mg::Vertex::getCorrectionTwo() const
{
    return _vertexData.getCTwo();
}*/
/*double mg::Vertex::getLocalCorrectionTwo() const
{
    return _vertexData.getCTwoLocal();
}*/
/*double mg::Vertex::getTempCorrections() const
{
    return _vertexData.getTempc();
}*/
double mg::Vertex::getBPXCorrection() const
{
    return _vertexData.getCBPX();
}
double mg::Vertex::getUH() const
{
    return _vertexData.getUH();
}

/*void mg::Vertex::setCorrectionI(double newCorrection)
{
    _vertexData.setCI(newCorrection);
}*/
/*void mg::Vertex::setCoarseCorrectionI(double newCorrection)
{
    _vertexData.setCCoarseI(newCorrection);
}*/
/*void mg::Vertex::setFineCorrectionI(double newCorrection)
{
    _vertexData.setCFineI(newCorrection);
}*/
void mg::Vertex::setLocalCorrectionI(double newCorrection)
{
    _vertexData.setCLocalI(newCorrection);
}
/*double mg::Vertex::getCorrectionI() const
{
    return _vertexData.getCI();
}*/
/*double mg::Vertex::getCoarseCorrectionI() const
{
    return _vertexData.getCCoarseI();
}*/
/*double mg::Vertex::getFineCorrectionI() const
{
    return _vertexData.getCFineI();
}*/
double mg::Vertex::getLocalCorrectionI() const
{
    return _vertexData.getCLocalI();
}
void mg::Vertex::setUH(double newU)
{
    _vertexData.setUH(newU);
}
/*void mg::Vertex::setDiff(double diff)
{
    _vertexData.setDiff(diff);
}*/
/*void mg::Vertex::setDiffI(double diff)
{
    _vertexData.setDiffI(diff);
}*/
/*double mg::Vertex::getDiff() const
{
    return _vertexData.getDiff();
}*/
/*double mg::Vertex::getDiffI() const
{
    return _vertexData.getDiffI();
}*/



/*double mg::Vertex::getK() const
{
    return _vertexData.getK();
}
void mg::Vertex::setK(double newK)
{
    _vertexData.setK(newK);
}*/

void mg::Vertex::incrementSamplingPoints()
{
    _vertexData.setIntegrationPoints(_vertexData.getIntegrationPoints() + 1);
}

bool mg::Vertex::canAnalyseForRefine()
{
    return _vertexData.getAnalyseForRefine();
}

double mg::Vertex::getSamplingPoints()
{
    return _vertexData.getIntegrationPoints();
}

void mg::Vertex::setAnalyseForRefine(bool newValue)
{
    _vertexData.setAnalyseForRefine(newValue);
}

/*double mg::Vertex::getChange() const
{
    return 0.0;
}*/


tarch::la::Vector<THREE_POWER_D, double> mg::Vertex::getAccumulatedValue() const
{
    return _vertexData.getAccumulatedValues();
}
/*tarch::la::Vector<THREE_POWER_D, double> mg::Vertex::getAccumulatedValueI() const
{
    return _vertexData.getAccumulatedValuesI();
}*/
/*tarch::la::Vector<THREE_POWER_D, double> mg::Vertex::getAccumulatedValueTranspose() const
{
    return _vertexData.getAccumulatedValuesTranspose();
}*/
/*tarch::la::Vector<THREE_POWER_D, double> mg::Vertex::getStencilBuffer() const
{
    return _vertexData.getParallelBuffer();
}*/
/*void mg::Vertex::prepBuffer()
{
    _vertexData.setParallelBuffer(_vertexData.getAccumulatedValues());
}*/

tarch::la::Vector<THREE_POWER_D, double> mg::Vertex::getStencil() const
{

    return _vertexData.getStencil();
}


/*tarch::la::Vector<THREE_POWER_D, double> mg::Vertex::getStencilI() const
{
    return _vertexData.getStencilI();
}*/

/*tarch::la::Vector<THREE_POWER_D, double> mg::Vertex::getStencilTranspose() const
{
    return _vertexData.getStencilTranspose();
}*/


double mg::Vertex::getStencilIndexOffset(int x, int y, int z)
{
    return _vertexData.getStencil(9 * (z + 1) + 3 * (y + 1) + (x + 1));
}

/*double mg::Vertex::getStencilIndexOffsetI(int x, int y, int z)
{
    return _vertexData.getStencilI(9 * (z + 1) + 3 * (y + 1) + (x + 1));
}*/


tarch::la::Vector<FIVE_POWER_D, double> mg::Vertex::getRestriction() const
{
    return _vertexData.getRestriction();
}


/*tarch::la::Vector<FIVE_POWER_D, double> mg::Vertex::getProlongation() const
{
    return _vertexData.getProlongation();
}*/

/*tarch::la::Vector<FIVE_POWER_D, double> mg::Vertex::getProlongationI() const
{
    return _vertexData.getProlongationI();
}*/


tarch::la::Vector<FIVE_POWER_D, double> mg::Vertex::getRestrictionI() const
{
    return _vertexData.getRestrictionI();
}


void mg::Vertex::resetAccumulatedValues()
{
    _vertexData.setAccumulatedValues(0.0);
    //_vertexData.setAccumulatedValuesI(0.0);
    //_vertexData.setAccumulatedValuesTranspose(0.0);
}

void mg::Vertex::incAccumulatedValue(double accVal, int x, int y, int z)
{
    tarch::la::Vector<THREE_POWER_D, double> accVals = _vertexData.getAccumulatedValues();
    accVals(9 * z + 3 * y + x) = accVals(9 * z + 3 * y + x) + accVal;
    _vertexData.setAccumulatedValues(accVals);
}
/*void mg::Vertex::incAccumulatedValueI(double accVal, int x, int y, int z)
{
    tarch::la::Vector<THREE_POWER_D, double> accVals = _vertexData.getAccumulatedValuesI();
    accVals(9 * z + 3 * y + x) = accVals(9 * z + 3 * y + x) + accVal;
    _vertexData.setAccumulatedValuesI(accVals);
}*/

/*void mg::Vertex::incAccumulatedValueTranspose(double accVal, int x, int y, int z)
{
    tarch::la::Vector<THREE_POWER_D, double> accVals = _vertexData.getAccumulatedValuesTranspose();
    accVals(9 * z + 3 * y + x) = accVals(9 * z + 3 * y + x) + accVal;
    _vertexData.setAccumulatedValuesTranspose(accVals);
}*/


/*void mg::Vertex::setAccumulatedValueTranspose(double newVal, int x, int y, int z)
{
    tarch::la::Vector<THREE_POWER_D, double> currentStencil = _vertexData.getAccumulatedValuesTranspose();
    currentStencil(9 * z + 3 * y + x) = newVal;
    _vertexData.setAccumulatedValuesTranspose(currentStencil);
}*/

/*void mg::Vertex::setAccumulatedValueTranspose(double newVal, int index)
{
    tarch::la::Vector<THREE_POWER_D, double> currentStencil = _vertexData.getAccumulatedValuesTranspose();
    currentStencil(index) = newVal;
    _vertexData.setAccumulatedValuesTranspose(currentStencil);
}*/

bool mg::Vertex::isDifferentToUpdate()
{
    bool different = false;

    for(int i = 0; i < THREE_POWER_D; i++)
    {
        double diff = std::fabs(_vertexData.getStencil()(i) - _vertexData.getAccumulatedValues()(i));
        if(diff > 1e-8)
        {
            different = true;
        }
    }

    return different;
}

void mg::Vertex::updateStencil()
{

    _vertexData.setStencil(_vertexData.getAccumulatedValues());
    //_vertexData.setD(_vertexData.getStencil()(THREE_POWER_D / 2));
    //_vertexData.setTempD(_vertexData.getStencil()(THREE_POWER_D / 2));


}


/*void mg::Vertex::updateStencilFine()
{
    tarch::la::Vector<THREE_POWER_D, double> I(0.0);
    I(THREE_POWER_D / 2) = 1.0;
    _vertexData.setStencil(_vertexData.getAccumulatedValues());
    _vertexData.setD(_vertexData.getStencil()(THREE_POWER_D / 2));
}*/

/*void mg::Vertex::updateStencilFine(double h)
{
    tarch::la::Vector<THREE_POWER_D, double> I(0.0);
    I(THREE_POWER_D / 2) = 1.0;
    _vertexData.setStencil(_vertexData.getAccumulatedValues());
    _vertexData.setD(_vertexData.getStencil()(THREE_POWER_D / 2));
}*/

/*void mg::Vertex::updateStencilIFine()
{
    _vertexData.setStencilI(_vertexData.getAccumulatedValuesI());
    _vertexData.setDI(_vertexData.getStencilI()(THREE_POWER_D / 2));
    _vertexData.setStencilI(_vertexData.getAccumulatedValues());
    _vertexData.setDI(_vertexData.getStencilI()(THREE_POWER_D / 2));
}*/

/*void mg::Vertex::updateStencilIFine(double h)
{
    _vertexData.setStencilI(_vertexData.getAccumulatedValuesI());
    _vertexData.setDI(_vertexData.getStencilI()(THREE_POWER_D / 2));
    _vertexData.setStencilI(_vertexData.getAccumulatedValues());
    _vertexData.setDI(_vertexData.getStencilI()(THREE_POWER_D / 2));
}*/

/*void mg::Vertex::updateStencilI()
{

    _vertexData.setStencilI(_vertexData.getStencil());

    _vertexData.setDI(_vertexData.getStencil()(THREE_POWER_D / 2));

    _vertexData.setDI(_vertexData.getStencilI()(THREE_POWER_D / 2));
}*/


/*void mg::Vertex::updateStencilTranspose()
{
    _vertexData.setStencilTranspose(_vertexData.getAccumulatedValuesTranspose());
}*/



void mg::Vertex::setStencil(tarch::la::Vector<THREE_POWER_D, double> stencil)
{
    _vertexData.setStencil(stencil);
    //_vertexData.setD(_vertexData.getStencil()(THREE_POWER_D / 2));
    //_vertexData.setTempD(_vertexData.getStencil()(THREE_POWER_D / 2));


    //_vertexData.setStencilI(stencil);
    //_vertexData.setDI(_vertexData.getStencil()(THREE_POWER_D / 2));
    //_vertexData.setTempDI(_vertexData.getStencil()(THREE_POWER_D / 2));
}


/*void mg::Vertex::setStencilI(tarch::la::Vector<THREE_POWER_D, double> stencil)
{
    _vertexData.setStencilI(stencil);
    _vertexData.setDI(_vertexData.getStencilI()(THREE_POWER_D / 2));
    _vertexData.setTempDI(_vertexData.getStencilI()(THREE_POWER_D / 2));
}*/


/*void mg::Vertex::setAccVal(tarch::la::Vector<THREE_POWER_D, double> stencil)
{
    _vertexData.setAccumulatedValues(stencil);
}*/


/*void mg::Vertex::setAccValI(tarch::la::Vector<THREE_POWER_D, double> stencil)
{
    _vertexData.setAccumulatedValuesI(stencil);
}*/


void mg::Vertex::setRestriction(tarch::la::Vector<FIVE_POWER_D, double> r)
{
    _vertexData.setRestriction(r);
}


void mg::Vertex::setRestrictionIMG(tarch::la::Vector<FIVE_POWER_D, double> r)
{
    switch(altRestriction)
    {
    case mg::Vertex::AlternativeRestriction::Injection:
    {
        break;
    }
    case mg::Vertex::AlternativeRestriction::HalfWeighting:
    case mg::Vertex::AlternativeRestriction::Smoothed:
    {
        tarch::la::Vector<FIVE_POWER_D, double> r2;
#ifdef Dim3
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                for(int z = 0; z < 5; z++)
                {
                    if(((x == 2) && (y == 2)) || ((y == 2) && (z == 2)) || ((z == 2) && (x == 2)))
                    {
                        r2(25 * x + 5 * y + z) = r(25 * x + 5 * y + z);
                    }
                }
            }
        }
#elif Dim2
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                if((x == 2) || (y == 2))
                {
                    r2(5 * x + y) = r(5 * x + y);
                }
                if((y == 3))
                {
                    r2(5 * x + y) = (-2.0) * r(5 * x + y);
                }
            }
        }
        r2(5 * 2 + 2) = 1.0;
#endif
        _vertexData.setRestrictionI(r2);
        break;
    }
    case mg::Vertex::AlternativeRestriction::PiecewiseConstant:
    {
        break;
    }
    case mg::Vertex::AlternativeRestriction::ReducedKernelFullWeighting:
    {
        break;
    }
    case mg::Vertex::AlternativeRestriction::FullWeighting:
    {
        _vertexData.setRestrictionI(r);
        break;
    }
    case mg::Vertex::AlternativeRestriction::SmoothedPartial:
    {
        break;
    }
    case mg::Vertex::AlternativeRestriction::DampedHalf:
    {
        break;
    }
    case mg::Vertex::AlternativeRestriction::AltHalf:
    {
        break;
    }
    case mg::Vertex::AlternativeRestriction::AltSmoothed:
    {
        break;
    }
    case mg::Vertex::AlternativeRestriction::Undef:
        break;
    }
}



/*void mg::Vertex::setProlongation(tarch::la::Vector<FIVE_POWER_D, double> p)
{
    _vertexData.setProlongation(p);
}*/
/*void mg::Vertex::setProlongationI(tarch::la::Vector<FIVE_POWER_D, double> p)
{
    _vertexData.setProlongationI(p);
}*/



void mg::Vertex::initialiseDLinearRestriction()
{
    _vertexData.setRestriction( matrixfree::stencil::getDLinearInterpolation() );


}


/*void mg::Vertex::initialiseDLinearProlongation()
{
    _vertexData.setProlongation( matrixfree::stencil::getDLinearInterpolation() );

}*/


void mg::Vertex::initialiseRestrictionI()
{
    switch(altRestriction)
    {
    case mg::Vertex::AlternativeRestriction::Injection:
    {

        tarch::la::Vector<FIVE_POWER_D, double> I(0.0);
        I(FIVE_POWER_D / 2) = 1.0;
        _vertexData.setRestrictionI(I);
        break;
    }
    case mg::Vertex::AlternativeRestriction::HalfWeighting:
    {
        tarch::la::Vector<FIVE_POWER_D, double> R;
#ifdef Dim3
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                for(int z = 0; z < 5; z++)
                {
                    if(((x == 2) && (y == 2)) || ((y == 2) && (z == 2)) || ((z == 2) && (x == 2)))
                    {
                        double xdiff = std::fabs((double)x - 2.0);
                        double ydiff = std::fabs((double)y - 2.0);
                        double zdiff = std::fabs((double)z - 2.0);
                        double scaling = 1.0;
                        scaling *= (3.0 - (double)xdiff) / 9.0;
                        scaling *= (3.0 - (double)ydiff) / 9.0;
                        scaling *= (3.0 - (double)zdiff) / 9.0;
                        R(25 * x + 5 * y + z) = scaling;
                    }
                    else
                    {
                        R(25 * x + 5 * y + z) = 0.0;
                    }
                    if((x == 2) && (y == 2) && (z == 2))
                    {
                        R(25 * x + 5 * y + z) = 1.0;
                    }

                }
            }
        }
#elif Dim2
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                if((x == 2) || (y == 2))
                {
                    double xdiff = std::fabs((double)x - 2.0);
                    double ydiff = std::fabs((double)y - 2.0);
                    double scaling = 1.0;
                    scaling *= (3.0 - (double)xdiff) / 6.0;
                    scaling *= (3.0 - (double)ydiff) / 6.0;
                    R(5 * x + y) = scaling;
                }
                else
                {
                    R(5 * x + y) = 0.0;
                }
                if((x == 2) && (y == 2))
                {
                    R(5 * x + y) = 1.0;
                }

            }
        }
#else
        assertionMsg(false, "not implemented yet");
#endif

        _vertexData.setRestrictionI(R);

        break;
    }
    case mg::Vertex::AlternativeRestriction::ReducedKernelFullWeighting:
    {
        tarch::la::Vector<FIVE_POWER_D, double> R;
#ifdef Dim3
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                for(int z = 0; z < 5; z++)
                {
                    if(((x > 0) && (x < 4)) || ((y > 0) && (y < 4)) || ((z > 0) && (z < 4)))
                    {
                        double xdiff = std::fabs((double)x - 2.0);
                        double ydiff = std::fabs((double)y - 2.0);
                        double zdiff = std::fabs((double)z - 2.0);
                        double scaling = 1.0;
                        scaling *= (2.0 - (double)xdiff) / 2.0;
                        scaling *= (2.0 - (double)ydiff) / 2.0;
                        scaling *= (2.0 - (double)zdiff) / 2.0;
                        R(25 * x + 5 * y + z) = scaling;
                    }
                    else
                    {
                        R(25 * x + 5 * y + z) = 0.0;
                    }
                }
            }
        }
#elif Dim2
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                if(((x > 0) && (x < 4)) || ((y > 0) && (y < 4)))
                {
                    double xdiff = std::fabs((double)x - 2.0);
                    double ydiff = std::fabs((double)y - 2.0);
                    double scaling = 1.0;
                    scaling *= (2.0 - (double)xdiff) / 2.0;
                    scaling *= (2.0 - (double)ydiff) / 2.0;
                    R(5 * x + y) = scaling;
                }
                else
                {
                    R(5 * x + y) = 0.0;
                }
            }
        }
#else
        assertionMsg(false, "not implemented yet");
#endif

        _vertexData.setRestrictionI(R);
        break;
    }
    case mg::Vertex::AlternativeRestriction::FullWeighting:
    {
        _vertexData.setRestrictionI( matrixfree::stencil::getDLinearInterpolation() );
    }
    case mg::Vertex::AlternativeRestriction::PiecewiseConstant:
    {
        tarch::la::Vector<FIVE_POWER_D, double> C(1.0 / 27.0);
        _vertexData.setRestrictionI( C );
    }
    case mg::Vertex::AlternativeRestriction::Smoothed:
    {
        tarch::la::Vector<FIVE_POWER_D, double> R;
        R = 0, 0, 0.0833333333, 0, 0, \
            0, 0, 0.1666666667, 0, 0, \
            0.0833333333, 0.1666666667, 0.4444444444, 0.1666666667, 0.0833333333, \
            0, 0, 0.1666666667, 0, 0, \
            0, 0, 0.0833333333, 0, 0;


        _vertexData.setRestrictionI( matrixfree::stencil::getDLinearInterpolation() - FineGridOmega * R);
        break;
    }
    case mg::Vertex::AlternativeRestriction::SmoothedPartial:
    {
        tarch::la::Vector<FIVE_POWER_D, double> R;
        R = 0, 0, 0.0833333333, 0, 0, \
            0, 0, 0.1666666667, 0, 0, \
            0.0833333333, 0.1666666667, 0.4444444444, 0.1666666667, 0.0833333333, \
            0, 0, 0.1666666667, 0, 0, \
            0, 0, 0.0833333333, 0, 0;


        tarch::la::Vector<SEVEN_POWER_D, double> R2;
        R2 = -0.0138888889, -0.0416666667, -0.0833333333, -0.0972222222, -0.0833333333, -0.0416666667, -0.0138888889, \
             -0.0416666667, 0, 0, 0.0833333333, 0, 0, -0.0416666667, \
             -0.0833333333, 0, 0, 0.1666666667, 0, 0, -0.0833333333, \
             -0.0972222222, 0.0833333333, 0.1666666667, 0.4444444444, 0.1666666667, 0.0833333333, -0.0972222222, \
             -0.0833333333, 0, 0, 0.1666666667, 0, 0, -0.0833333333, \
             -0.0416666667, 0, 0, 0.0833333333, 0, 0, -0.0416666667, \
             -0.0138888889, -0.0416666667, -0.0833333333, -0.0972222222, -0.0833333333, -0.0416666667, -0.0138888889;


        _vertexData.setRestrictionI( R );
        _vertexData.setAccRestrictionIBackup( R2 );
        _vertexData.setAccRestrictionI( R2 );
        break;
    }
    case mg::Vertex::AlternativeRestriction::DampedHalf:
    {
        tarch::la::Vector<FIVE_POWER_D, double> R;
#ifdef Dim3
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                for(int z = 0; z < 5; z++)
                {
                    if(((x == 2) && (y == 2)) || ((y == 2) && (z == 2)) || ((z == 2) && (x == 2)))
                    {
                        double xdiff = std::fabs((double)x - 2.0);
                        double ydiff = std::fabs((double)y - 2.0);
                        double zdiff = std::fabs((double)z - 2.0);
                        double scaling = 1.0;
                        scaling *= (3.0 - (double)xdiff) / 9.0;
                        scaling *= (3.0 - (double)ydiff) / 9.0;
                        scaling *= (3.0 - (double)zdiff) / 9.0;
                        R(25 * x + 5 * y + z) = scaling;
                    }
                    else
                    {
                        R(25 * x + 5 * y + z) = 0.0;
                    }
                    if((x == 2) && (y == 2) && (z == 2))
                    {
                        R(25 * x + 5 * y + z) = 1.0;
                    }

                }
            }
        }
#elif Dim2
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                if((x == 2) || (y == 2))
                {
                    double xdiff = std::fabs((double)x - 2.0);
                    double ydiff = std::fabs((double)y - 2.0);
                    double scaling = 1.0;
                    scaling *= (3.0 - (double)xdiff) / 6.0;
                    scaling *= (3.0 - (double)ydiff) / 6.0;
                    R(5 * x + y) = scaling;
                }
                else
                {
                    R(5 * x + y) = 0.0;
                }
                if((x == 2) && (y == 2))
                {
                    R(5 * x + y) = 1.0;
                }


            }
        }
#else
        assertionMsg(false, "not implemented yet");
#endif

        _vertexData.setRestrictionI(FineGridOmega * R);

        break;
    }
    case mg::Vertex::AlternativeRestriction::AltHalf:
    {
        tarch::la::Vector<FIVE_POWER_D, double> R;
#ifdef Dim3
        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                for(int z = 0; z < 5; z++)
                {
                    if(((x == 2) && (y == 2)) || ((y == 2) && (z == 2)) || ((z == 2) && (x == 2)))
                    {
                        double xdiff = std::fabs((double)x - 2.0);
                        double ydiff = std::fabs((double)y - 2.0);
                        double zdiff = std::fabs((double)z - 2.0);
                        double scaling = 1.0;
                        scaling *= (3.0 - (double)xdiff) / 9.0;
                        scaling *= (3.0 - (double)ydiff) / 9.0;
                        scaling *= (3.0 - (double)zdiff) / 9.0;
                        R(25 * x + 5 * y + z) = scaling;
                    }
                    else
                    {
                        R(25 * x + 5 * y + z) = 0.0;
                    }
                    if((x == 2) && (y == 2) && (z == 2))
                    {
                        R(25 * x + 5 * y + z) = 1.0;
                    }

                }
            }
        }
#elif Dim2

        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 5; y++)
            {
                if((x == 2) || (y == 2))
                {
                    double xdiff = std::fabs((double)x - 2.0);
                    double ydiff = std::fabs((double)y - 2.0);
                    double scaling = 1.0;
                    scaling *= (3.0 - (double)xdiff) / 6.0;
                    scaling *= (3.0 - (double)ydiff) / 6.0;
                    R(5 * x + y) = scaling;
                }
                else
                {
                    R(5 * x + y) = 0.0;
                }
                if((x == 2) && (y == 2))
                {
                    R(5 * x + y) = 1.0;
                }


            }
        }
#else
        assertionMsg(false, "not implemented yet");
#endif

        _vertexData.setRestrictionI(_vertexData.getOmega()*R);
    }
    case mg::Vertex::AlternativeRestriction::AltSmoothed:
    {
        tarch::la::Vector<FIVE_POWER_D, double> R;
        R = 0, 0, 0.0833333333, 0, 0, \
            0, 0, 0.1666666667, 0, 0, \
            0.0833333333, 0.1666666667, 0.4444444444, 0.1666666667, 0.0833333333, \
            0, 0, 0.1666666667, 0, 0, \
            0, 0, 0.0833333333, 0, 0;

        _vertexData.setRestrictionI( FineGridOmega * R);
    }
    case mg::Vertex::AlternativeRestriction::Undef:
        //assertionMsg(false, "not properly initialised" );
        break;
    }
}

/*void mg::Vertex::initialiseProlongationI()
{
    tarch::la::Vector<FIVE_POWER_D, double> I(0.0);
    I(FIVE_POWER_D / 2) = 1.0;
    _vertexData.setProlongationI(I);
    _vertexData.setProlongationI( matrixfree::stencil::getDLinearInterpolation() );

}*/



void mg::Vertex::initialiseRestrictionIAlt()
{
    tarch::la::Vector<FIVE_POWER_D, double> R;
#ifdef Dim3
    for(int x = 0; x < 5; x++)
    {
        for(int y = 0; y < 5; y++)
        {
            for(int z = 0; z < 5; z++)
            {
                if(((x == 2) && (y == 2)) || ((y == 2) && (z == 2)) || ((z == 2) && (x == 2)))
                {
                    double xdiff = std::fabs((double)x - 2.0);
                    double ydiff = std::fabs((double)y - 2.0);
                    double zdiff = std::fabs((double)z - 2.0);
                    double scaling = 1.0;
                    scaling *= (3.0 - (double)xdiff) / 3.0;
                    scaling *= (3.0 - (double)ydiff) / 3.0;
                    scaling *= (3.0 - (double)zdiff) / 3.0;
                    R(25 * x + 5 * y + z) = scaling;
                }
                else
                {
                    R(25 * x + 5 * y + z) = 0.0;
                }

            }
        }
    }
#elif Dim2
    for(int x = 0; x < 5; x++)
    {
        for(int y = 0; y < 5; y++)
        {
            if((x == 2) || (y == 2))
            {
                double xdiff = std::fabs((double)x - 2.0);
                double ydiff = std::fabs((double)y - 2.0);
                double scaling = 1.0;
                scaling *= (3.0 - (double)xdiff) / 3.0;
                scaling *= (3.0 - (double)ydiff) / 3.0;
                R(5 * x + y) = scaling;
            }
            else
            {
                R(5 * x + y) = 0.0;
            }

        }
    }
#else
    assertionMsg(false, "not implemented yet");
#endif

    _vertexData.setRestrictionI(R);
}


void mg::Vertex::initialiseRestrictionIAlt2()
{
    tarch::la::Vector<FIVE_POWER_D, double> R;
#ifdef Dim3
    for(int x = 0; x < 5; x++)
    {
        for(int y = 0; y < 5; y++)
        {
            for(int z = 0; z < 5; z++)
            {
                if(((x > 0) && (x < 4)) || ((y > 0) && (y < 4)) || ((z > 0) && (z < 4)))
                {
                    double xdiff = std::fabs((double)x - 2.0);
                    double ydiff = std::fabs((double)y - 2.0);
                    double zdiff = std::fabs((double)z - 2.0);
                    double scaling = 1.0;
                    scaling *= (2.0 - (double)xdiff) / 2.0;
                    scaling *= (2.0 - (double)ydiff) / 2.0;
                    scaling *= (2.0 - (double)zdiff) / 2.0;
                    R(25 * x + 5 * y + z) = scaling;
                }
                else
                {
                    R(25 * x + 5 * y + z) = 0.0;
                }
            }
        }
    }
#elif Dim2
    for(int x = 0; x < 5; x++)
    {
        for(int y = 0; y < 5; y++)
        {
            if(((x > 0) && (x < 4)) || ((y > 0) && (y < 4)))
            {
                double xdiff = std::fabs((double)x - 2.0);
                double ydiff = std::fabs((double)y - 2.0);
                double scaling = 1.0;
                scaling *= (2.0 - (double)xdiff) / 2.0;
                scaling *= (2.0 - (double)ydiff) / 2.0;
                R(5 * x + y) = scaling;
            }
            else
            {
                R(5 * x + y) = 0.0;
            }
        }
    }
#else
    assertionMsg(false, "not implemented yet");
#endif

    _vertexData.setRestrictionI(R);
}

double mg::Vertex::getRestrictionElement(int x, int y, int z)
{
    if((x < 0) || (x > 4) || (y < 0) || (y > 4) || (z < 0) || (z > 4))
    {
        return 0.0;
    }
    else
    {
        return _vertexData.getRestriction()(x + (5 * y) + z * 25);
    }
}
double mg::Vertex::getRestrictionElementI(int x, int y, int z)
{
    if((x < 0) || (x > 4) || (y < 0) || (y > 4) || (z < 0) || (z > 4))
    {
        return 0.0;
    }
    else
    {
        return _vertexData.getRestrictionI()(x + (5 * y));
    }
}

/*double mg::Vertex::getProlongationElement(int x, int y, int z)
{
    if((x < 0) || (x > 4) || (y < 0) || (y > 4) || (z < 0) || (z > 4))
    {
        return 0.0;
    }
    else
    {
        return _vertexData.getProlongation()(x + 5 * y + z * 25);
    }
}*/

/*double mg::Vertex::getProlongationElementI(int x, int y, int z)
{
    if((x < 0) || (x > 4) || (y < 0) || (y > 4) || (z < 0) || (z > 4))
    {
        return 0.0;
    }
    else
    {
        return getDummyProlongationElement(x, y, z);
    }
}*/


double mg::Vertex::getDummyProlongationElement(int x, int y, int z)
{

    tarch::la::Vector<FIVE_POWER_D, double> P(0.0);
    P = matrixfree::stencil::getDLinearInterpolation();

    if((x < 0) || (x > 4) || (y < 0) || (y > 4) || (z < 0) || (z > 4))
    {
        return 0.0;
    }
    else
    {
        return P(x + 5 * y + z * 25);
    }
}


double mg::Vertex::getError(const tarch::la::Vector<DIMENSIONS, double> &fineGridX, const tarch::la::Vector<DIMENSIONS, double> &fineGridH) const
{
    double pi = 3.1415926535897;
    double timeStep = fineGridX(2) / fineGridH(2);
    double actual = exp(-2.0 * pi * pi * timeStep * 0.005) * sin(pi * fineGridX(0)) * sin(pi * fineGridX(1));
    return (actual - _vertexData.getU());
}

/*double mg::Vertex::getRelativeError(const tarch::la::Vector<DIMENSIONS, double> &fineGridX, const tarch::la::Vector<DIMENSIONS, double> &fineGridH) const
{
    double pi = 3.1415926535897;
    double timeStep = fineGridX(2) / fineGridH(2);
    double actual = exp(-2.0 * pi * pi * timeStep * 0.005) * sin(pi * fineGridX(0)) * sin(pi * fineGridX(1));
    if(actual != 0.0)
    {
        return (actual - _vertexData.getU()) / actual;
    }
    else
    {
        return actual - _vertexData.getU();
    }
}*/

double mg::Vertex::getRefinementCondition() const
{
    return _vertexData.getRefinementCondition();
}
void mg::Vertex::resetRefinementCondition()
{
    _vertexData.setRefinementCondition(0.0);
}


bool  mg::Vertex::isConverged()
{
    return _vertexData.getConverged();
}
void  mg::Vertex::setConverged(bool _isConverged)
{
    _vertexData.setConverged(_isConverged);
}
/*void  mg::Vertex::setTempConverged(bool tempConverged)
{
    _vertexData.setTempConverged(tempConverged);
}*/
/*bool  mg::Vertex::getTempConverged()
{
    return _vertexData.getTempConverged();
}*/
/*void  mg::Vertex::updateConvergence()
{
    if(_vertexData.getTempConverged() == true)
    {
        _vertexData.setConverged(true);
    }
}*/


void mg::Vertex::incRestrictionAccVal(double accVal, int x, int y, int z)
{
    if((x > 5) || (x < -1) || (y > 5) || (y < -1) || (z > 5) || (z < -1))
    {

    }
    else
    {
        tarch::la::Vector<SEVEN_POWER_D, double> accVals = _vertexData.getAccRestrictionI();
        accVals(7 * (y + 1) + (x + 1) ) = accVals(7 * (y + 1) + (x + 1) ) + accVal;
        _vertexData.setAccRestrictionI(accVals);
    }

}

void mg::Vertex::resetRestrictionAccVal()
{
    _vertexData.setAccRestrictionIBackup(_vertexData.getAccRestrictionI());
    _vertexData.setAccRestrictionI(0.0);
}

void mg::Vertex::updateRestrictionI()
{
    tarch::la::Vector<FIVE_POWER_D, double> temp;
    for(int i = 0; i < 5; i++)
    {
        for(int j = 0; j < 5; j++)
        {
            temp(5 * i + j) = _vertexData.getAccRestrictionI(7 * (i + 1) + (j + 1));
        }
    }
    _vertexData.setRestrictionI(temp);

}


/*void mg::Vertex::resetDeltas()
{
    _vertexData.setRestrictionDeltas(0.0);
}*/

/*void mg::Vertex::incDelta(tarch::la::Vector<FIVE_POWER_D, double> delta)
{
    _vertexData.setRestrictionDeltas(_vertexData.getRestrictionDeltas() + delta);

}*/
/*void mg::Vertex::updateBoxMGRestriction()
{
    _vertexData.setProlongationI(_vertexData.getProlongationI() + _vertexData.getRestrictionDeltas());

}*/

/*tarch::la::Vector<FIVE_POWER_D, double> mg::Vertex::getDelta() const
{
    return _vertexData.getRestrictionDeltas();
}*/


bool mg::Vertex::isFirstTouch()
{
    return _vertexData.getFirstTouch();

}
void mg::Vertex::resetFirstTouch()
{
    _vertexData.setFirstTouch(true);
}
void mg::Vertex::madeFirstTouch()
{
    _vertexData.setFirstTouch(false);
}


void mg::Vertex::zeroInterpolants()
{
    //_vertexData.setProlongationI(0.0);
    _vertexData.setRestrictionI(0.0);
    //_vertexData.setProlongation(0.0);
    _vertexData.setRestriction(0.0);

}

double mg::Vertex::getRestrictionElementIBoundary(int x, int y, int z)
{
    if((x < -1) || (x > 5) || (y < -1) || (y > 5) || (z < -1) || (z > 5))
    {
        return 0.0;
    }
    else
    {
        return _vertexData.getAccRestrictionIBackup((x + 1) + (7 * (y + 1)));
    }

}

tarch::la::Vector<SEVEN_POWER_D, double> mg::Vertex::getRestrictionElementIStencil()
{
    return _vertexData.getAccRestrictionIBackup();
}

