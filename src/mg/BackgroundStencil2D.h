
#ifndef _MG_BACKGROUNDSTENCIL2D_H_
#define _MG_BACKGROUNDSTENCIL2D_H_

#include "mg/point.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"
#include "mg/point.h"
#include "mg/State.h"
#include <list>
#include <vector>

#include "tarch/multicore/BooleanSemaphore.h"
#include "tarch/multicore/Lock.h"

#include "peano/heap/Heap.h"

#include "matrixfree/solver/Multigrid.h"
#include "mg/mappings/GenerateStencil.h"

namespace mg
{
    class BackgroundStencil2D;
    typedef peano::heap::PlainDoubleHeapWithDaStGenRecords DataHeap;
    typedef peano::heap::PlainBooleanHeap BoolHeap;
    typedef peano::heap::RLEIntegerHeap IntHeap;

}

class mg::BackgroundStencil2D
{
private:
    int _heapIndex;

    std::list<point> _point_list;
    matrixfree::solver::Multigrid _multigrid;
    double _min_x;
    double _min_y;
    double _min_z;
    double _maxDif;
    double _xdif;
    double _ydif;
    double _zdif;

    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> _stencil;
    double _cellX;
    double _cellY;
    double _cellZ;
    double _h;
    int _numberofSamplingPoints;
    bool _finished;

    double getPermeabilityD(double x, double y);
    double getSquaredSum(double xpos, double ypos, double h);
    double getVarInt(double upos, double h);
    double getCentreQuadrature(double xpos, double ypos, double h);
    double getEdgeQuadrature(double xpos, double ypos, double h, bool xdirection);
    double getCornerQuadrature(double xpos, double ypos, double h);
    double edgeNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_xposf, bool vert_yposf, const int numberofSamplingPoints);
    double centreNumericalIntegration(bool vert_xpos, bool vert_ypos, const int numberofSamplingPoints);
    double cornerNumericalIntegration(bool vert_xpos, bool vert_ypos, const int numberofSamplingPoints);
public:
    BackgroundStencil2D(mg::State &solverState);
    BackgroundStencil2D();
    void init(mg::State &solverState, int numberOfSamplingPoints, int heapIndex);
    bool operator()();
    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> getStencil();
    int getNumPoints();
    void setCellData(double cellX, double cellY, double cellZ, double h);
    bool isFinished();
    void resetFinished();

    static tarch::multicore::BooleanSemaphore _mysemaphore;

};

#endif
