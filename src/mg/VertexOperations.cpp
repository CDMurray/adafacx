// Do not modify any part of this file. It will be overwritten throughout the 
// next pdt run.


#include "mg/VertexOperations.h"
#include "peano/utils/Loop.h"
#include "peano/grid/Checkpoint.h"


mg::VertexOperations::VertexOperations() { 
}

 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllU(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getU(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllUH(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getUH(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllUI(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getUI(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllF(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getF(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllFI(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getFI(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllRes(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getRes(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllResI(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getResI(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllResH(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getResH(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllCLocal(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getCLocal(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllCLocalI(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getCLocalI(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllCBPX(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getCBPX(); enddforx return result; }
 tarch::la::Vector<FOUR_POWER_D,double> mg::VertexOperations::readAllRefinementCondition(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<FOUR_POWER_D,double> result; dfor4(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getRefinementCondition(); enddforx return result; }



 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readU(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getU(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readUH(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getUH(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readUI(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getUI(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readF(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getF(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readFI(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getFI(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readRes(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getRes(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readResI(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getResI(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readResH(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getResH(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readCLocal(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getCLocal(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readCLocalI(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getCLocalI(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readCBPX(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getCBPX(); enddforx return result; }
 tarch::la::Vector<TWO_POWER_D,double> mg::VertexOperations::readRefinementCondition(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices) { tarch::la::Vector<TWO_POWER_D,double> result; dfor2(x) result(xScalar) = vertices[ enumerator(x) ]._vertexData.getRefinementCondition(); enddforx return result; }






 tarch::la::Vector<TWO_POWER_D_TIMES_THREE_POWER_D,double>  mg::VertexOperations::readAccumulatedValues(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices)  { tarch::la::Vector<TWO_POWER_D_TIMES_THREE_POWER_D,double> result; dfor2(x) tarch::la::slice(result,vertices[ enumerator(x) ]._vertexData.getAccumulatedValues(),xScalar*THREE_POWER_D); enddforx return result; }





 double mg::VertexOperations::readU(const Vertex& vertex) { return vertex._vertexData.getU(); }
 double mg::VertexOperations::readUH(const Vertex& vertex) { return vertex._vertexData.getUH(); }
 double mg::VertexOperations::readUI(const Vertex& vertex) { return vertex._vertexData.getUI(); }
 double mg::VertexOperations::readF(const Vertex& vertex) { return vertex._vertexData.getF(); }
 double mg::VertexOperations::readFI(const Vertex& vertex) { return vertex._vertexData.getFI(); }
 double mg::VertexOperations::readRes(const Vertex& vertex) { return vertex._vertexData.getRes(); }
 double mg::VertexOperations::readResI(const Vertex& vertex) { return vertex._vertexData.getResI(); }
 double mg::VertexOperations::readResH(const Vertex& vertex) { return vertex._vertexData.getResH(); }
 double mg::VertexOperations::readCLocal(const Vertex& vertex) { return vertex._vertexData.getCLocal(); }
 double mg::VertexOperations::readCLocalI(const Vertex& vertex) { return vertex._vertexData.getCLocalI(); }
 double mg::VertexOperations::readCBPX(const Vertex& vertex) { return vertex._vertexData.getCBPX(); }
 double mg::VertexOperations::readRefinementCondition(const Vertex& vertex) { return vertex._vertexData.getRefinementCondition(); }










 tarch::la::Vector<THREE_POWER_D,double>  mg::VertexOperations::readAccumulatedValues(const Vertex& vertex)  { return vertex._vertexData.getAccumulatedValues(); }


 double  mg::VertexOperations::readAccumulatedValues(const Vertex& vertex, int index)  { assertion(index>=0); assertion(index<THREE_POWER_D); return vertex._vertexData.getAccumulatedValues(index); }







 void mg::VertexOperations::writeAllU(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setU( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllUH(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setUH( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllUI(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setUI( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllF(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setF( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllFI(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setFI( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllRes(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setRes( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllResI(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setResI( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllResH(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setResH( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllCLocal(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setCLocal( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllCLocalI(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setCLocalI( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllCBPX(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setCBPX( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeAllRefinementCondition(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<FOUR_POWER_D,double>& values ) { dfor4(x) vertices[ enumerator(x) ]._vertexData.setRefinementCondition( values(xScalar) ); enddforx }


 void mg::VertexOperations::writeU(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setU( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeUH(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setUH( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeUI(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setUI( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeF(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setF( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeFI(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setFI( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeRes(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setRes( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeResI(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setResI( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeResH(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setResH( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeCLocal(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setCLocal( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeCLocalI(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setCLocalI( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeCBPX(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setCBPX( values(xScalar) ); enddforx }
 void mg::VertexOperations::writeRefinementCondition(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D,double>& values ) { dfor2(x) vertices[ enumerator(x) ]._vertexData.setRefinementCondition( values(xScalar) ); enddforx }






 void mg::VertexOperations::writeAccumulatedValues(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D_TIMES_THREE_POWER_D,double>& values) { dfor2(x) tarch::la::Vector<THREE_POWER_D,double> temp = tarch::la::slice<THREE_POWER_D>(values,xScalar*THREE_POWER_D); vertices[ enumerator(x) ]._vertexData.setAccumulatedValues( temp ); enddforx }





 void mg::VertexOperations::writeU(Vertex& vertex, const double& values ) { vertex._vertexData.setU( values ); }
 void mg::VertexOperations::writeUH(Vertex& vertex, const double& values ) { vertex._vertexData.setUH( values ); }
 void mg::VertexOperations::writeUI(Vertex& vertex, const double& values ) { vertex._vertexData.setUI( values ); }
 void mg::VertexOperations::writeF(Vertex& vertex, const double& values ) { vertex._vertexData.setF( values ); }
 void mg::VertexOperations::writeFI(Vertex& vertex, const double& values ) { vertex._vertexData.setFI( values ); }
 void mg::VertexOperations::writeRes(Vertex& vertex, const double& values ) { vertex._vertexData.setRes( values ); }
 void mg::VertexOperations::writeResI(Vertex& vertex, const double& values ) { vertex._vertexData.setResI( values ); }
 void mg::VertexOperations::writeResH(Vertex& vertex, const double& values ) { vertex._vertexData.setResH( values ); }
 void mg::VertexOperations::writeCLocal(Vertex& vertex, const double& values ) { vertex._vertexData.setCLocal( values ); }
 void mg::VertexOperations::writeCLocalI(Vertex& vertex, const double& values ) { vertex._vertexData.setCLocalI( values ); }
 void mg::VertexOperations::writeCBPX(Vertex& vertex, const double& values ) { vertex._vertexData.setCBPX( values ); }
 void mg::VertexOperations::writeRefinementCondition(Vertex& vertex, const double& values ) { vertex._vertexData.setRefinementCondition( values ); }










 void mg::VertexOperations::writeAccumulatedValues(Vertex& vertex, const tarch::la::Vector<THREE_POWER_D,double>& values) { vertex._vertexData.setAccumulatedValues( values ); }


 void mg::VertexOperations::writeAccumulatedValues(Vertex& vertex, int index, double value) { vertex._vertexData.setAccumulatedValues( index, value ); }







































































































































