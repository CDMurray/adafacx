#include "mg/BackgroundStencil2D.h"
#include "mg/VertexOperations.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"
#include <cmath>

#include "matrixfree/stencil/ElementMatrix.h"
#include "matrixfree/stencil/StencilFactory.h"

tarch::multicore::BooleanSemaphore mg::BackgroundStencil2D::_mysemaphore;

void mg::BackgroundStencil2D::setCellData(double cellX, double cellY, double cellZ, double h)
{
    _cellX = cellX;
    _cellY = cellY;
    _cellZ = cellZ;
    _h = h;
}

void mg::BackgroundStencil2D::init(mg::State &solverState, int numberOfSamplingPoints, int heapIndex)
{
    _numberofSamplingPoints = numberOfSamplingPoints;
    _finished = false;
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            _stencil(i, j) = 0.0;
        }
    }

    struct point dif = solverState.getMaxDifPoint();
    _xdif = dif.x;
    _ydif = dif.y;
    _zdif = dif.z;
    struct point min = solverState.getMin();
    _min_x = min.x;
    _min_y = min.y;
    _min_z = min.z;

    _point_list = solverState.getPointList();
    _multigrid = matrixfree::solver::Multigrid();

    _heapIndex = heapIndex;
}

mg::BackgroundStencil2D::BackgroundStencil2D()
{
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            _stencil(i, j) = 0.0;
        }
    }
}

mg::BackgroundStencil2D::BackgroundStencil2D(mg::State &solverState)
{
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int j = 0; j < TWO_POWER_D; j++)
        {
            _stencil(i, j) = 0.0;
        }
    }

    struct point dif = solverState.getMaxDifPoint();
    _xdif = dif.x;
    _ydif = dif.y;
    _zdif = dif.z;
    struct point min = solverState.getMin();
    _min_x = min.x;
    _min_y = min.y;
    _min_z = min.z;

    _point_list = solverState.getPointList();
    _multigrid = matrixfree::solver::Multigrid();
}



bool mg::BackgroundStencil2D::operator()()
{

    _finished = false;
    tarch::la::Matrix<4, 4, double> stencil;
    bool outer_x = false, outer_y = false;
    //i is the vertex that storing stencil in (so always view as x/h and y/h as basis functions)
    for(int i = 0; i < 4; i++)
    {
        bool inner_x = false, inner_y = false;
        for(int j = 0; j < 4; j++)
        {
            int count = 0;
            if(outer_x == inner_x)
            {
                count++;
            }
            if(outer_y == inner_y)
            {
                count++;
            }
            double result = 0.0;
            switch(count)
            {
            case 0:
                result = cornerNumericalIntegration(outer_x, outer_y, _numberofSamplingPoints);
                break;
            case 1:
                result = edgeNumericalIntegration(outer_x, outer_y, inner_x, inner_y, _numberofSamplingPoints);
                break;
            case 2:
                result = centreNumericalIntegration(outer_x, outer_y, _numberofSamplingPoints);
                break;
            }
            stencil(i, j) = result;
            if (inner_x)
            {
                inner_y = !inner_y;
            }
            inner_x = !inner_x;
        }

        if (outer_x)
        {
            outer_y = !outer_y;
        }
        outer_x = !outer_x;
    }

    tarch::multicore::Lock myLock(_mysemaphore);
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            DataHeap::getInstance().getData(_heapIndex)[4 * i + j]._persistentRecords._u = stencil(i, j);
        }
    }
    BoolHeap::getInstance().getData(_heapIndex)[0]._persistentRecords._u = true;
    myLock.free();
    return false;
}


bool mg::BackgroundStencil2D::isFinished()
{
    return _finished;
}
void mg::BackgroundStencil2D::resetFinished()
{
    _finished = false;
}

int mg::BackgroundStencil2D::getNumPoints()
{
    return _numberofSamplingPoints;
}


tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> mg::BackgroundStencil2D::getStencil()
{
    return _stencil;
}




//calculate the exact partial derivative integral centred around the value at point p
//in d-dim cube with side length h
double mg::BackgroundStencil2D::getSquaredSum(double xpos, double ypos, double h)
{

    double lowerBound[] = {xpos - h / 2.0, ypos - h / 2.0};
    double upperBound[] = {xpos + h / 2.0, ypos + h / 2.0};
    double total = 0.0;
    total +=  (upperBound[0] * upperBound[0] * upperBound[0] - lowerBound[0] * lowerBound[0] * lowerBound[0]) / 3.0;
    total +=  (upperBound[1] * upperBound[1] * upperBound[1] - lowerBound[1] * lowerBound[1] * lowerBound[1]) / 3.0;
    total *= h;
    return total;
}


double mg::BackgroundStencil2D::getVarInt(double upos, double h)
{
    double lowerBound = upos - h / 2.0;
    double total = 2 * lowerBound + h;
    total *= (h * h) / 2.0;
    return total;
}

double mg::BackgroundStencil2D::getCentreQuadrature(double xpos, double ypos, double h)
{
    return getSquaredSum(xpos, ypos, h);
}

double mg::BackgroundStencil2D::getEdgeQuadrature(double xpos, double ypos, double h, bool xdirection)
{
    if(xdirection)
    {
        return getVarInt(xpos, h) - getSquaredSum(xpos, ypos, h);
    }
    else
    {
        return getVarInt(ypos, h) - getSquaredSum(xpos, ypos, h);
    }
}

double mg::BackgroundStencil2D::getCornerQuadrature(double xpos, double ypos, double h)
{
    return getSquaredSum(xpos, ypos, h) - getVarInt(xpos, h) - getVarInt(ypos, h);
}

double mg::BackgroundStencil2D::getPermeabilityD(double x, double y)
{
    tarch::la::Vector<DIMENSIONS, double> vector;

#ifdef Dim3
    vector = x, y, 0.0;
#else
    vector = x, y;
#endif

    return mg::mappings::GenerateStencil::getEpsilon(vector);
}

double mg::BackgroundStencil2D::edgeNumericalIntegration(bool vert_xpos, bool vert_ypos, bool vert_xposf, bool vert_yposf, const int numberofSamplingPoints)
{
    struct point lowestCorner;
    if(vert_xpos)
    {
        lowestCorner.x = _cellX - _h / 2.0;
    }
    else
    {
        lowestCorner.x = _cellX + _h / 2.0;
    }
    if(vert_ypos)
    {
        lowestCorner.y = _cellY - _h / 2.0;
    }
    else
    {
        lowestCorner.y = _cellY + _h / 2.0;
    }
    //traversing x direction if difference in x values (hence xor the bools)
    bool xdirection = vert_xpos ^ vert_xposf;
    const double cellSize = _h;
    const double quadratureSpacing = cellSize / (double)numberofSamplingPoints;
    const double stepSize = 1.0 / (double)numberofSamplingPoints;
    std::vector<double> partialDerivativeEvals000(numberofSamplingPoints * numberofSamplingPoints);
    double cellXPos, cellYPos;
    double xmod, ymod;
    if(vert_xpos)
    {
        xmod = 1.0;
    }
    else
    {
        xmod = -1.0;
    }
    if(vert_ypos)
    {
        ymod = 1.0;
    }
    else
    {
        ymod = -1.0;
    }
    double xpos, ypos;
    xpos = stepSize / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        ypos = stepSize / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            partialDerivativeEvals000[j + i * numberofSamplingPoints] = getEdgeQuadrature(xpos, ypos, stepSize, xdirection);
            ypos += stepSize;
        }
        xpos += stepSize;
    }
    std::vector<double> permeabilityValues(numberofSamplingPoints * numberofSamplingPoints);
    int permCount = 0;
    cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            permeabilityValues[permCount] = getPermeabilityD(cellXPos, cellYPos);
            cellYPos += ymod * quadratureSpacing;
            permCount++;
        }
        cellXPos += xmod * quadratureSpacing;
    }
    double totalVal = 0.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            totalVal += permeabilityValues[j + i * numberofSamplingPoints] * partialDerivativeEvals000[j + i * numberofSamplingPoints];
        }
    }
    return totalVal;
}

double mg::BackgroundStencil2D::centreNumericalIntegration(bool vert_xpos, bool vert_ypos, const int numberofSamplingPoints)
{
    struct point lowestCorner;
    if(vert_xpos)
    {
        lowestCorner.x = _cellX - _h / 2.0;
    }
    else
    {
        lowestCorner.x = _cellX + _h / 2.0;
    }
    if(vert_ypos)
    {
        lowestCorner.y = _cellY - _h / 2.0;
    }
    else
    {
        lowestCorner.y = _cellY + _h / 2.0;
    }
    const double cellSize = _h;
    const double quadratureSpacing = cellSize / (double)numberofSamplingPoints;
    const double stepSize = 1.0 / (double)numberofSamplingPoints;
    std::vector<double> partialDerivativeEvals000(numberofSamplingPoints * numberofSamplingPoints);
    double cellXPos, cellYPos;
    double xmod, ymod;
    if(vert_xpos)
    {
        xmod = 1.0;
    }
    else
    {
        xmod = -1.0;
    }
    if(vert_ypos)
    {
        ymod = 1.0;
    }
    else
    {
        ymod = -1.0;
    }
    double xpos, ypos;
    xpos = stepSize / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        ypos = stepSize / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            partialDerivativeEvals000[j + i * numberofSamplingPoints] = getCentreQuadrature(xpos, ypos, stepSize);
            ypos += stepSize;
        }
        xpos += stepSize;
    }
    std::vector<double> permeabilityValues(numberofSamplingPoints * numberofSamplingPoints);
    int permCount = 0;
    cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    //cellXPos = lowestCorner.x + quadratureSpacing/2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
        //cellYPos = lowestCorner.y + quadratureSpacing/2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            permeabilityValues[permCount] = getPermeabilityD(cellXPos, cellYPos);
            cellYPos += ymod * quadratureSpacing;
            permCount++;
        }
        cellXPos += xmod * quadratureSpacing;
    }
    double totalVal = 0.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            totalVal += permeabilityValues[j + i * numberofSamplingPoints] * partialDerivativeEvals000[j + i * numberofSamplingPoints];
        }
    }
    return totalVal;
}

double mg::BackgroundStencil2D::cornerNumericalIntegration(bool vert_xpos, bool vert_ypos, const int numberofSamplingPoints)
{
    struct point lowestCorner;
    if(vert_xpos)
    {
        lowestCorner.x = _cellX - _h / 2.0;
    }
    else
    {
        lowestCorner.x = _cellX + _h / 2.0;
    }
    if(vert_ypos)
    {
        lowestCorner.y = _cellY - _h / 2.0;
    }
    else
    {
        lowestCorner.y = _cellY + _h / 2.0;
    }
    const double cellSize = _h;
    const double quadratureSpacing = cellSize / (double)numberofSamplingPoints;
    const double stepSize = 1.0 / (double)numberofSamplingPoints;
    std::vector<double> partialDerivativeEvals000(numberofSamplingPoints * numberofSamplingPoints);
    double cellXPos, cellYPos;
    double xmod, ymod;
    if(vert_xpos)
    {
        xmod = 1.0;
    }
    else
    {
        xmod = -1.0;
    }
    if(vert_ypos)
    {
        ymod = 1.0;
    }
    else
    {
        ymod = -1.0;
    }
    double xpos, ypos;
    xpos = stepSize / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        ypos = stepSize / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            partialDerivativeEvals000[j + i * numberofSamplingPoints] = getCornerQuadrature(xpos, ypos, stepSize);
            ypos += stepSize;
        }
        xpos += stepSize;
    }

    std::vector<double> permeabilityValues(numberofSamplingPoints * numberofSamplingPoints);
    int permCount = 0;
    cellXPos = lowestCorner.x + xmod * quadratureSpacing / 2.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        cellYPos = lowestCorner.y + ymod * quadratureSpacing / 2.0;
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            permeabilityValues[permCount] = getPermeabilityD(cellXPos, cellYPos);
            cellYPos += ymod * quadratureSpacing;
            permCount++;
        }
        cellXPos += xmod * quadratureSpacing;
    }
    double cornerVal = 0.0;
    for(int i = 0; i < numberofSamplingPoints; i++)
    {
        for(int j = 0; j < numberofSamplingPoints; j++)
        {
            cornerVal += permeabilityValues[j + i * numberofSamplingPoints] * partialDerivativeEvals000[j + i * numberofSamplingPoints];
        }
    }
    return cornerVal;
}

