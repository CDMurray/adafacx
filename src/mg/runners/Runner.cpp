#include "mg/runners/Runner.h"


#include "mg/repositories/Repository.h"
#include "mg/repositories/RepositoryFactory.h"

#include "mg/mappings/ProjectThreeGridDiff.h"
#include "mg/mappings/CreateGrid.h"
#include "mg/mappings/BStencil.h"
#include "mg/mappings/FMGRefine.h"


#include "peano/utils/UserInterface.h"

#include "tarch/Assertions.h"

#include "tarch/parallel/Node.h"
#include "tarch/parallel/NodePool.h"

#include "tarch/timing/Watch.h"

#include "peano/datatraversal/autotuning/OracleForOnePhaseDummy.h"
#include "peano/datatraversal/autotuning/Oracle.h"

//#include <tbb/tick_count.h>

// @todo Remove this include as soon as you've created your real-world geometry
#include "peano/geometry/Hexahedron.h"


#ifdef Parallel
#include "peano/parallel/SendReceiveBufferPool.h"
#include "peano/parallel/JoinDataBufferPool.h"
#include "tarch/parallel/FCFSNodePoolStrategy.h"
#include "peano/parallel/loadbalancing/OracleForOnePhaseWithGreedyPartitioning.h"
#include "peano/parallel/loadbalancing/OracleForOnePhase.h"
#endif


tarch::logging::Log mg::runners::Runner::_log( "mg::runners::Runner" );


mg::runners::Runner::Runner()
{
    // @todo Insert your code here
}


mg::runners::Runner::~Runner()
{
    // @todo Insert your code here
}


int mg::runners::Runner::run(SolverType type, int numberOfSmoothingSteps, int totalCores, int FMGCycle)
{
    // @todo Insert your geometry generation here and adopt the repository
    //       generation to your needs. There is a dummy implementation to allow
    //       for a quick start, but this is really very dummy (it generates
    //       solely a sphere computational domain and basically does nothing with
    //       it).

    // Start of dummy implementation
    peano::geometry::Hexahedron geometry(
        tarch::la::Vector<DIMENSIONS, double>(1.0),
        tarch::la::Vector<DIMENSIONS, double>(0.0)
    );
    mg::repositories::Repository *repository =
        mg::repositories::RepositoryFactory::getInstance().createWithSTDStackImplementation(
            geometry,
            tarch::la::Vector<DIMENSIONS, double>(1.0),  // domainSize,
            tarch::la::Vector<DIMENSIONS, double>(0.0)   // computationalDomainOffset
        );
    // End of dummy implementation



#ifdef Parallel
    if (tarch::parallel::Node::getInstance().isGlobalMaster())
    {
        tarch::parallel::NodePool::getInstance().setStrategy(
            new tarch::parallel::FCFSNodePoolStrategy()
        );
    }
    tarch::parallel::NodePool::getInstance().restart();
    tarch::parallel::NodePool::getInstance().waitForAllNodesToBecomeIdle();

    peano::parallel::loadbalancing::Oracle::getInstance().setOracle(
        new peano::parallel::loadbalancing::OracleForOnePhaseWithGreedyPartitioning(false)
    );
    peano::parallel::SendReceiveBufferPool::getInstance().setBufferSize( 64 );
    peano::parallel::JoinDataBufferPool::getInstance().setBufferSize( 64 );

    tarch::parallel::Node::getInstance().setDeadlockTimeOut(120 * 8);
    tarch::parallel::Node::getInstance().setTimeOutWarning(60 * 8);
#endif

#ifdef SharedMemoryParallelisation
    const int numberOfCores = totalCores;
    tarch::multicore::Core::getInstance().configure(numberOfCores, 0);
    peano::datatraversal::autotuning::Oracle::getInstance().setOracle(new peano::datatraversal::autotuning::OracleForOnePhaseDummy(true));
#endif

    int result = 0;
    if (tarch::parallel::Node::getInstance().isGlobalMaster())
    {
        result = runAsMaster( *repository, type, numberOfSmoothingSteps, totalCores, FMGCycle );
    }
#ifdef Parallel
    else
    {
        result = runAsWorker( *repository );
    }
#endif

    delete repository;

#ifdef Parallel
    tarch::parallel::NodePool::getInstance().terminate();
    mg::repositories::RepositoryFactory::getInstance().shutdownAllParallelDatatypes();
#endif

    return result;
}


int mg::runners::Runner::runAsMaster(mg::repositories::Repository &repository, SolverType type, int numberOfSmoothingSteps, int totalCores, int FMGCycle)
{
    peano::utils::UserInterface::writeHeader();

    if (
        type == SolverType::additiveMGWithExponentialDampingWithPlots
        or
        type == SolverType::additiveMGWithExponentialDamping
    )
    {
        mg::mappings::CreateGrid::exponentialDamping = true;
    }
    else
    {
        mg::mappings::CreateGrid::exponentialDamping = false;

    }

    repository.switchToCreateGrid();
#ifdef Parallel
    do
    {
        repository.iterate();
    }
    while(!repository.getState().isGridBalanced());
    repository.iterate();
#else
    repository.iterate();
#endif


    if(mg::mappings::FMGRefine::initial)
    {
        int iterate = 0;
        int totalCount = mg::mappings::CreateGrid::maxLevel - mg::mappings::CreateGrid::minLevel;
        while(iterate < totalCount)
        {
            repository.getState().setPerformRefine(true);
            repository.switchToPrerefine();
            repository.iterate();
            iterate++;
        }

    }
    repository.getState().setPerformRefine(false);

    logInfo( "runAsMaster(...)", "grid is constructed" );

    switch(type)
    {
    case SolverType::Undef:
        assertionMsg(false, "not properly initialised" );
        break;
    case SolverType::adaFACx:
    case SolverType::adaFACxWithPlots:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::adaFACx;
        break;
    case SolverType::AFACx:
    case SolverType::AFACxWithPlots:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::AFACx;
        break;
    case SolverType::additiveMG:
    case SolverType::additiveMGWithPlots:
    case SolverType::additiveMGWithExponentialDampingWithPlots:
    case SolverType::additiveMGWithExponentialDamping:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::Additive;
        break;
    case SolverType::bpx:
    case SolverType::bpxWithPlots:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::BPX;
        break;
    case SolverType::twoProject:
    case SolverType::twoProjectWithPlots:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::twoProject;
        break;
    case SolverType::twoProjectSimple:
    case SolverType::twoProjectSimpleWithPlots:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::twoProjectSimple;
        break;
    case SolverType::altRP:
    case SolverType::altRPWithPlots:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::altRP;
        break;
    case SolverType::adaFACxSimple:
    case SolverType::adaFACxSimpleWithPlots:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::adaFACxSimple;
        break;
    case SolverType::AFACxAlt:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::AFACxAlt;
        break;
    case SolverType::AFACxSimple:
    case SolverType::AFACxSimpleWithPlots:
        mg::mappings::ProjectThreeGridDiff::type = mg::mappings::ProjectThreeGridDiff::Type::AFACxSimple;
        break;
    }


    repository.switchToInitialSetup();
    tarch::timing::Watch stencilAssemblyWatch("mg::Runner", "runAsMaster()", false);
    repository.iterate();
    stencilAssemblyWatch.stopTimer();
    logInfo( "runAsMaster(...)", "assembly time=" << stencilAssemblyWatch.getCalendarTime() );



    repository.getState().setStencilConverged(false);
    bool precompute = false;



    switch(mg::mappings::BStencil::fineStencilType)
    {
    case mg::mappings::BStencil::FineStencilType::Undef:
    case mg::mappings::BStencil::FineStencilType::Static:
    case mg::mappings::BStencil::FineStencilType::StaticWithCoarse:
    case mg::mappings::BStencil::FineStencilType::Background:
    case mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse:
        precompute = false;
        break;
    case mg::mappings::BStencil::FineStencilType::Precompute:
    case mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse:
        precompute = true;
        break;
    }

    repository.getState().setUpdateCoarse(true);

    repository.getState().setStencilConverged(false);
    if(precompute)
    {
        bool stencilConverged = false;
        switch(mg::mappings::ProjectThreeGridDiff::restriction)
        {
        case mg::mappings::ProjectThreeGridDiff::Restriction::dlinear:
            repository.switchToStencilPrecomputeOnChangingGrid();
            break;
        case mg::mappings::ProjectThreeGridDiff::Restriction::boxMG:
            repository.switchToStencilPrecomputeOnChangingGrid();
            break;
        case mg::mappings::ProjectThreeGridDiff::Restriction::Undef:
            break;

        }
        int j = 1;
        while(!stencilConverged)
        {
            repository.iterate();
            stencilConverged = repository.getState().getStencilConverged();
            logInfo( "runAsMaster()", "stencil iteration " << j );
            j++;
        }
    }

    int maxDepth = mg::mappings::CreateGrid::initialLevel;
    //maxDepth-1 as don't interpolate coarse stencils to finest grid
    switch(mg::mappings::BStencil::fineStencilType)
    {
    case mg::mappings::BStencil::FineStencilType::Undef:
    case mg::mappings::BStencil::FineStencilType::Static:
    case mg::mappings::BStencil::FineStencilType::Background:
    case mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse:
        break;
    case mg::mappings::BStencil::FineStencilType::Precompute:
    case mg::mappings::BStencil::FineStencilType::StaticWithCoarse:
    case mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse:
    {
        for(int k = 0; k < maxDepth; k++)
        {
            repository.switchToStencilPrecomputeOnChangingGrid();
            repository.iterate();
        }
    }
    break;
    }



    repository.getState().setFirstIter(true);
    int i = 0;
    double initialL2Norm = 0.0;
    double initialLinfNorm = 0.0;
    double initialGlobalL2Norm = 0.0;
    double initialGlobalLinfNorm = 0.0;
    int MaxIterations = 200;


    int successiveResidualIncreases = 0;
    int maxNumberOfResidualIncreases = 5;


    repository.getState().setUpdateCoarse(true);

    double oldL2Norm = 0.0;

    int curMaxDepth = maxDepth;
    while(i < MaxIterations)
    {
        if(((i % FMGCycle) == 0) && (i > 0))
        {
            //For FMG cycle
            if(mg::mappings::CreateGrid::minLevel == mg::mappings::CreateGrid::finalMinLevel)
            {
                repository.getState().setPerformRefine(true);
            }
        }
        repository.getState().setL2Norm(0.0);
        repository.getState().setDiagonallyDominant(true);
        if(numberOfSmoothingSteps == 1)
        {
            switch (type)
            {
            case SolverType::Undef:
                break;
            case SolverType::adaFACx:
            case SolverType::AFACx:
            case SolverType::adaFACxSimple:
            case SolverType::AFACxSimple:
            case SolverType::AFACxAlt:
            case SolverType::additiveMG:
            case SolverType::additiveMGWithExponentialDamping:
            case SolverType::bpx:
            case SolverType::altRP:
            case SolverType::twoProject:
            case SolverType::twoProjectSimple:
                if(mg::mappings::BStencil::fineStencilType == mg::mappings::BStencil::FineStencilType::Precompute)
                {
                    repository.switchToSingleSmooth();
                }
                else
                {
                    repository.switchToSingleSmooth();
                }
                break;
            case SolverType::adaFACxWithPlots:
            case SolverType::AFACxWithPlots:
            case SolverType::adaFACxSimpleWithPlots:
            case SolverType::AFACxSimpleWithPlots:
            case SolverType::additiveMGWithPlots:
            case SolverType::additiveMGWithExponentialDampingWithPlots:
            case SolverType::bpxWithPlots:
            case SolverType::altRPWithPlots:
            case SolverType::twoProjectWithPlots:
            case SolverType::twoProjectSimpleWithPlots:
                if(mg::mappings::BStencil::FineStencilType::Precompute == mg::mappings::BStencil::fineStencilType)
                {
                    repository.switchToSingleSmoothWithPlots();
                }
                else
                {
                    repository.switchToSingleSmoothWithPlots();
                }
                break;

            }
            repository.getState().setActivelyProjecting(true);
            repository.iterate();

            if((mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::adaFACx) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::twoProject))
            {
                repository.switchToDummyRestrict();
                repository.iterate();
            }
            if((mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::AFACx) || (mg::mappings::ProjectThreeGridDiff::type == mg::mappings::ProjectThreeGridDiff::Type::twoProject))
            {
                repository.switchToDummyProject();
                repository.iterate();
            }
        }
        else if(numberOfSmoothingSteps > 1)
        {
            repository.switchToInitialSmooth();
            repository.iterate();
            repository.switchToMidSmooth();
            switch (type)
            {
            case SolverType::Undef:
                break;
            case SolverType::adaFACx:
            case SolverType::AFACx:
            case SolverType::adaFACxSimple:
            case SolverType::AFACxSimple:
            case SolverType::AFACxAlt:
            case SolverType::additiveMG:
            case SolverType::additiveMGWithExponentialDamping:
            case SolverType::bpx:
            case SolverType::altRP:
            case SolverType::twoProject:
            case SolverType::twoProjectSimple:
                repository.switchToFinalSmooth();
                break;
            case SolverType::adaFACxWithPlots:
            case SolverType::AFACxWithPlots:
            case SolverType::adaFACxSimpleWithPlots:
            case SolverType::AFACxSimpleWithPlots:
            case SolverType::additiveMGWithPlots:
            case SolverType::additiveMGWithExponentialDampingWithPlots:
            case SolverType::bpxWithPlots:
            case SolverType::altRPWithPlots:
            case SolverType::twoProjectWithPlots:
            case SolverType::twoProjectSimpleWithPlots:
                repository.switchToFinalSmoothWithPlots();
                break;

            }
            repository.iterate();


        }
        if(repository.getState().justRefined())
        {

            if(curMaxDepth < mg::mappings::CreateGrid::maxLevel)
            {
                curMaxDepth++;
            }
            if((mg::mappings::BStencil::fineStencilType == mg::mappings::BStencil::FineStencilType::Precompute) || (mg::mappings::BStencil::fineStencilType == mg::mappings::BStencil::FineStencilType::PrecomputeWithoutCoarse))
            {
                bool stencilConverged = false;
                repository.switchToStencilPrecomputeOnChangingGrid();
                int j = 1;
                while(!stencilConverged)
                {
                    repository.iterate();
                    stencilConverged = repository.getState().getStencilConverged();
                    logInfo( "runAsMaster()", "stencil iteration " << j << " within solver iteration " << i);
                    j++;
                }
            }
            if((mg::mappings::BStencil::fineStencilType == mg::mappings::BStencil::FineStencilType::Precompute) || (mg::mappings::BStencil::fineStencilType == mg::mappings::BStencil::FineStencilType::BackgroundWithCoarse) || (mg::mappings::BStencil::fineStencilType == mg::mappings::BStencil::FineStencilType::StaticWithCoarse))
            {
                int k = 1;
                repository.switchToStencilPrecomputeOnChangingGrid();
                while(k < curMaxDepth)
                {
                    repository.iterate();
                    logInfo( "runAsMaster()", "stencil coarsening " << k << " within solver iteration " << i);
                    k++;
                }

            }
        }

        if ( tarch::la::equals(initialL2Norm, 0.0) )
        {
            initialL2Norm = repository.getState().getL2Norm();
        }

        if ( tarch::la::equals(initialLinfNorm, 0.0) )
        {
            initialLinfNorm = repository.getState().getLinfNorm();
        }


        if ( tarch::la::equals(initialGlobalL2Norm, 0.0) )
        {
            initialGlobalL2Norm = repository.getState().getGlobalL2Norm();
        }

        if ( tarch::la::equals(initialGlobalLinfNorm, 0.0) )
        {
            initialGlobalLinfNorm = repository.getState().getGlobalLinfNorm();
        }

        if(!repository.getState().getAllDiagonallyDominant())
        {
            logInfo( "runAsMaster()", "solver diverges - stencil no longer diagonally dominant" );
        }

        logInfo( "runAsMaster()", "iteration " << (i + 1) );
        logInfo( "runAsMaster()", "|r|_L2=" << repository.getState().getL2Norm() );
        logInfo( "runAsMaster()", "|r|_inf=" << repository.getState().getLinfNorm() );
        logInfo( "runAsMaster()", "|u|(0.5)=" << repository.getState().getPointNorm() );
        logInfo( "runAsMaster()", "|u|(h/2)=" << repository.getState().getPointNorm2() );
        logInfo( "runAsMaster()", "rel residual=" << repository.getState().getL2Norm() / initialL2Norm );
        logInfo( "runAsMaster()", "rel max res=" << repository.getState().getLinfNorm() / initialLinfNorm );

        logInfo( "runAsMaster()", "|R|_L2=" << repository.getState().getGlobalL2Norm() );
        logInfo( "runAsMaster()", "|R|_inf=" << repository.getState().getGlobalLinfNorm() );
        logInfo( "runAsMaster()", "global l2=" << repository.getState().getGlobalL2Norm() / initialGlobalL2Norm );
        logInfo( "runAsMaster()", "global max=" << repository.getState().getGlobalLinfNorm() / initialGlobalLinfNorm );


        if (
            repository.getState().getL2Norm() > oldL2Norm * 1.1
            and
            i > 10
        )
        {
            logError( "runAsMaster()", "solver diverges" );
            oldL2Norm = repository.getState().getL2Norm();
            successiveResidualIncreases++;
            if(successiveResidualIncreases >= maxNumberOfResidualIncreases)
            {
                int newLmin = mg::mappings::CreateGrid::minLevel + 1;
                successiveResidualIncreases = 0;
                if(newLmin <= mg::mappings::CreateGrid::maxLevel)
                {
                    mg::mappings::CreateGrid::minLevel = newLmin;
                }
                else
                {
                    i = MaxIterations;

                }
                logInfo( "runAsMaster()", "Lmin_reduction to " << newLmin << " in iteration " << (i + 1));
            }

        }
        else if (
            repository.getState().getL2Norm() / initialL2Norm < 1e-10
        )
        {
            logInfo( "runAsMaster()", "solver has converged" );
            i = MaxIterations;
            successiveResidualIncreases = 0;
        }
        else
        {
            oldL2Norm = repository.getState().getL2Norm();
            successiveResidualIncreases = 0;
        }

        repository.getState().setFirstIter(false);
        if(mg::mappings::ProjectThreeGridDiff::delayCoarse == true)
        {
            int newLmin = mg::mappings::CreateGrid::minLevel - 1;
            if(newLmin >= mg::mappings::CreateGrid::finalMinLevel)
            {
                mg::mappings::CreateGrid::minLevel = newLmin;
            }
        }



        logInfo( "runAsMaster()", "min = " << mg::mappings::CreateGrid::minLevel );
        logInfo( "runAsMaster()", "max = " << mg::mappings::CreateGrid::maxLevel );
        logInfo( "runAsMaster()", "curMax = " << mg::mappings::CreateGrid::currentMaxLevel );
        logInfo( "runAsMaster()", "curMax = " << curMaxDepth );

        i++;


    }



    repository.logIterationStatistics( false );
    repository.terminate();

    return 0;
}
