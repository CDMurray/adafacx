// This file originally was created by pdt (Peano Development Toolkit) as part
// of a code based upon the Peano project by Tobias Weinzierl. For conditions 
// of distribution and use of this project, please see the copyright notice at
// www.peano-framework.org. Feel free to adopt the license and authorship of 
// this file and your project to your needs as long as the license is in 
// agreement with the original Peano user constraints. A reference to/citation  
// of  Peano and its author is highly appreciated.
#ifndef _MG_RUNNERS_RUNNER_H_ 
#define _MG_RUNNERS_RUNNER_H_ 

#include "peano/peano.h"
#include "tarch/logging/Log.h"
#include "tarch/logging/CommandLineLogger.h"


#include "tarch/multicore/MulticoreDefinitions.h"
#ifdef SharedMemoryParallelisation
#include "tarch/multicore/Core.h"
#endif


namespace mg {
    namespace runners {
      class Runner;
    }

    namespace repositories {
      class Repository;
    }
}



/**
 * Runner
 *
 */
class mg::runners::Runner {
  public:
    enum class SolverType {
     Undef,
     additiveMGWithPlots,
     additiveMG,
     additiveMGWithExponentialDampingWithPlots,
     additiveMGWithExponentialDamping,
     adaFACxWithPlots,
     adaFACx,
     AFACxWithPlots,
     AFACx,
     adaFACxSimpleWithPlots,
     adaFACxSimple,
     AFACxSimpleWithPlots,
     AFACxSimple,
     AFACxAlt,
     bpxWithPlots,
     bpx,
     altRPWithPlots,
     altRP,
     twoProjectWithPlots,
     twoProject,
     twoProjectSimpleWithPlots,
     twoProjectSimple,
    };

  private:
	static tarch::logging::Log _log;


    int runAsMaster(mg::repositories::Repository& repository, SolverType type,int smoothingSteps,int totalCores,int FMGcycle);
    
    #ifdef Parallel
    int runAsWorker(mg::repositories::Repository& repository);
    
    /**
     * If the master node calls runGlobalStep() on the repository, all MPI 
     * ranks besides rank 0 invoke this operation no matter whether they are 
     * idle or not. Hence, please call this operation manually within 
     * runAsMaster() if you require the master to execute the same function 
     * as well.
     */ 
    void runGlobalStep();
    #endif
  public:
    Runner();
    virtual ~Runner();

    /**
     * Run
     */
    int run(SolverType type,int smooth,int totalCores,int FMGcycle);
};

#endif
