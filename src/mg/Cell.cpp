#include "mg/Cell.h"
#include "mg/Vertex.h"


mg::Cell::Cell():
    Base()
{
    // @todo Insert your code here
    _cellData.setIntegrationNodeCount(2);
    _cellData.setAnalyseForRefine(true);
}


mg::Cell::Cell(const Base::DoNotCallStandardConstructor &value):
    Base(value)
{
    // Please do not insert anything here
}

mg::Cell::Cell(const Base::PersistentCell &argument):
    Base(argument)
{
}


tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, double> mg::Cell::getStencil() const
{
    return _cellData.getStencil();
}


void mg::Cell::setStencil(tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, double> stencil)
{
    _cellData.setStencil(stencil);
}


void mg::Cell::incrementSamplingPoints()
{
    _cellData.setIntegrationNodeCount(_cellData.getIntegrationNodeCount() + 1);

}

int mg::Cell::getSamplingPoints()
{
    return _cellData.getIntegrationNodeCount();
}

bool mg::Cell::canAnalyseForRefine()
{
    return _cellData.getAnalyseForRefine();
}

void mg::Cell::setAnalyseForRefine(bool newValue)
{
    _cellData.setAnalyseForRefine(newValue);
}


void mg::Cell::initHeap()
{
    const int newIndex = DataHeap::getInstance().createData(16);
    _cellData.setHeapIndex(newIndex);
    BoolHeap::getInstance().createDataForIndex(newIndex, 1);
    BoolHeap::getInstance().getData(newIndex)[0]._persistentRecords._u = false;
    assertion(newIndex >= 0);
}

void mg::Cell::initHeap(int heapIndex, int dataIndex)
{

    DataHeap::getInstance().createDataForIndex(heapIndex, 16);
    DataHeap::getInstance().createDataForIndex(dataIndex, 64);

    _cellData.setHeapIndex(heapIndex);
    _cellData.setDataHeapIndex(dataIndex);
    IntHeap::getInstance().createDataForIndex(heapIndex, 1);
    IntHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = 2;


    BoolHeap::getInstance().createDataForIndex(heapIndex, 2);

    BoolHeap::getInstance().getData(heapIndex)[0]._persistentRecords._u = false;
    BoolHeap::getInstance().getData(heapIndex)[1]._persistentRecords._u = false;

}

void mg::Cell::setHeap(int heapIndex, int dataIndex)
{
    _cellData.setHeapIndex(heapIndex);
    _cellData.setDataHeapIndex(dataIndex);
}
void mg::Cell::setHeap(int heapIndex)
{
    _cellData.setHeapIndex(heapIndex);
}

int mg::Cell::getHeapIndex()
{
    return _cellData.getHeapIndex();
}
int mg::Cell::getDataHeapIndex()
{
    return _cellData.getDataHeapIndex();
}

bool mg::Cell::getConverged()
{
    return _cellData.getHasConverged();
}


void mg::Cell::setConverged(bool converged)
{
    _cellData.setHasConverged(converged);
}


void mg::Cell::mapCellStencilOntoVertices(
    const tarch::la::Vector<4 * 9, double>  &frag,
    mg::Vertex *const                    fineGridVertices,
    const peano::grid::VertexEnumerator  &fineGridVerticesEnumerator
)
{
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int jx = 0; jx < 3; jx++)
        {
            for(int jy = 0; jy < 3; jy++)
            {
                double val = frag(i * THREE_POWER_D + 3 * jy + jx);
                fineGridVertices[fineGridVerticesEnumerator(i)].incAccumulatedValue( val, jx, jy, 0);
            }
        }
    }
}


void mg::Cell::mapCellStencilOntoVertices(
    const tarch::la::Vector<8 * 27, double>  &frag,
    mg::Vertex *const                    fineGridVertices,
    const peano::grid::VertexEnumerator  &fineGridVerticesEnumerator
)
{
    for(int i = 0; i < TWO_POWER_D; i++)
    {
        for(int jx = 0; jx < 3; jx++)
        {
            for(int jy = 0; jy < 3; jy++)
            {
                for(int jz = 0; jz < 3; jz++)
                {
                    double val = frag(i * THREE_POWER_D + 9 * jz + 3 * jy + jx);
                    fineGridVertices[fineGridVerticesEnumerator(i)].incAccumulatedValue( val, jx, jy, jz);
                }
            }
        }
    }
}
