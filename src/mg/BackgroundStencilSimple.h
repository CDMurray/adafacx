
#ifndef _MG_BACKGROUNDSTENCILSIMPLE_H_
#define _MG_BACKGROUNDSTENCILSIMPLE_H_

#include "mg/point.h"
#include "tarch/la/Matrix.h"
#include "tarch/la/Vector.h"
#include "mg/point.h"
#include "mg/State.h"
#include <list>
#include <vector>

#include "tarch/multicore/BooleanSemaphore.h"
#include "tarch/multicore/Lock.h"

#include "peano/heap/Heap.h"

#include "matrixfree/solver/Multigrid.h"
#include "mg/mappings/GenerateStencil.h"

namespace mg
{
    class BackgroundStencilSimple;
    typedef peano::heap::PlainDoubleHeapWithDaStGenRecords DataHeap;
    typedef peano::heap::PlainBooleanHeap BoolHeap;
    typedef peano::heap::RLEIntegerHeap IntHeap;

}

class mg::BackgroundStencilSimple
{
private:
    int _heapIndex;

    std::list<point> _point_list;
    matrixfree::solver::Multigrid _multigrid;
    double _min_x;
    double _min_y;
    double _min_z;
    double _maxDif;
    double _xdif;
    double _ydif;
    double _zdif;

    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> _stencil;
    double _cellX;
    double _cellY;
    double _cellZ;
    double _h;
    int _numberofSamplingPoints;
    bool _finished;

    double getEpsilon(const int numberofSamplingPoints);
    double getPermeabilityD(double xpos, double ypos, double zpos);
public:
    BackgroundStencilSimple(mg::State &solverState);
    BackgroundStencilSimple();
    void init(mg::State &solverState, int numberOfSamplingPoints, int heapIndex);
    bool operator()();
    tarch::la::Matrix<TWO_POWER_D, TWO_POWER_D, double> getStencil();
    int getNumPoints();
    void setCellData(double cellX, double cellY, double cellZ, double h);
    bool isFinished();
    void resetFinished();
    static tarch::multicore::BooleanSemaphore _mysemaphore;



    //virtual ~BackgroundStencil();
};

#endif